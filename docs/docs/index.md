<img alt="DW Logo" src="https://ogkcreative.com/email/ogk_logo.png" width="125" height="50" />

# OGK Creative

####Custom Wordpress Site by [OGK](https://ogkcreative.com)


Production: [https://ogkbase3.wpengine.com/](https://ogkbase3.wpengine.com/) 

Staging: [https://ogkbase3.wpengine.com/](https://ogkbase3.staging.wpengine.com/)


<img alt="WPE logo" src="https://mycdn-wpengine.netdna-ssl.com/packs/images/WPE_logo-4ff9f7c0c11cc715bcac74d194d7c153.svg" width="125" height="50" />

[WPEngine Dashboard:](https://my.wpengine.com/installs/ogkbase3)

*site is password protected because it is a transeferrable site on WP Engine. 

USER: `demo`

PASS: `**********`


