# Database


###Local DB Setup Script

*MAMP* - _c'mon it's MAMP do I really need to explain this!_

If you are going the manual route keep reading:

#####Create DB:Table and DB:User

```mysql
  CREATE DATABASE ogkbase3;
                  
  CREATE USER 'ogkbase3'@'localhost'
  IDENTIFIED BY '{{your_db_password}}';
                  
  GRANT  ALL PRIVILEGES
          ON  ogkbase3.*
          TO 'ogkbase3'@'localhost'
  WITH GRANT OPTION;
                                
  FLUSH PRIVILEGES;
``` 

####Login to MySQL
```bash
   $ mysql -u root -p
```
<small>It will then ask you to enter the password for the root mysql user</small>

####Import the SQL dump file `/wp-content/mysql.sql`
```mysql
mysql >  use ogkbase3;
mysql > source ~/Sites/{{your_install_folder_name}}/wp-content/mysql.sql;

```

## Update your `wp_config.php`
_ TODO: add a local default wp_config.php file_

*Items marked with `{{double brackets}}` are variables for you till fill in with your localhost setup*

```php
<?php
# Database Configuration
define( 'DB_NAME', 'ogkbase3' );
define( 'DB_USER', 'ogkbase3' );
define( 'DB_PASSWORD', '{{your_db_password}}' );
define( 'DB_HOST', '127.0.0.1' );
define( 'DB_HOST_SLAVE', '127.0.0.1' );
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', 'utf8_unicode_ci');
$table_prefix = 'wp_';

define( 'WP_SITEURL', 'http://{{your-local-hostname.com}}' );
define( 'WP_HOME', 'http://{{your-local-hostname.com}}' );

# Security Salts, Keys, Etc
# generate salts if ya wanna --> https://api.wordpress.org/secret-key/1.1/salt/
# paste in below

define('WPLANG', 'en');
define( 'WP_DEBUG', true);

define('ALLOW_UNFILTERED_UPLOADS', true);

# WP Engine ID <-- Not Needed either

# WP Engine Settings <-- remove these

# That's It. Pencils down
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');

```

 <small><small>_Hope this helps!_ :-) [CM](https://colinmichaels.com) </small></small>
