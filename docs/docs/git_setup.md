#GIT

#### WPE Git Deployment Docs

[https://wpengine.com/git/](https://wpengine.com/git/) 

## Setup Git With WPEngine 

[Legacy Staging Docs](https://wpengine.com/support/using-git-on-sites-with-legacy-staging/)

[Deploy to Staging](https://wpengine.com/support/using-git-on-sites-with-legacy-staging/#staging)

[Deploy to Production](#PRODUCTION)
[Deploy to Production](#STAGING)

1. Add your SSH Key to the WPE install
    1. Go to the WPE account page and choose the `Git push` option under the environment you are working in 
        1. <small>Don't have a key?  instuctions here --> [SSH Key for GitHub ](https://help.github.com/en/articles/connecting-to-github-with-ssh)</small>
    1. Generate a SSH key 
        ```
        $ ssh-keygen -t rsa -b 4096 -C "your_email@theogk.com" 
        ```     
    1. Paste your `SSH public key` into the input box.
1. #####Check to see what installs you have access to and your key is installed on
    
    ```bash
     $ ssh git@git.wpengine.com info
    ```   
   
2. Download a snapshot of the site (this will include the site files and DB dump)
    1. [Backup Points](https://my.wpengine.com/installs/ogkbase3/backup_points#production)
    1. [PHP My Admin](https://my.wpengine.com/installs/ogkbase3/phpmyadmin)

3. Add .gitignore 
    1. [WPE Default .gitignore](https://wpengine.com/wp-content/uploads/2013/10/recommended-gitignore-no-wp.txt)
    
4. Add the initial commit locally

```bash
$ git init
$ git add -A
$ git commit -m "Your first commit or whatever you are adding"

```

##PRODUCTION

##### +Add Remote
 
```bash
$ cd ~/Sites/ogkbase3
$ git remote add production git@git.wpengine.com:production/ogkbase3.git
```
#####Check that you have your remotes inputted correctly
```bash
$ git remote -v
```
##### <- Pull from

```bash
$ git pull staging master  
```

##### Push to ->

```bash
$ git push staging master 
```

##STAGING

##### + Add Remote 

```bash
$ cd ~/Sites/ogkbase3
$ git remote add staging git@git.wpengine.com:staging/ogkbase3.git
```

##### <- Pull from 

```bash                                          
$ git pull staging master  
```


##### Push to ->

```bash
$ git push staging master  
```

#### Contributing

  * Don't spoon it, Fork it!
  * Create your special feature branch: `git checkout -b my-new-feature` 
  * Commit your changes: `git commit -am 'Add some feature'` 
  * Push to the branch: `git push staging/master my-new-feature`


