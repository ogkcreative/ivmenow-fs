</main>

<footer class="flex-col flex-sb flex-ac">
    <div class="container">
        <div class="footer-top">
            <div class="buttons flex-col flex-ac">
                <div class="title-desktop ta-center">
                    <div class="h3 tc-wht">Receive emails about sales, new products, and personalized discounts!</div>
                </div>
                <div class="title-mobile ta-center">
                    <div class="h3 tc-wht">Let's improve your health.</div>
                </div>

                <div class="enter-email">
                    <?= do_shortcode('[gravityform id="2" title="false" description="true" ajax="true"]') ?>
                </div>
                <?php get_template_part("includes/social-icons"); ?>
            </div>
        </div>

        <div class="footer-center flex flex-sb">
            <div class="footer-menu desktop forty">
                <?php
                wp_nav_menu(array(
                    'menu' => 'footer-menu'
                ));
                ?>
            </div>
            <div class="footer-menu mobile forty">
                <?php
                wp_nav_menu(array(
                    'menu' => 'mobile-footer-menu'
                ));
                ?>
            </div>

            <div class="social-mobile">
                <?php get_template_part("includes/social-icons"); ?>
            </div>


            <div class="locations forty">
                <?php if (have_rows('location', 'options')): ?>
                <a href="/location" class="unclickable benefit-text tc-wht">Locations We Serve</a>
                <div class="locations-inner">
                    <?php while (have_rows('location', 'options')): the_row(); ?>
                        <div class="location small-text">
                            <a class="tc-wht" href="<?= get_sub_field('location_link', 'options')?>"><?= get_sub_field('location', 'options') ?></a>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>

        </div>

        <div class="footer-bottom flex flex-sb">
            <div class="footer-logo">
                <p class="copyright">
                    <span>&copy;<?= date("Y"); ?> <?= get_field('company_name', 'options') ?>. All rights reserved.</span><br>
                </p>
            </div>
            <div class="card-logos flex">
                <?= file_get_contents(get_field('mastercard', 'options')) ?>
                <?= file_get_contents(get_field('visa', 'options')) ?>
            </div>
            <div class="disclosure small-text tc-wht sixty-five">
                <?= get_field('disclosure', 'options') ?>
            </div>
        </div>
    </div>

</footer><!--.footer-->
<?php wp_footer(); ?>
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script>
    window.__lc = window.__lc || {};
    window.__lc.license = 10072583;
    ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
</script>
<noscript><a href="https://www.livechatinc.com/chat-with/10072583/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
<!-- End of LiveChat code -->
</body>
</html>