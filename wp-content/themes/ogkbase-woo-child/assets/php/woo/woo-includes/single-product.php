<?php
/** =========================================================================================== **/
/** ============================   SINGLE PRODUCT FUNCTIONS  ================================== **/
/** =========================================================================================== **/


// Add a container if field in site options is set to true!
if ( get_field('shop_page_contained', 'options') ) {
    add_filter('woocommerce_before_single_product', 'ogk_single_product_section_start');
    add_filter('woocommerce_after_single_product', 'ogk_single_product_section_end');
}

// Add container start
function woocommerce_container_start() {
    echo "<div class='container'>";
}

// Add container end
function woocommerce_container_end() {
    echo "</div>";
}

function ogk_single_product_section_start() {
    echo '<section class="product-top-section bg-offWhite"><div class="container">';
}

function ogk_single_product_section_end() {
    echo '</div></section>';
}

/** =========================================================================================== **/

// Remove product data tabs & Related products
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

function cw_remove_quantity_fields($return, $product) {
    return true;
}

/** =========================================================================================== **/

/**
 * Remove the breadcrumbs
 */
add_action('init', 'woo_remove_wc_breadcrumbs');
function woo_remove_wc_breadcrumbs() {
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
}


/** =========================================================================================== **/


// Remove sidebar
add_action('wp', 'ogk_remove_sidebar_product_pages');

function ogk_remove_sidebar_product_pages() {
    if ( is_product() ) {
        remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
    }
}

/** =========================================================================================== **/


// Add price to button
add_filter('woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text');
function woocommerce_custom_single_add_to_cart_text() {
    global $product;
    return __('Add to Cart', 'woocommerce');
}


/** =========================================================================================== **/


// Single Product Template
add_filter('woocommerce_before_single_product_summary', 'woocommerce_after_single_product');
function woocommerce_after_single_product() {
    get_template_part('includes/template', 'woo-product');
}


/** =========================================================================================== **/


add_action('wp_footer', 'bbloomer_cart_refresh_update_qty');

function bbloomer_cart_refresh_update_qty() {
    if ( is_cart() ) {
        ?>
        <script type="text/javascript">
            jQuery('div.woocommerce input.qty').on('change', function () {
                jQuery("button[name='update_cart']").trigger("click");
            });
        </script>
        <?php
    }
}