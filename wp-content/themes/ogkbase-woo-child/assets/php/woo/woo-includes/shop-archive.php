<?php
/** =========================================================================================== **/
/** =============================   SHOP ARCHIVE FUNCTIONS  =================================== **/
/** =========================================================================================== **/


// Remove breadcrumbs
add_action('template_redirect', 'remove_shop_breadcrumbs');
function remove_shop_breadcrumbs() {

  if ( is_shop() )
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

}

/** =========================================================================================== **/


// Remove sidebar
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);


/** =========================================================================================== **/

// Change shop loop title heading tag
add_filter('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);

add_filter('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);

function woocommerce_template_loop_product_title() {
  echo '<h4 class="product-flavor">' . get_field('product_flavor') . '</h4>';
  echo '<h4 class="woocommerce-loop-product__title"><span>' . get_the_title() . '</span></h4>';
}

/** =========================================================================================== **/

// Remove "Removed from cart. Undo?"
add_action('woocommerce_before_shop_loop', 'sw_delete_remove_product_notice', 5);

function sw_delete_remove_product_notice() {
  $notices = WC()->session->get('wc_notices', array());
  if ( isset($notices['success']) ) {
    for ($i = 0; $i < count($notices['success']); $i++) {
      if ( strpos($notices['success'][$i], __('removed', 'woocommerce')) !== false ) {
        array_splice($notices['success'], $i, 1);
      }
    }
    WC()->session->set('wc_notices', $notices['success']);
  }
}


/** =========================================================================================== **/

// Remove Add to Cart button on shop loop
add_action('init', 'remove_loop_button');
function remove_loop_button() {
  remove_action('woocommerce_after_shop_loop_item',
    'woocommerce_template_loop_add_to_cart', 10);
}

/** =========================================================================================== **/

// Replace Add to Cart button on shop loop with normal button
add_action('woocommerce_after_shop_loop_item', 'replace_add_to_cart');
function replace_add_to_cart() {
  global $product;
  $link = $product->get_permalink();
  echo '<br><div class="btn-wrap"><a class="btn" href="' . $link . '">View Details</a></div>';
}