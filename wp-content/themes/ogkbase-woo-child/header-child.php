<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title><?=  bloginfo( 'title' ); ?></title>
    <meta name="viewport" content="width=device-width">

    <?php $gtm_id = get_field( 'tag_manager_id', 'options' );
    if($gtm_id): ?>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', '<?= $gtm_id ?>');</script>
        <!-- End Google Tag Manager -->
    <?php endif; ?>

    <?php wp_head(); ?>


    <script>
        // Set the options to make LazyLoad self-initialize
        window.lazyLoadOptions = {
            elements_selector: ".lazy",
            // ... more custom settings?
        };
        // Listen to the initialization event and get the instance of LazyLoad
        window.addEventListener('LazyLoad::Initialized', function (event) {
            window.lazyLoadInstance = event.detail.instance;
        }, false);
        function checkWebP(callback) {
            var webP = new Image();
            webP.onload = webP.onerror = function () {
                callback(webP.height == 2);
            };
            webP.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
        };
        checkWebP(function (support) {
            if (support) {
                jQuery(document.body).addClass('webp');
            }else{
                jQuery(document.body).addClass('no-webp');
            }
        });
    </script>

    <link rel="stylesheet" href="https://use.typekit.net/wjs4zrp.css">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#1874d1">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">


</head>


<body <?php body_class(); ?>>

<?php if($gtm_id): ?>
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=<?= get_field( 'tag_manager_id', 'options' ) ?>"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
<?php endif; ?>

<header class="container-large flex flex-sb flex-ac">
    <div class="logo">
        <a href="<?php echo get_home_url(); ?>" title="<?= wp_title() ?>">
            <?php if ( get_field( 'logo', 'options' ) ): ?>
                <?= file_get_contents(get_field( 'logo', 'options' )); ?>
            <?php else: ?>
                <h2><?= wp_title(); ?></h2>
            <?php endif; ?>
        </a>
    </div><!--/logo-->
    <div class="menu-main-menu-container">
        <?php
        wp_nav_menu( array(
            'menu' => 'main-menu'
        ) );
        ?>
    </div>
    <div class="header-right flex flex-sb flex-ac">
        <div class="buttons flex flex-ac">
            <div class="header-phone btn-wrap">
                <a href="tel:<?= get_field('phone', 'options') ?>" class="btn small outline">Call Us <?= get_field('phone', 'options') ?></a>
            </div>
            <div class="header-button btn-wrap">
                <a href="/request-form" class="btn small">Book Mobile Visit</a>
            </div>
        </div>
    </div>
</header>
<main class="site-content">