<?php
$articleCat = get_the_category($post);
$footerPosts = get_posts(array(
    'post_type' => 'post',
    'numberposts' => 2,
    'order' => 'ASC',
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $articleCat[0]->term_id,
            'include_children' => false
        )
    )
));
?>
<?php $previous = get_previous_post(); ?>
<?php $next = get_next_post(); ?>
<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
    <section class="gradient-hero-section flex flex-ac <?php if ($category_image == true) echo 'category_image'; ?>">
        <div class="container" style="min-height: 400px; max-width:700px">
            <div class="iv-therapy flex flex-ac flex-jc">
<!--                --><?php //if($articleCat > 0): echo 'multiple-cats'?>
                <?= file_get_contents(get_field('category_image', $articleCat[0])); ?>
                <div class="category"><?= the_category($post) ?></div>
<!--                --><?php //else : echo '1 cat';   endif; ?>
            </div>
            <h1 class="tc-wht h2 ta-center"><?= get_the_title(); ?></h1>
        </div>
    </section>

    <section class="blog-content">
        <div class="blog-container flex-col flex-jc">
            <div class="blog-image flex-flex-ac"><?php echo the_post_thumbnail('full'); ?></div>
            <div class="content">
                <?= apply_filters('the_content', $post->post_content); ?>
            </div>
            <div class="post-buttons flex flex-sb">
                <?php if ($previous): ?>
                    <a href="<?= get_permalink($previous) ?>" class="left flex flex-ac benefit-text">Older</a>
                <?php endif; ?>
                <?php if ($next): ?>
                    <a href="<?= get_permalink($next) ?>" class="right flex flex-ac benefit-text">Newer</a>
                <?php endif; ?>
            </div>
        </div>


        <div class="footer-posts  bg-offWhite">
            <div class="container flex flex-sb">
                <?php foreach ($footerPosts as $post) {
                    include(locate_template('includes/single-article-template.php', false, false));
                } ?>
            </div>
        </div>


    </section>

<?php endwhile; ?>
<?php get_footer(); ?>
