<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<title><?= get_the_title() ?> | <?= get_bloginfo( 'title' ); ?></title>
	<meta name="viewport" content="width=device-width">

    <meta name="facebook-domain-verification" content="w38o4iuleivw6juv7hbgjr789z5dsk" />

	<?php wp_head(); ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-P8JR4Z2');</script>
    <!-- End Google Tag Manager -->

    <!--    Global site tag (gtag.js) - Google Analytics-->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127342191-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-127342191-1');
        gtag('config', 'AW-810146244');
    </script>

    <script>
        gtag('config', 'AW-810146244/9XzDCKWw_ewBEMSzp4ID', {
            'phone_conversion_number': '<?= get_field('phone', 'options') ?>'
        });
    </script>


    <link rel="stylesheet" href="https://use.typekit.net/xqo2ruy.css">

    <!--    this is font-->
    <link rel="stylesheet" href="https://use.typekit.net/wjs4zrp.css">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#1874d1">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">


    <style>
        :root {
            --woo-skin-color: <?= get_field('woo_skin_color', 'options') ?>;
            --cart-content-color: <?= get_field('cart_content_color', 'options') ?>;
            --cart-text-color: <?= get_field('cart_text_color', 'options') ?>;
            --cart-bg-color: <?= get_field('cart_bg_color', 'options') ?>;
            --cart-counter-color: <?= get_field('cart_counter_color', 'options') ?>;
            --cart-counter-bg-color: <?= get_field('cart_counter_bg_color', 'options') ?>;
            --inactive-color: <?= get_field('inactive_color', 'options') ?>;
            --checkout-bg-color: <?= get_field('checkout_bg_color', 'options') ?>;
            --checkout-shipping-inactive-color: <?= get_field('checkout_shipping_inactive_color', 'options') ?>;
        }
    </style>


</head>

<body <?php body_class(); ?>>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8JR4Z2"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php get_template_part("includes/mini-cart") ?>


<?php if(!is_page('Checkout')): ?>
<header class="container-large">
    <div class="logo">
        <a href="/" alt="Home link">
            <?php if (get_field('logo', 'options')): ?>
                <?= file_get_contents(get_field('logo', 'options')); ?>
            <?php else: ?>
                <h2><?= wp_title(); ?></h2>
            <?php endif; ?>
        </a>
    </div>
    <div class="header-left desktop">
        <?php
        wp_nav_menu(array(
            'menu' => 'main-menu'
        ));
        ?>
    </div>
    <div class="header-right desktop">

        <div class="buttons flex">
            <div class="header-phone btn-wrap ">
                <a href="tel:<?= get_field('phone', 'options') ?>" class="btn small outline"
                   onclick="gtag('event', 'Button-Click', { 'event_category' : 'Call-Click', 'event_label': 'Call-CTA-Header'});">Call Us <?= get_field('phone', 'options') ?></a>
            </div>
            <div class="header-button btn-wrap init-popup">
                <a href="/request-form" class="btn small"
                   onclick="gtag('event', 'Button-Click', { 'event_category' : 'Request-Form', 'event_label': 'Book-Mobile-Visit-CTA-Header'});">Book Mobile Visit</a>
            </div>
        </div>

        <?php if(is_user_logged_in()): ?>
            <a href="<?= wc_get_page_permalink( 'myaccount' ); ?>" class="other-link">Account</a>
        <?php else: ?>
            <a href="<?= wc_get_page_permalink( 'myaccount' ); ?>" class="other-link">Login</a>
        <?php endif; ?>
        <?php
        $cart_count = WC()->cart->get_cart_contents_count();
        $cart_total = WC()->cart->get_cart_total(); ?>
        <div class="header-cart-count">
            <div class="icon">
                <div class="cart-count-number">
                    <span class="cart-count-number-span"><?= WC()->cart->get_cart_contents_count() ?></span>
                </div>
            </div>
        </div>
        <div class="header-hamburger mobile" style="display: none;">
            <div class="hamburger">
                <span></span>
                <span></span>
            </div>
        </div>
	</div>
    <div class="nav-menu">
        <div class="container">
            <div class="menu-mobile-menu-container">
                <ul id="menu-mobile-menu" class="menu">

                    <?php
                    $menu_items = wp_get_nav_menu_items('mobile-menu');


                    foreach ($menu_items as $menu_item) { ?>
                        <?php if ($menu_item->menu_item_parent == 0) { ?>

                            <li class="menu-item"><a href="<?= $menu_item->url; ?>"><?= $menu_item->title; ?></a>
                                <ul class="sub-menu">
                                    <?php foreach ($menu_items as $mi) {
                                        if ($menu_item->ID == $mi->menu_item_parent) { ?>
                                            <li class="menu-item-has-terms">
                                                <a href="<?= $mi->url; ?>">
                                                    <?php
                                                    echo($mi->title);
                                                    $terms = get_the_terms($mi->object_id, 'page_tags'); ?>
                                                    <div class="terms">
                                                        <?php
                                                        $termCount = count($terms);
                                                        $index = 1;
                                                        foreach ($terms as $term) {
                                                            if ($index < $termCount) {
                                                                echo '<span>' . $term->name . ', </span>';
                                                            } else {
                                                                echo '<span>' . $term->name . '</span>';
                                                            }
                                                            $index++;
                                                        }
                                                        ?>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php }
                                    } ?>
                                </ul>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        <div class="chunky-links">
            <?php if (is_user_logged_in()): ?>
                <a href="<?= wc_get_page_permalink('myaccount'); ?>" class="chunky-link">Account</a>
            <?php else: ?>
                <a href="<?= wc_get_page_permalink('myaccount'); ?>" class="chunky-link">Login</a>
            <?php endif; ?>
            <a href="<?= wc_get_page_permalink('shop'); ?>" class="chunky-link">Shop</a>
        </div>
    </div>
    </div>
</header>
<section class="mobile-buttons" style="display:none;">
        <a href="tel:<?= get_field('phone', 'options') ?>" class="mobile-nav-but call">Call Us <?= get_field('phone', 'options') ?></a>
        <a href="/request-form" class="mobile-nav-but">Book Mobile Visit</a>
</section>
<?php else: ?>
    <?php
    $cart_count = WC()->cart->get_cart_contents_count();
    $cart_total = WC()->cart->get_cart_total(); ?>


<header class="checkout-header">
    <div class="container">
        <div class="left">
            <a href="<?= get_permalink( get_page_by_title( 'Shop All' ) )?>">Keep Shopping</a>
        </div>
        <div class="right">
            <div class="cart-total">
                <?= $cart_total ?>
            </div>
            <div class="cart-count header-cart-count cart-count-number">
              <span class="cart-count-number-span"><?= $cart_count ?></span>
            </div>
        </div>
    </div>
</header>

<?php endif; ?>
<main class="site-content">





