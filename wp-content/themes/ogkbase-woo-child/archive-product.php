<?php get_header(); ?>


<?php $post = get_post(get_option( 'woocommerce_shop_page_id' ));?>
<?php get_template_part("includes/flexible" ); ?>
<?php if(strlen(get_the_content())): ?>
    <div class="container content-container">
        <?php the_content(); ?>
    </div>
<?php endif; ?>
<?php get_footer(); ?>