<section class="basic-hero-section flex flex-ac" style="background: url(<?= get_sub_field('basic_hero_bg') ?>) no-repeat center / cover;">
    <div class="container">
        <div class="fifty">
            <h1 class="tc-wht inview-fadeUp"><?= get_sub_field('basic_hero_title') ?></h1>
            <div class="btn-wrap inview-fadeUp">
                <a href="<?= get_sub_field('button_link') ?>" class="btn" id="basic-hero-cta-<?= get_row_index() ?>-<?= get_the_ID() ?>"><?= get_sub_field('button_text') ?></a>
            </div>
        </div>
    </div>
</section>