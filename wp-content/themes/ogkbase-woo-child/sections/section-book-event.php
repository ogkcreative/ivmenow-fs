<section class="book-event-section">
    <div class="container">
        <div class="h2 tc-prim ta-center"><?= get_sub_field('title')?></div>
        <?php if (have_rows('event')):?>
        <div class="events flex flex-sb">
            <?php while(have_rows('event')): the_row();?>
            <div class="event flex-col flex-jc ta-center inview-fadeUp">
                    <div class="image"><?= wp_get_attachment_image(get_sub_field('image'), 'full') ?></div>
                    <div class="small-title"><?= get_sub_field('text')?></div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif;?>
        <div class="btn-wrap flex-jc inview-fadeUp">
            <a href="<?= get_sub_field('button_link') ?>" class="btn" id="book-event-cta-<?= get_row_index() ?>-<?= get_the_ID() ?>"><?= get_sub_field('button_text') ?></a>
        </div>
    </div>
</section>