<section class="contact-form-section">
    <div class="container flex flex-sb">
        <div class="lets-talk forty">
            <div class="card-single flex flex-ac">
                <?php if (have_rows('contact_card')): ?>
                    <div class="article-single">
                        <?php while (have_rows('contact_card')):the_row(); ?>
                            <div class="article-details flex flex-col flex-sb">
                                <div class="h2 tc-prim"><?= get_sub_field('title') ?></div>
                                <div class="excerpt benefit-text"><?= get_sub_field('content') ?></div>
                                <div class="line-seperator"></div>
                                <div class="subtitle small-title"><?= get_sub_field('subtitle') ?></div>
                                <div class="contact-fields flex flex-col tc-prim">
                                    <a class="benefit-text tc-prim" href="<?= get_sub_field('phone_number') ?>" id="contact-call-button">Call (561) 706-0448</a>
                                    <a class="benefit-text tc-prim" href="<?= get_sub_field('email') ?>" id="contact-email-button">Email info@ivmenowfl.com</a>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="contact-form forty">
            <div class="h2 tc-prim"><?=get_sub_field('form_title')?></div>
            <?= get_sub_field('form_wysiwyg') ?>
        </div>
    </div>

</section>