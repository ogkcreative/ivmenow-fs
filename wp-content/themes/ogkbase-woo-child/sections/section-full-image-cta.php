<section class="full-image-cta-section">
    <div class="gradient"></div>
    <div class="mobile-image">
        <?= wp_get_attachment_image(get_sub_field('image'), 'full') ?>
    </div>
    <div class="container flex flex-ac">
        <div class="text-wrap fifty">
            <h1 class="h2 tc-prim inview-fadeUp"><?= get_sub_field('title') ?></h1>
            <h2 class="small-title tc-prim inview-fadeUp"><?= get_sub_field('subtitle') ?></h2>
            <p class="small-title tc-prim inview-fadeUp"><?= get_sub_field('text') ?></p>
            <div class="small-title tc-prim inview-fadeUp"><?= get_sub_field('list_title') ?></div>
            <?php if (have_rows('list')): ?>
                <ul class="checked-list inview-fadeUp">
                    <?php while (have_rows('list')): the_row(); ?>
                        <li class="inview-fadeUp"><?= get_sub_field('list_item') ?></li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
            <div class="btn-wrap inview-fadeUp">
                <a class="btn-arrow" href="<?= get_sub_field('button_link') ?>" id="full-image-cta-<?= get_row_index() ?>-<?= get_the_ID() ?>"><?= get_sub_field('button_text') ?></a>
            </div>
        </div>
    </div>
</section>