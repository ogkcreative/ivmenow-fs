<?php
    $type = get_sub_field('type');
    $amnt = get_sub_field('quantity_per_page');
    $qStr = $type . '_filters';
    $filters = null;
    $postTemp = $post;
?>
    <script>
        var docTitle = '<?= $postTemp->post_title ?>';
        document.title = docTitle;
    </script>
<?php
    if(get_sub_field($qStr)){
        $filters = get_sub_field($qStr);
    };

    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'orderby' => 'date',
        'post_type' => $type,
        'posts_per_page' => $amnt,
        'paged'    => $paged,
        'post_status'     => 'publish'
    );

    $allBol = false;
    $getTax = null;
    $getTerm = null;

    if ( $type == 'product' ) {
        $args['paged'] = null;
    }

    if(strlen($_GET["tax"]) > 0 && strlen($_GET["term"]) > 0 ){
        //adjust args arr with tax query;
        $getTax = $_GET["tax"];
        $getTerm = $_GET["term"];
        $args['tax_query'] = array(
                array(
                    'taxonomy' => $getTax,
                    'field' => 'term_id',
                    'terms' => $getTerm,
                    'include_children' => false
                )
        );

    }
    elseif(count($filters) == 1){
        $args['tax_query'] = array(
            array(
                'taxonomy' => $filters[0]->taxonomy,
                'field' => 'term_id',
                'terms' => $filters[0]->term_id,
                'include_children' => false
            )
        );
    }
    else {
        $allBol = true;
    }

    $the_query = new WP_Query( $args );
?>

<section class="archive-section archive-<?= $type; ?> flex-col ">
    <?php if(count($filters) > 1){ ?>
        <div class="filters-container">
                <a href="<?= get_the_permalink();?>?all=true" class="filter-button clear-filter <?php if($allBol == true){ echo 'active'; }?>">All <?php if( $type == 'product' ): ?>Products<?php else: ?>Topics<?php endif; ?></a>
            <?php foreach ($filters as $filter){ ?>
                <a class="filter-button <?php if($getTerm == $filter->term_id ){ echo 'active'; }?>" href="<?= get_the_permalink();?>?tax=<?= $filter->taxonomy;?>&term=<?= $filter->term_id; ?>" data-termid="<?= $filter->term_id; ?>" data-slug="<?= $filter->slug;?>" data-tax="<?= $filter->taxonomy;?>">
                    <?php if(get_field('category_image', $filter)){ ?>
                      <?=file_get_contents(get_field('category_image', $filter))?>
                    <?php } ?>
                    <?php if($type == 'product'){
                        $idcat = $filter->term_id;
                        $thumbnail_id = get_term_meta ( $idcat, 'thumbnail_id', true );
                        $image = wp_get_attachment_url( $thumbnail_id );
                        echo file_get_contents($image);
                    }?>
                    <?= $filter->name; ?></a> <?php }?>
        </div>

        <div class="mobile-filters-container">
            <div class="filters-container">
                <div class="mobile-filter-trigger">
                    <a class="select current-item filter-button clear-filter" id="filter-text">Choose A Topic</a>
                </div>
                <div class="dropdown-list">
                    <div class="filter-trigger">
                        <a href="?all=true" class="filter-button <?php if($allBol == true){ echo 'active'; }?>">All <?php if( $type == 'product' ): ?>Products<?php else: ?>Topics<?php endif; ?></a>
                    </div>
                <?php foreach ($filters as $filter): ?>
                    <div class="filter-trigger">
                        <a class="filter-button <?php if($getTerm == $filter->term_id ){ echo 'active'; }?>" href="?tax=<?= $filter->taxonomy;?>&term=<?= $filter->term_id; ?>" data-termid="<?= $filter->term_id; ?>" data-slug="<?= $filter->slug;?>" data-tax="<?= $filter->taxonomy;?>">
                            <?php if(get_field('category_image', $filter)){ ?>
                                <?=file_get_contents(get_field('category_image', $filter))?>
                            <?php } ?>
                            <?php if($type == 'product'){
                                $idcat = $filter->term_id;
                                $thumbnail_id = get_term_meta ( $idcat, 'thumbnail_id', true );
                                $image = wp_get_attachment_url( $thumbnail_id );
                                echo file_get_contents($image);
                            }?>
                            <?= $filter->name; ?></a>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="container archive-item-container flex flex-wrap inview-fadeUp">
        <?php if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) :
                $the_query->the_post();

                if($type == 'post'){
                    include(locate_template('includes/single-article-template.php', false, false));
                } elseif ($type == 'page' || $type == 'product'){
                    include(locate_template('includes/single-card-template.php', false, false));
                }
            endwhile; ?>
            <?php
            $post = $postTemp;
            ?>
        <?php endif; ?>

    </div>
    <?php if($type == 'post'): ?>
    <div class="pagination-container container">
        <?php
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $the_query->max_num_pages,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 1,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => sprintf( '<i class="fas fa-chevron-left"></i> %1$s', __( 'Prev Page', 'text-domain' ) ),
            'next_text'    => sprintf( '%1$s <i class="fas fa-chevron-right"></i>', __( 'Next Page', 'text-domain' ) ),
            'add_args'     => false,
            'add_fragment' => '',
        ) );
        ?>
        <?php endif; ?>
    </div>
</section>