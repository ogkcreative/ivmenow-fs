<?php
    //create defaults
    global $product;
    if($post->ID == 2410){
        $product = wc_get_product(1708);
        $post->post_type = "product";
    }
    $isProd = false;
    $price = '';
    $terms = '';
    $index = 1;
    $termCount = 0;
    if($post->post_type == "product"){
        global $product;
        $isProd = true;
        $price = $product->get_price();
    } else {
        $price = get_field('service_price');
        $terms = get_the_terms($post, 'page_tags');
        $termCount = count($terms);
    }
?>

<section class="cta-book-cart-section flex">
    <div class="image forty">
        <?= wp_get_attachment_image(get_sub_field('image'), 'full') ?>
    </div>
    <div class="text sixty padding">
        <div class="text-wrap inview-fadeUp">
            <h1 class="h1"><?= get_sub_field('title') ?></h1>
            <h2 class="small-title tc-prim"><?= get_sub_field('subtitle') ?></h2>
            <p><?= get_sub_field('text') ?></p>
            <div class="small-title tc-prim"><?= get_sub_field('list_title')?></div>
            <?php if (have_rows('list')): ?>
                <ul class="checked-list">
                    <?php while (have_rows('list')): the_row(); ?>
                        <li><?= get_sub_field('list_item') ?></li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
            <?php if(!$isProd): ?>
            <div class="tc-prim ingredients">
                <div class="small-title tc-prim">Ingredients</div>
                    <?php foreach($terms as $term):?>
                        <?php if($index < $termCount){
                            echo '<span>'. $term->name . ', </span>';
                        } else {
                            echo '<span>'. $term->name . '</span>';
                        }?>
                        <?php $index++ ?>

                    <?php endforeach; ?>
            </div>
                <?php if(get_field('variants')): ?>
                <div class="ingredients">
                    <?php $variArr = get_field('variants'); ?>
                    <table class="variations" cellspacing="0">
                        <tbody>

                            <tr>
                                <td class="label"><label for="<?php echo esc_attr( sanitize_title( $vari['variant_title'] ) ); ?>"><?php echo wc_attribute_label( $vari['variant_title'] ); // WPCS: XSS ok. ?></label></td>
                                <td class="value">
                                    <select id="price-option" onchange="serviceVariantPriceHandler(this)">
                                        <?php foreach ( $variArr as $vari ) :

                                             echo '<option value="'.$vari['variant_price'].'">'.$vari['variant_title'].'</option>';

                                         endforeach; ?>
                                    </select>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <?php endif; ?>

            <?php endif; ?>

            <?php if( is_a( $product, 'WC_Product_Variable' ) ): ?>
                <div class="variable-options">
                    <?php do_action('woocommerce_single_product_summary'); ?>
                </div>
            <?php endif; ?>
            <?php if( is_a($product, 'WC_Product_Simple') || !$isProd ): ?>
                <div class="qty-container inview-fadeUp">
                    <div class="qty-btn" data-increment="-1">-</div>
                    <input class="single-quantity" type="text" name="quantity" value="1" readonly />
                    <div class="qty-btn" data-increment="1">+</div>
                </div>
            <?php endif; ?>
            <div class="book-box flex inview-fadeUp">

                <?php if( is_a($product, 'WC_Product_Simple') || !$isProd ): ?>
                    <div class="price price-single h1" data-price="<?= $price; ?>">$<?= $price; ?></div>
                <?php endif; ?>

                <div class="init-popup btn-wrap">
                    <?php if(!$isProd && get_field('variants') ): ?>
                        <a href="/request-form" onclick="serviceActiveVariant('<?= get_sub_field('title'); ?>')" class="btn" >Book Now</a>
                    <?php elseif(!$isProd &&  !get_field('variants') ): ?>
                        <a href="/request-form" onclick="setActiveItem('<?= get_sub_field('title'); ?>','<?= get_field('service_price'); ?>')" class="btn" >Book Now</a>
                    <?php elseif( is_a( $product, 'WC_Product_Simple' ) ): ?>
                        <button type="submit" value="<?= $post->ID; ?>" name="add-to-cart"  class="single_add_to_cart_button btn" >Add to Cart</button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>