<section class="small-image-text-section">
    <div class="container flex flex-sb flex-ac <?= get_sub_field('row_direction')?>">
        <div class="img thirty-five inview-fadeUp">
            <?= wp_get_attachment_image(get_sub_field('image'), 'full') ?>
        </div>
        <div class="text fifty inview-fadeUp">
            <div class="h2 tc-prim"><?= get_sub_field('title')?></div>
            <div class="tc-prim content"><?= get_sub_field('content')?></div>
        </div>
    </div>
</section>