<section class="card-slider-section inview-fadeUp">
    <?php if(get_sub_field('card_title_wysiwyg') && get_sub_field('cards_title')): ?>
    <div class="container flex flex-wrap">
        <div class="fifty flex flex-ac flex-jc">
            <h2 class="tc-prim"><?= get_sub_field('cards_title');?>
                <?php if(get_sub_field('cards_cta')):?>
                    <a href="<?= get_sub_field('cards_cta_link');?>"><?= get_sub_field('cards_cta'); ?></a>
                <?php endif; ?>
            </h2>
        </div>
        <div class="fifty flex flex-ac flex-jc">
            <?= get_sub_field('card_title_wysiwyg'); ?>
        </div>
        <?php if(get_sub_field('button_text')):?>
        <div class="btn-wrap expert-button">
            <a href="/request-form" class="btn"><?= get_sub_field('button_text')?></a>
        </div>
        <?php endif; ?>
        <?php if(get_sub_field('expert_image')):?>
<!--        <div class="expert-image">-->
<!--            <a href="/request-form">-->
<!--                --><?//= wp_get_attachment_image(get_sub_field('expert_image', 'full'));?>
<!--            </a>-->
<!--        </div>-->
        <?php endif; ?>
    </div>
    <?php elseif(get_sub_field('cards_title')): ?>
    <div class="container">
        <h2 class="small-title text-center tc-prim"><?= get_sub_field('cards_title')?>
            <?php if(get_sub_field('cards_cta')):?>
                <a href="<?= get_sub_field('cards_cta_link');?>"><?= get_sub_field('cards_cta'); ?></a>
            <?php endif; ?>
        </h2>
    </div>
    <?php endif; ?>
    <?php
    $cardsData= get_sub_field('cards_data');
    //    var_dump($cardsData);3
    if($cardsData): ?>
        <div class="services card-slider owl-carousel">
            <?php foreach($cardsData as $post): setup_postdata($post); ?>
                <?php get_template_part("includes/single-card-template"); ?>
            <?php endforeach; wp_reset_postdata(); ?>
        </div>
    <?php endif; ?>

</section>