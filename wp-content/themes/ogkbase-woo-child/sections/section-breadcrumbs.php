<?php
$breadArr = [];
if(get_sub_field('custom_path') == false){
    if(get_post_parent()){
        $breadArr = [get_post_parent(), $post ];
    } else {
        $breadArr =[get_post(get_option( 'page_on_front' )), $post ];
    }

} elseif ( count(get_sub_field('custom_path')) > 0){
    $breadArr = get_sub_field('custom_path');
} else{
    echo 'no bread';
    die();
}
$breadHtml = '';

for($i=0; $i<count($breadArr); $i++){

    $breadHtml .= '<a href="'.get_the_permalink($breadArr[$i]).'">'.$breadArr[$i]->post_title.'</a>';
    if($i+1<count($breadArr)){
        $breadHtml .= ' <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="7.206" height="11.584" viewBox="0 0 7.206 11.584">
                                <path id="Path_245" data-name="Path 245" d="M8591-890.036l5.085-5.085L8591-900.206" transform="translate(-8590.297 900.913)" fill="none" stroke="#0c2664" stroke-width="2"/>
                            </svg>
                        </span> ';
    }
}
?>
<section class="container breadcrumb-section">
    <div class="bred-items-container"><?php echo $breadHtml; ?></div>
    <?php if(get_sub_field('include_search')): ?>
        <div class="search-container"><?php get_search_form(); ?>
            <svg onclick="document.getElementById('searchsubmit').click();" xmlns="http://www.w3.org/2000/svg" width="22.349" height="17.62" viewBox="0 0 22.349 17.62">
                <path id="Path_30" data-name="Path 30" d="M-1894.613,3327.63l-1.244,1.244,6.686,6.686h-18.981v1.76h18.981l-6.686,6.686,1.244,1.244,8.81-8.81Z" transform="translate(1908.152 -3327.63)" fill="#0246A8"/>
            </svg>
        </div>
    <?php endif; ?>
</section>
