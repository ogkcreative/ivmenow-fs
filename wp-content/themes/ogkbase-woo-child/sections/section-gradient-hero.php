<section class="gradient-hero-section">
    <div class="container flex-col flex-ac">
        <h1 class="tc-wht h2 ta-center"><?= get_sub_field('gradient_hero_title') ?></h1>
        <div class="subtitle ta-center small-title tc-wht">
            <?= get_sub_field('gradient_hero_subtitle') ?>
        </div>
    </div>
</section>