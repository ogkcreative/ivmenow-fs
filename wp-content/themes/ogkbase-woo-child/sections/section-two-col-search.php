<section class="two-col-search">
    <div class="container flex flex-wrap">
        <div class="fifty">
            <?= get_sub_field('text_block_area'); ?>
        </div>
        <div class="fifty flex">
            <div class="search-container">
                <?php get_search_form(); ?>
                <svg onclick="console.log('test'); document.getElementById('searchsubmit').click();" xmlns="http://www.w3.org/2000/svg" width="22.349" height="17.62" viewBox="0 0 22.349 17.62">
                    <path id="Path_30" data-name="Path 30" d="M-1894.613,3327.63l-1.244,1.244,6.686,6.686h-18.981v1.76h18.981l-6.686,6.686,1.244,1.244,8.81-8.81Z" transform="translate(1908.152 -3327.63)" fill="#0246A8"/>
                </svg>
            </div>
        </div>
    </div>
</section>