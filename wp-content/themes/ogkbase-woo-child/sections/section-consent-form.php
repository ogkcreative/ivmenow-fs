<section class="consent-form-section">
    <div class="container">
        <div class="h2 tc-prim"><?= get_sub_field('form_title')?></div>
        <div class="consent-form">
            <?= get_sub_field('form_wysiwyg');?>
        </div>

    </div>
</section>