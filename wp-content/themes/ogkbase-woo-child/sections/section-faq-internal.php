<style>
    body .site-content{
        overflow: unset;
    }
    .container{
        overflow: unset;
    }
</style>

<section class="faq-internal-section">
    <div class="container flex flex-sb">
        <div class="category">
            <h1 class="small-title q"><?= get_sub_field('title') ?></h1>
            <?php if (have_rows('questions')): ?>
            <ul class="questions">
                <?php while (have_rows('questions')):the_row(); ?>
                     <a href="#answer-<?= get_sub_field('question_number') ?>" data-target="answer-<?= get_sub_field('question_number') ?>"
                        class="question tc-prim small-text" id="question-<?= get_sub_field('question_number') ?>">
                         <li><?= get_sub_field('question') ?></li>
                    </a>

                <?php endwhile; ?>
            </ul>
        </div>
        <?php endif; ?>

        <div class="q-and-a">
            <?php if (have_rows('questions')): ?>
                <ul class="<?= get_sub_field('title') ?> questions-answers">
                    <?php while (have_rows('questions')):the_row(); ?>
                    <div class="answer-anchor" id="answer-<?= get_sub_field('question_number') ?>"></div>
                        <li class="answer inview-fadeUp">
                            <p class="h2 tc-prim"><?= get_sub_field('question') ?></p>
                            <p><?= get_sub_field('answer') ?></p>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>

        </div>
    </div>
    </div>

</section>