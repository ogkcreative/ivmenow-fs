<section class="schedule-home-visit-section bg-offWhite">
    <div class="container">
        <?php if(get_sub_field('title')):?>
        <div class="small-title tc-prim flex flex-jc inview-fadeUp"><?= get_sub_field('title') ?></div>
        <?php endif; ?>
        <?php if(get_sub_field('title')):?>
        <div class="med-title tc-prim ta-center inview-fadeUp"><?= get_sub_field('subtitle')?></div>
        <?php endif; ?>
        <?php if (have_rows('step')): ?>
            <div class="steps flex flex-sb inview-fadeUp">
                <?php while (have_rows('step')): the_row(); ?>
                    <div class="step inview-fadeUp">
                        <div class="image">
                            <div class="gradient-bar"></div>
                            <img src="<?= get_sub_field('image')['url'] ?>" alt="<?= get_sub_field('image')['alt'] ?>">
                        </div>
                        <div class="number-wrap">
                            <div class="h2 tc-prim number"><?= get_sub_field('number') ?></div>
                        </div>
                        <div class="h3 tc-prim ta-center"><?= get_sub_field('text') ?></div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <?php $button = get_sub_field('button');
        if( $button ): ?>
            <div class="btn-wrap flex-jc inview-fadeUp">
                <a href="<?= $button['url'] ?>" class="btn" id="schedule-home-visit-<?= get_row_index() ?>-<?= get_the_ID() ?>"><?= $button['title'] ?></a>
            </div>
        <?php endif; ?>
    </div>
</section>