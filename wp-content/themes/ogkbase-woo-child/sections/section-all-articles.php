<?php
$articleCat = get_sub_field('article_data');
$articleAmnt = get_sub_field('article_amount');

// $articleData = get_posts(array(
//     'post_type' => 'post',
//     'numberposts' => $articleAmnt,
//     'order' => 'ASC',
//     'tax_query' => array(
//         array(
//             'taxonomy' => 'category',
//             'field' => 'term_id',
//             'terms' => $articleCat[0]->term_id,
//             'include_children' => false
//         )
//     )
// ));


$articleData = array(
	'post_type' => 'post',
    'numberposts' => $articleAmnt,
    'order' => 'ASC',
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $articleCat[0]->term_id,
            'include_children' => false
        )
    )
);

$articles = new WP_Query($articleData);

?>

<section class="all-articles-section <?php if(get_sub_field('article_bg')){ echo 'bg-offWhite'; } ?>">
    <div class="container">
        <div class="h2 tc-prim ta-center inview-fadeUp"><?= get_sub_field('title')?></div>
        <div class="articles flex flex-sb">
			
			<?php if($articles->have_posts()): ?>
				<?php while($articles->have_posts()): $articles->the_post(); ?>
					<?php get_template_part('includes/single-article-template'); ?>
				<?php endwhile; wp_reset_postdata(); ?>
			<?php endif; ?>
			
        </div>
    </div>
</section>
