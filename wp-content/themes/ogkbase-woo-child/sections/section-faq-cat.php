<section class="faq-cat-section <?= get_sub_field('bg_color')?>">
    <div class="container">
        <div class="faq-categories flex-col flex-wrap inview-fadeUp">
            <?php if(have_rows('category', 'options')):?>
                <?php while(have_rows('category', 'options')): the_row();?>
                    <a href="<?= get_sub_field('cat_link') ?>" class="faq-cat small-title">
                        <?= get_sub_field('cat_name')?>
                    </a>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
