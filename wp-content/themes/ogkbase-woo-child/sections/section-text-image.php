<section class="text-image-section">
    <div class="mobile-image"><img src="<?= get_sub_field('image') ?>" alt=""></div>
    <div class="container flex flex-sb flex-ac">
        <div class="img fifty inview-fadeUp">
            <img src="<?= get_sub_field('image') ?>" alt="">
        </div>
        <div class="text forty">
            <div class="h2 tc-prim inview-fadeUp"><?= get_sub_field('title')?></div>
            <div class="h3 tc-prim content inview-fadeUp"><?= get_sub_field('content')?></div>
        </div>
    </div>
</section>