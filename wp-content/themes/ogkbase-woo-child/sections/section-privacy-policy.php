<section class="privacy-policy-section">
    <div class="container flex">
        <div class="related-articles thirty">
            <div class="small-title">Related Articles</div>
            <a class="small-text tc-prim" href="<?= get_field('related_articles', 'options')?>">Terms of Service</a>
        </div>
        <div class="privacy-policy">
            <h1 class="h2 tc-prim">Privacy Policy</h1>
            <?= get_field('content', 'options')?>
        </div>
    </div>
</section>