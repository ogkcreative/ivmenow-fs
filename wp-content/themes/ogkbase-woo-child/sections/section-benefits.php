<section class="benefits-section bg-offWhite">
    <div class="container-large">
        <?php if (get_sub_field('title')):?>
        <div class="h2 ta-center tc-prim inview-fadeUp"><?= get_sub_field('title')?></div>
        <?php endif; ?>
        <?php if (have_rows('benefit')): ?>
            <div class="benefits flex flex-sb flex-jc">
                <?php while (have_rows('benefit')): the_row(); ?>
                    <div class="benefit inview-fadeUp">
                        <?= wp_get_attachment_image(get_sub_field('icon'), 'full') ?>
                        <div class="small-title ta-center"><?= get_sub_field('title') ?></div>
                        <p class="benefit-text ta-center"><?= get_sub_field('subtitle') ?></p>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <?php if(get_sub_field('button_link')):?>
        <div class="btn-wrap flex flex-jc inview-fadeUp">
            <a class="btn" href="<?=get_sub_field('button_link')?>" id="benefits-cta-<?= get_row_index() ?>-<?= get_the_ID() ?>"><?=get_sub_field('button_text')?></a>
        </div>
        <?php endif; ?>
    </div>
</section>