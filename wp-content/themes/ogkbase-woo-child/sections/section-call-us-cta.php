<section class="call-us-cta-section <?= get_sub_field('bg_color')?>">
    <div class="container flex flex-sb flex-ac">
        <div class="img thirty-five inview-fadeUp">
            <?= wp_get_attachment_image(get_sub_field('image'), 'full') ?>
        </div>
        <div class="text fifty inview-fadeUp">
            <div class="h2 tc-prim"><?= get_sub_field('title')?></div>
            <div class="h3 tc-prim content"><?= get_sub_field('content')?></div>
            <div class="btn-wrap inview-fadeUp">
                <a class="btn" href="<?=get_sub_field('button_link')?>" id="call-us-cta-<?= get_row_index() ?>-<?= get_the_ID() ?>"><?=get_sub_field('button_text')?></a>
            </div>
        </div>
    </div>
</section>