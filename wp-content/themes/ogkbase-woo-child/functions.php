<?php
/***
 *
 * OGKBase Woo Child Theme Functions
 *
 */

/**
 * Add Child Theme Styles
 */
add_action( 'wp_enqueue_scripts', 'ogk_base_woo_child_enqueue_styles' );
function ogk_base_woo_child_enqueue_styles() {
    $dist = get_stylesheet_directory_uri() . '/dist/';
    wp_enqueue_style('child-styles', $dist . 'assets/sass/style.css', array('parent-styles'), wp_get_theme()->get('2.0'));
}


/**
 * Add Child Theme Scripts
 */
add_action('wp_enqueue_scripts', 'ogk_base_woo_child_enqueue_scripts', 0);
function ogk_base_woo_child_enqueue_scripts() {

    $dist = get_stylesheet_directory_uri().'/dist';

    wp_register_script('child-scripts', $dist . '/assets/js/scripts-min.js', array('parent-scripts'), wp_get_theme()->get('2.0'), true);
    wp_enqueue_script('child-scripts');
}


/**
 * Add WooCommerce Theme Functions
 */
$incl_path = get_stylesheet_directory() . "/assets/php/woo/";
require_once ($incl_path . 'woo-functions.php');



function page_taxonomy_create() {
    $labels = array(
        'name'              => 'Page Category',
        'singular_name'     => 'Page Category',
        'search_items' =>  __( 'Search Page Categories' ),
        'all_items' => __( 'All Page Categories' ),
        'parent_item' => __( 'Parent Page Category' ),
        'parent_item_colon' => __( 'Parent Page Category:' ),
        'edit_item' => __( 'Edit Page Category' ),
        'update_item' => __( 'Update Page Category' ),
        'add_new_item' => __( 'Add New Page Category' ),
        'new_item_name' => __( 'New Page Category Name' ),
        'menu_name' => __( 'Page Categories' ),
    );
    register_taxonomy('page_category', array('page'),
        array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'has_archive' => true,
            'show_in_rest' => true,
            'rewrite' => array( 'slug' => 'page_category' ),
        )
    );
}
add_action('init', 'page_taxonomy_create', 0);


function page_tags_taxonomy_create() {
    $labels = array(
        'name'              => 'Page Tags',
        'singular_name'     => 'Page Tags',
        'search_items' =>  __( 'Search Page Tags' ),
        'all_items' => __( 'All Page Tags' ),
        'parent_item' => __( 'Parent Page Tags' ),
        'parent_item_colon' => __( 'Parent Page Tags:' ),
        'edit_item' => __( 'Edit Page Tags' ),
        'update_item' => __( 'Update Page Tags' ),
        'add_new_item' => __( 'Add New Page Tags' ),
        'new_item_name' => __( 'New Page Tags Name' ),
        'menu_name' => __( 'Page Tags' ),
    );
    register_taxonomy('page_tags', array('page'),
        array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'has_archive' => true,
            'show_in_rest' => true,
            'rewrite' => array( 'slug' => 'page_tags' ),
        )
    );
}
add_action('init', 'page_tags_taxonomy_create', 0);

function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
add_filter( 'excerpt_length', function($length) {
    return 26;
} );


function html5_search_form( $form ) {
    $form = '<section class="search"><form role="search" method="get" id="search-form" action="' . home_url( '/' ) . '" >
    <label class="screen-reader-text" for="s">' . __('',  'domain') . '</label>
     <input value="' . get_search_query() . '" name="s" id="s" placeholder="Search" />
     <input type="submit" id="searchsubmit" value="'. esc_attr__('Go', 'domain') .'" />
     </form></section>';
    return $form;
}

add_filter( 'get_search_form', 'html5_search_form' );

function woocommerce_button_proceed_to_checkout() {
    $checkout_url = WC()->cart->get_checkout_url();
    $medicalCheck = false;
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        if(get_field('medical_history', $cart_item['product_id']) == true){
            $medicalCheck = true;
        }
    }
    if($medicalCheck){ ?>
        <a href="/medical-history-form" class="checkout-button button alt wc-forward"><?php _e( 'Provide Medical History', 'woocommerce' ); ?></a>
    <?php } else { ?>
        <a href="<?= $checkout_url; ?>" class="checkout-button button alt wc-forward"><?php _e( 'Proceed To Checkout', 'woocommerce' ); ?></a>
    <?php }
}

if(isset($_GET["formID"])){
    $form_id = $_GET["formID"];
    setcookie('formSubmitted', $form_id, time() + (86400 * 30), "/");
}

add_action('woocommerce_checkout_update_order_meta',function( $order_id, $data ) {
    if(isset($_COOKIE['formSubmitted'])) {
        $order = wc_get_order($order_id);
        $order->update_meta_data('medical_form_id', $_COOKIE['formSubmitted']);
        $order->update_meta_data('medical_form_link', '/wp-admin/admin.php?page=gf_entries&view=entry&id=8&lid='.$_COOKIE['formSubmitted']);
        $order->save();
    }
} , 10, 2);

function custom_admin_js() {

    echo '<script type="text/javascript">
            if(document.querySelector("#medical-link")){
                var tar = document.querySelector("#medical-link");
                tar.querySelector("input").style.display = "none";
                tar.innerHTML += `<a href="${tar.querySelector("input").value}">View Medical History</a>`;
            }
          </script>';
}
add_action('admin_footer', 'custom_admin_js');


function registerLocationPostType()
{
    $labels = [
        'name'                  => _x('Locations', 'Location General Name', 'ogk'),
        'singular_name'         => _x('Location', 'Location Singular Name', 'ogk'),
        'menu_name'             => __('Locations', 'ogk'),
        'name_admin_bar'        => __('Location', 'ogk'),
        'archives'              => __('Item Archives', 'ogk'),
        'attributes'            => __('Item Attributes', 'ogk'),
        'parent_item_colon'     => __('Parent Item:', 'ogk'),
        'all_items'             => __('All Locations', 'ogk'),
        'add_new_item'          => __('Add New Location', 'ogk'),
        'add_new'               => __('Add New', 'ogk'),
        'new_item'              => __('New Item', 'ogk'),
        'edit_item'             => __('Edit Item', 'ogk'),
        'update_item'           => __('Update Item', 'ogk'),
        'view_item'             => __('View Item', 'ogk'),
        'view_items'            => __('View Items', 'ogk'),
        'search_items'          => __('Search Item', 'ogk'),
        'not_found'             => __('Not found', 'ogk'),
        'not_found_in_trash'    => __('Not found in Trash', 'ogk'),
        'featured_image'        => __('Featured Image', 'ogk'),
        'set_featured_image'    => __('Set featured image', 'ogk'),
        'remove_featured_image' => __('Remove featured image', 'ogk'),
        'use_featured_image'    => __('Use as featured image', 'ogk'),
        'insert_into_item'      => __('Insert into item', 'ogk'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'ogk'),
        'items_list'            => __('Items list', 'ogk'),
        'items_list_navigation' => __('Items list navigation', 'ogk'),
        'filter_items_list'     => __('Filter items list', 'ogk'),
    ];
    $args = [
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    ];
    register_post_type('location', $args);
}
add_action('init', 'registerLocationPostType');


function ogk_overwrite_permalink($permalink)
{
    if( get_field('overwrite_permalink', get_the_ID())) {
        $permalink = get_field('overwrite_permalink', get_the_ID());
    }
    return $permalink;
}

add_filter('post_type_link', 'ogk_overwrite_permalink', 10, 2);


/**
 * Die and Dump function
 * Colin's little function for debugging
 * Borrowed from Laravel's DD Function --> https://laravel.com/docs/5.6/helpers#method-dd
 * Can be deleted on production
 */
if ( ! function_exists( 'dd' ) ) {
    function dd() {
        array_map( function ( $x ) {
            echo "<pre>";
            var_dump( $x );
            echo "</pre>";
        }, func_get_args() );
        die;
    }
}
?>