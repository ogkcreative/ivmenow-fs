<?php
   $articleCat = get_the_category( $post );
?>
<div class="article-single flex-col flex-sb inview-fadeUp">
    <div class="article-details">
        <div class="image"><?php echo the_post_thumbnail('medium_large');?></div>

        <div class="iv-therapy flex flex-ac">
            <?php if(get_field('category_image', $articleCat[0])){ ?>
                <img src="<?=get_field('category_image', $articleCat[0])?>" alt="category image">
            <?php } ?>
            <div class="category">
                <a href="/education-portal/?tax=<?= $articleCat[0]->taxonomy;?>&term=<?= $articleCat[0]->term_id; ?>"><?=$articleCat[0]->name?></a>
            </div>
        </div>
       <a href="<?= get_permalink($post)?>">
           <div class="h3"><?=$post->post_title;?></div>
       </a>
        <div class="excerpt"><?= get_the_excerpt($post)?></div>
    </div>
    <div class="read-btn-wrap">
        <a class="read-more-btn" href="<?= get_permalink($post)?>">Read More</a>
    </div>
</div>
