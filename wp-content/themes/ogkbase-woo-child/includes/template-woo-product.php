<?php
/**
 *
 * WooCommerce Product Details Template
 *
 * This lives below the main product photo and details
 *
 */ ?>


<section class="product-top" id="product-<?php the_ID(); ?>">
        <?php
        global $product;

        $attachment_ids = $product->get_gallery_image_ids(); ?>
    <div class="product-wrap">

        <div class="prod-image fifty">
            <?php if ( $attachment_ids && $product->get_image_id() ) : $pdot = 0; ?>
                <div class="product-image-dots">
                    <?php foreach ( $attachment_ids as $attachment_id ) : ?>
                        <div class="product-image-dot" data="<?= $pdot ?>">
                            <?= wp_get_attachment_image($attachment_id, 'full') ?>
                        </div>
                    <?php $pdot++; endforeach; ?>
                </div>
            <?php endif; ?>
            <?php if ( $attachment_ids && $product->get_image_id() ) : ?>
                <div class="product-image-slider owl-carousel">
                    <?php foreach ( $attachment_ids as $attachment_id ) : ?>
                        <div class="image">
                            <?= wp_get_attachment_image($attachment_id, 'full') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="prod-details forty-five">
            <h5 class="prod-title"><?= get_the_title(); ?></h5>
            <h1 class="prod-flavor"><?= get_field('product_flavor') ?><span class="mg"><?= get_field('concentration') ?></span></h1>
            <p><?= get_the_content(); ?></p>
            <?php  do_action('woocommerce_single_product_summary'); ?>
        </div>
    </div>
</section>
<section class="product-specs">
    <div class="specs">
        <div class="prod-size spec">
            <h5>Size</h5>
            <h6><?= get_field('size') ?></h6>
        </div>
        <div class="prod-strength spec">
            <h5>Strength</h5>
            <h6><?= get_field('concentration') ?></h6>
        </div>
        <div class="prod-flavor spec">
            <h5>Flavor</h5>
            <h6><?= get_field('simple_flavor') ?></h6>
        </div>
    </div>
</section>
<section class="product-details">
    <div class="container">
        <div class="slide-dots-container thirty">
        <?php if( have_rows('product_details') ): $c = 0; ?>
            <div class="product-details-dots">
                <?php while( have_rows('product_details') ): the_row(); ?>
                    <div class="detail-dot" data="<?= $c ?>"><h5><?= get_sub_field('slide_title') ?></h5></div>
                <?php $c++; endwhile; ?>
            </div>
        <?php endif; ?>
            <a href="/lab-results/" class="lab-results-button"><h5>See Lab Results</h5></a>
        </div>

        <?php if( have_rows('product_details') ): ?>
            <div class="product-details-slider owl-carousel sixty">
                <?php while( have_rows('product_details') ): the_row(); ?>
                    <div class="prod-details-slide" data-hash="<?= get_sub_field('slide_title') ?>">
                        <?php if(have_rows('slide_details')): ?>
                            <div class="slide-detail-blocks">
                                <?php while(have_rows('slide_details')): the_row(); ?>
                                    <div class="detail-block">
                                        <h4><?= get_sub_field('detail_block_title') ?></h4>
                                        <p><?= get_sub_field('detail_block_text') ?></p>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>

<section class="product-related">
    <div class="container">
        <div class="title thirty">
            <h3>Some Favorites <span>You May Also Like</span></h3>
        </div>
        <div class="products sixty-five">
            <?php

            $current_post = get_the_ID();

            $args = array(
                'post_type'      => 'product',
                'posts_per_page' => 2,
                'orderby'        => 'rand',
                'post__not_in'   => array($current_post)
            );

            $related_products = new WP_Query( $args );

            if( $related_products->have_posts() ): ?>
                <div class="rel-products">
                    <?php while( $related_products->have_posts() ): $related_products->the_post(); ?>
                        <a href="<?= get_the_permalink() ?>" class="rel-product">
                            <?= woocommerce_get_product_thumbnail(); ?>
                            <h6><?= strip_tags(get_field('product_flavor')) ?></h6>
                            <p><?= get_the_title() ?></p>
                            <?php $price = get_post_meta( get_the_ID(), '_price', true ); ?>
                            <h5><?= wc_price( $price ); ?></h5>
                        </a>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
