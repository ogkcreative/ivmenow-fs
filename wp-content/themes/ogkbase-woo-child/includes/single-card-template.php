<?php
$terms = get_the_terms($post, 'page_tags');
$isProd = false;

$prodLink = get_permalink();
$price = get_field('service_price');
$clickEv = "gtag('event', 'Button-Click', {'event_category':'Book-Now-Click', 'event_label': 'Book-Now-" . sanitize_title(get_the_title()) . "});";
$btnLink = '<a class="btn medium" href="/request-form" data-name="'.get_the_title().'" data-price="'.$price.'" onclick="setActiveItem(this.dataset.name, this.dataset.price); ' . $clickEv . '">$'. $price . ' Book Now </a>';
//$btntxt = "Book Now";
if(get_post_type() == "product"){
    $isProd = true;
    $prod = wc_get_product( $post->ID );
    $price = $prod->get_price();
    $prodTitleSan = str_replace(get_the_title($post->ID), '-', ' ');
    $clickEv = "gtag('event', 'Button-Click', {'event_category':'Add-To-Cart-Click', 'event_label': 'Add-To-Cart-" . $prod->slug . "});";
    $btnLink =  '<button type="submit" value="'. $post->ID .'" name="add-to-cart"  class="single_add_to_cart_button btn" onclick="' . $clickEv . '">$'.$price. ' Add to Cart</button>';
    $terms = [];
}
$termCount = count($terms);
$index = 1;


?>


<div data-value="<?=get_the_title();?>" data-price="<?=$price;?>" class="item card-single">
    <div class="detail-wrap">
        <h3 class="h3 medium"><a href="<?= $prodLink ?>"><?= get_the_title(); ?></a></h3>
        <div class="small-title"> <?= get_field('sub_heading') ?></div>
        <div class="line-seperator"></div>
        <div class="card-tags">
            <?php if($isProd){
                echo $post->post_excerpt;
            } ?>
            <?php foreach($terms as $term):?>
                <?php if($index < $termCount){
                    echo '<span>'. $term->name . ', </span>';
                } else {
                    echo '<span>'. $term->name . '</span>';
                }?>
                <?php $index++ ?>

            <?php endforeach; ?>
        </div>

    </div>
    <div class="btn-wrap">
       <?= $btnLink; ?>
    </div>
</div>
