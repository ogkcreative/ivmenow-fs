<?php
/**
 * The template for displaying all single posts
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 * @package WordPress
 * @subpackage
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
<?php
global $post;
$args = array(
    'post_type' => 'location',
);
$locations        = new WP_Query( $args );

    ?>

<?php if ( $locations->have_posts() ) : ?>

<section class="location-section">
    <?php while ( $locations->have_posts() ) : $locations->the_post(); ?>
    <div class="location">

    </div>
        <?php endwhile; ?>
</section>



<?php endif; ?>
<?php get_footer();
