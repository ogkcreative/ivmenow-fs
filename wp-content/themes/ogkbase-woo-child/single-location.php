<?php
/**
 * The template for displaying all single posts
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 * @package WordPress
 * @subpackage
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>


<?php if(have_posts()):
    while(have_posts()): the_post(); ?>

        <section class="gradient-hero-section">
            <div class="container flex-col flex-ac">
                <h1 class="tc-wht h2 ta-center">In-Home Vitamin and IV Therapy and Hydration in <?= get_the_title() ?></h1>
            </div>
        </section>
        <?php if(have_rows('card_slider')):?>
        <?php while(have_rows('card_slider')): the_row();?>

                <section class="card-slider-section inview-fadeUp">
                    <?php if(get_sub_field('card_title_wysiwyg') && get_sub_field('cards_title')): ?>
                        <div class="container flex flex-wrap">
                            <div class="fifty flex flex-ac flex-jc">
                                <h2 class="tc-prim"><?= get_sub_field('cards_title');?>
                                    <?php if(get_sub_field('cards_cta')):?>
                                        <a href="<?= get_sub_field('cards_cta_link');?>"><?= get_sub_field('cards_cta'); ?></a>
                                    <?php endif; ?>
                                </h2>
                            </div>
                            <div class="fifty flex flex-ac flex-jc">
                                <?= get_sub_field('card_title_wysiwyg'); ?>
                            </div>
                            <?php if(get_sub_field('button_text')):?>
                                <div class="btn-wrap expert-button">
                                    <a href="/request-form" class="btn"><?= get_sub_field('button_text')?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php else: ?>
                        <div class="container">
                            <h2 class="small-title text-center tc-prim">IV Me Now is a Mobile IV Therapy service throughout the <?= get_the_title();?> area. Our IV Hydration and Vitamin therapy services are aimed at addressing many types of health and wellness needs.
                                <?php if(get_sub_field('cards_cta')):?>
                                    <a href="<?= get_sub_field('cards_cta_link');?>"><?= get_sub_field('cards_cta'); ?></a>
                                <?php endif; ?>
                            </h2>
                        </div>
                    <?php endif; ?>
                    <?php
                    $cardsData= get_sub_field('cards_data');
                    //    var_dump($cardsData);3
                    if($cardsData): ?>
                        <div class="services card-slider owl-carousel">
                            <?php foreach($cardsData as $post): setup_postdata($post); ?>
                                <?php get_template_part("includes/single-card-template"); ?>
                            <?php endforeach; wp_reset_postdata(); ?>
                        </div>
                    <?php endif; ?>

                </section>

        <?php endwhile; ?>
        <?php endif;?>

        <section class="call-us-cta-section">
            <div class="container flex flex-sb flex-ac">
                <div class="img thirty-five inview-fadeUp">
                    <?= wp_get_attachment_image(get_field('image'), 'full') ?>
                </div>
                <div class="text fifty inview-fadeUp">
                    <div class="h2 tc-prim"><?= get_field('title')?></div>
                    <div class="h3 tc-prim content"><?= get_field('content')?></div>
                    <div class="btn-wrap inview-fadeUp">
                        <a class="btn location-call-us-cta" href="tel:<?=get_field('phone', 'options')?>">Give Us a Call</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="benefits-section bg-offWhite">
            <div class="container-large">
                <?php if (get_field('title')):?>
                    <div class="h2 ta-center tc-prim inview-fadeUp"><?= get_field('title')?></div>
                <?php endif; ?>
                <?php if (have_rows('benefit')): ?>
                    <div class="benefits flex flex-sb flex-jc">
                        <?php while (have_rows('benefit')): the_row(); ?>
                            <div class="benefit inview-fadeUp">
                                <?= wp_get_attachment_image(get_sub_field('icon'), 'full') ?>
                                <div class="small-title ta-center"><?= get_sub_field('title') ?></div>
                                <p class="benefit-text ta-center"><?= get_sub_field('subtitle') ?></p>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
                <?php if(get_field('button_link')):?>
                    <div class="btn-wrap flex flex-jc inview-fadeUp">
                        <a class="btn location-benefits-cta" href="<?= get_permalink(1525) ?>"><?= get_field('button_text') ?></a>
                    </div>
                <?php endif; ?>
            </div>
        </section>

    <?php endwhile;
endif; ?>


<?php get_footer();
