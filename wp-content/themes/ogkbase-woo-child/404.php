<?php get_header(); ?>
<div class="content flex flex-ac" style="height:80vh; background:url(<?=get_field('404_image', 'options')?>) no-repeat center/cover;">
    <div class="error-inner container">
        <img class="logo" src="/wp-content/themes/ogkbase-woo-child/images/ivmenowlogo-sq.png">
        <div class="h2 tc-prim ta-center">404</div>
        <div class="h3 ta-center"><?=get_field('404_text', 'options');?></div>
        <div class="btn-wrap flex flex-jc">
            <a class="btn-arrow error-button" href="<?=get_field('button_link', 'options')?>"><?=get_field('button_text', 'options')?></a>
        </div>
    </div>
</div>

<?php get_footer();?>
