<?php get_header(); ?>

<?php if(strlen(get_the_content())): ?>
    <div class="container content-container">
        <?php the_content(); ?>
    </div>
<?php endif; ?>
<?php get_template_part("includes/flexible"); ?>

<?php get_footer(); ?>
