jQuery(document).ready(function ($) {
  let fadeInSpeed = 1000;
  $('.inview-fadeUp').on('inview', function (event, isInView) {
    if (isInView) {
      $(this).addClass("fadeUp");
    } else {
    }
  });
  $('.inview-counter').on('inview', function (event, isInView) {
    if (isInView) {
      $(this).addClass("counter");
    } else {
    }
  });
  $('.inview-fadeIn').on('inview', function (event, isInView) {
    if (isInView) {
      $(this).addClass("fadeIn");
    } else {
    }
  }); 

  $(".product-image-slider").owlCarousel({
    items: 1,
    loop: true,
    autoplay: true,
    autoplayTimeout: 2000,
    pagination: false,
    nav: false,
    rewindSpeed: 500
  });
  $(".product-details-slider").owlCarousel({
    items: 1,
    loop: true,
    autoplay: true,
    autoplayTimeout: 2000,
    pagination: false,
    nav: false,
    rewindSpeed: 500
  });
  $(".card-slider").owlCarousel({
    loop: true,
    autoplay: true,
    autoHeight: false,
    autoplayTimeout: 5000,
    pagination: false,
    rewindNav: false,
    checkVisibility: true,
    rewindSpeed: 0,
    nav: false,
    margin: 15,
    responsive: {
      0: {
        items: 1
      },
      300: {
        items: 2
      },
      400: {
        items: 2
      },
      550: {
        items: 3
      },
      768: {
        items: 3
      },
      992: {
        items: 4
      },
      1200: {
        items: 4
      },
      1400: {
        items: 5
      },
      1650: {
        items: 6
      },
      1900: {
        items: 7
      }
    }
  }); 

  $("#menu-item-1403").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Call-Click',
      'event_label': 'Call-CTA-Footer'
    });
  }); 

  $("#basic-hero-cta-1-22").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Request-Form',
      'event_label': 'Book-Mobile-Visit-CTA-Header'
    });
  }); 

  $("#schedule-home-visit-3-22").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Request-Form',
      'event_label': 'Three-Section-Schedule-Visit-CTA'
    });
  }); 

  $("#benefits-cta-7-22").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Request-Form',
      'event_label': 'Above-Footer-Get-IV-Started-CTA'
    });
  }); 

  $("#full-image-cta-5-22").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Product-Click',
      'event_label': 'Reverse-with-NAD-buy-Now-CTA-Home'
    });
  }); 

  $("#hp-startnow-anti-aging").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Product-Click',
      'event_label': 'Start-Now-Anti-Aging-Home'
    });
  }); 

  $("#hp-startnow-energy").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Product-Click',
      'event_label': 'Start-Now-Energy-Home'
    });
  }); 

  $("#hp-startnow-libido").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Product-Click',
      'event_label': 'Start-Now-Libido-Home'
    });
  }); 

  $("#hp-startnow-weight-loss").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Product-Click',
      'event_label': 'Start-Now-Weight-Loss-Home'
    });
  }); 

  $("#hp-startnow-wellness").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Product-Click',
      'event_label': 'Start-Now-Wellness-Home'
    });
  }); 

  $("#call-us-cta-3-1355").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Call-Click',
      'event_label': 'Give-Us-A-Call-IV-Services'
    });
  }); 

  $("#book-event-cta-4-1355").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Request-Form',
      'event_label': 'Book-An-Event-IV-Services'
    });
  }); 

  $("#schedule-home-visit-5-1358, #schedule-home-visit-5-1360, #schedule-home-visit-6-1370, #schedule-home-visit-5-1376, #schedule-home-visit-5-1378, #schedule-home-visit-5-1380, #schedule-home-visit-5-2045, #schedule-home-visit-5-2294").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Request-Form',
      'event_label': 'Schedule-Home-Visit-Above-Footer'
    });
  }); 

  $("#call-us-cta-3-168").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Call-Click',
      'event_label': 'Give-Us-A-Call-Wellness'
    });
  }); 

  $("#contact-call-button").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Call-Click',
      'event_label': 'Contact-Page-Call'
    });
  }); 

  $("#contact-email-button").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Call-Click',
      'event_label': 'Contact-Page-Email'
    });
  }); 

  $(".location-benefits-cta").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Request-Form',
      'event_label': 'Get-IV-Started-Location'
    });
  }); 

  $(".location-call-us-cta").on("click", function () {
    gtag('event', 'Button-Click', {
      'event_category': 'Request-Form',
      'event_label': 'Give-Us-A-Call-Location-Page'
    });
  });
  let anchorDrop = $(".category .small-title");
  anchorDrop.on("click", function () {
    if ($(window).width() < 600) {
      anchorDrop.siblings(".questions").slideUp();

      if ($(this).hasClass("active")) {
        anchorDrop.removeClass("active");
        $(this).siblings(".questions").slideUp();
      } else {
        anchorDrop.removeClass('active');
        $(this).addClass("active");
        $(this).siblings(".questions").slideDown();
      }
    }
  });
  let mobileFilterToggle = $(".mobile-filter-trigger");
  let list = $(".dropdown-list");
  let filterTrigger = $(".dropdown-list .filter-trigger");
  let filterButton = $(".filter-trigger .filter-button"); 

  mobileFilterToggle.on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
    list.toggleClass("open");
  });
  filterTrigger.on("click", function (e) {
    list.removeClass("open");
    mobileFilterToggle.removeClass("active");
    filterTrigger.removeClass("active");
    $(this).addClass("active");

    if ($(this).hasClass('active')) {
      $('.mobile-filter-trigger .select').text(filterText);
    }
  });
  $('.mobile-filter-trigger .select').text($('.mobile-filters-container .filter-button.active').text());

  if (document.querySelector('.service-select')) {
    if (sessionStorage.getItem('activeProd') && sessionStorage.getItem('activePrice')) {
      document.getElementById('service-price').innerHTML = sessionStorage.getItem('activePrice');
      document.getElementById('service-price').dataset.price = sessionStorage.getItem('activePrice');
      console.log(sessionStorage.getItem('activeProd') + '-' + sessionStorage.getItem('activePrice'));
      forceSelectOption(document.querySelector('.service-select'), sessionStorage.getItem('activeProd') + '-' + sessionStorage.getItem('activePrice'));
    }

    document.querySelector('.service-select select').addEventListener('change', function () {
      let name = this.value.slice(0, this.value.indexOf('-'));
      let price = this.value.slice(this.value.indexOf('-') + 1, this.value.length);
      document.getElementById('service-price').dataset.price = price;
      document.getElementById('service-price').innerHTML = price;
      document.querySelector('.extra_people input').value = 0;
      document.querySelector('.counter-results').innerHTML = 0;
      setActiveItem(name, price);
    });
  }

  ;
  $(".qty-btn").on("click", function () {
    console.log('qty btn');
    var inVal = parseInt(document.querySelector('.single-quantity').value);
    var basePrice = parseInt(document.querySelector('.price-single').dataset.price);

    if (inVal + parseInt(this.dataset.increment) >= 1) {
      inVal += parseInt(this.dataset.increment);
      document.querySelector('.single-quantity').value = inVal;
      document.querySelector('.price-single').innerHTML = '$' + inVal * basePrice;
    }
  });
});

function setActiveItem(name, price) {
  sessionStorage.setItem('activeProd', name);
  sessionStorage.setItem('activePrice', price);
}

function forceSelectOption(el, val) {
  el.querySelector('option[value="' + val + '"]').selected = true;
}

function friendCounter(val) {
  var tar = document.querySelector('.counter-results');
  var curVal = parseInt(tar.innerHTML) + val;
  var inp = document.querySelector('.extra_people input');
  var priceTar = document.getElementById('service-price');
  var price = parseInt(priceTar.dataset.price);

  if (curVal >= 0) {
    tar.innerHTML = curVal;
    inp.value = curVal;
    priceTar.innerHTML = price * (curVal + 1);
  } else {
    curVal = 0;
    tar.innerHTML = 0;
    inp.value = 0;
    priceTar.innerHTML = price;
  }
}

function serviceActiveVariant(title) {
  let prc = document.getElementById('price-option').value;
  setActiveItem(title, prc);
}

function serviceVariantPriceHandler(el) {
  let prc = el.value;
  document.querySelector('.price-single').innerHTML = "$" + prc;
}