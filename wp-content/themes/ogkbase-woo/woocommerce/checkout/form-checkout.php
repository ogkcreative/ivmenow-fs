<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
    exit;
}

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
    echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
    return;
}

?>


<form name="checkout" method="post" class="checkout woocommerce-checkout"
      action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

    <?php if ($checkout->get_checkout_fields()) : ?>

        <?php do_action('woocommerce_checkout_before_customer_details'); ?>

        <div class="customer-details page active" id="customer-details">

            <?php do_action('woocommerce_checkout_billing'); ?>
            <div class="btn-wrap">
                <a href="#shipping-details" id="to-shipping-details" class="btn next">Continue to Shipping Details</a>
            </div>

        </div>

        <div class="shipping-details page" id="shipping-details">

            <h4>Where would you like your order sent?</h4>

            <ul class="woocommerce-shipping-methods">
                <li class="ship-btn">
                    <input type="radio" name="checkout_ship_to_option" value="billing" checked>
                    <label>Ship to my Billing Address</label>
                    <div class="radio-button"></div>
                </li>
                <li class="ship-btn">
                    <input type="radio" name="checkout_ship_to_option" value="different">
                    <label>Ship to a Different Address</label>
                    <div class="radio-button"></div>
                </li>
            </ul>

            <?php do_action('woocommerce_checkout_shipping'); ?>
            <div class="btn-wrap" style="display: inline-block;">
                <button onclick="document.getElementById('sidebar-btn-cd').click()" class="btn back">Back</button>
            </div>
            <div class="btn-wrap" style="display:inline-block">
                <a href="#shipping-method" style="display: inline-block;" id="to-shipping-method" class="btn next">Continue to Shipping Method</a>
            </div>

        </div>

        <div class="shipping-method page" id="shipping-method">
            <h4>What is your preferred method of shipping?</h4>

            <?php do_action('woocommerce_review_order_before_shipping'); ?>

            <?php wc_cart_totals_shipping_html(); ?>

            <?php do_action('woocommerce_review_order_after_shipping'); ?>
            <div class="btn-wrap" style="display: inline-block;">
                <button onclick="document.getElementById('sidebar-btn-sd').click()" class="btn back">Back</button>
            </div>
            <div class="btn-wrap" style="display: inline-block;" >
                <a href="#review-order" id="to-review-order" class="btn next">Review Your Order</a>
            </div>
        </div>

        <?php do_action('woocommerce_checkout_after_customer_details'); ?>

    <?php endif; ?>

    <?php do_action('woocommerce_checkout_before_order_review_heading'); ?>

    <?php do_action('woocommerce_checkout_before_order_review'); ?>

    <div id="review-order" class="woocommerce-checkout-review-order page">
        <h4>Review your order</h4>
        <?php do_action('woocommerce_checkout_order_review'); ?>
        <div class="btn-wrap" style="display: inline-block;">
            <button onclick="document.getElementById('sidebar-btn-sm').click()" class="btn back">Back</button>
        </div>
        <div class="btn-wrap" style="display: inline-block;" >
            <a href="#payment-method" style="display: inline-block;" id="to-payment-method" class="btn next">Continue to Payment Method</a>
        </div>
    </div>

    <?php do_action('woocommerce_checkout_after_order_review'); ?>


</form>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>
