<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined('ABSPATH') || exit;

global $woocommerce;
?>

<?php //$cart = WC()->cart->get_cart();
//print_r($cart); ?>

<div class="order-review-wrap">
  <?php $cart = WC()->cart->get_cart();

  foreach ($cart as $cart_item):

    $product = wc_get_product($cart_item['product_id']);
    $product_image = wp_get_attachment_image($cart_item['image_id'], 'full');
    $item_qty = $cart_item['quantity'];
    $item_total = $cart_item['line_total'];
    $item_name = $cart_item['name'];

    // get the shipping label
    foreach (WC()->session->get('shipping_for_package_0')['rates'] as $method_id => $rate):

      if ( WC()->session->get('chosen_shipping_methods')[0] == $method_id ):
        $shipping_label = $rate->label; // The shipping method label name
      endif;

    endforeach;

    // get formatted totals
    $formatted_totals = array();
    foreach (WC()->cart->get_totals() as $key => $value) :
      $formatted_totals[$key] = wc_price($value);
    endforeach;
    ?>


    <div class="item-detail-line line">
      <div class="item-detail-line_left">
       <p><?= $product->get_image() ?><strong><?= $product->get_name() ?></strong> x <?= $item_qty ?></p>
      </div>
      <div class="item-detail-line_right">
        <p><strong><?= wc_price($product->get_price()) ?></strong></p>
      </div>
    </div>


  <?php endforeach; ?>
  <div class="subtotal-line line">
    <p class="label">Subtotal</p>
    <p><strong><?= wc_price(WC()->cart->get_subtotal()) ?></strong></p>
  </div>
  <div class="shipping-line line">
    <p><span class="label cart_shipping_label"><?= $shipping_label ?></span> Shipping</p>
    <p><strong class="cart_shipping_total"><?= $formatted_totals['shipping_total'] ?></strong></p>
  </div>
    <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
        <div class="coupon-line line coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
            <p><span class="label"><?php wc_cart_totals_coupon_label( $coupon ); ?></span></p>
            <p><strong class="cart_discount"><?php wc_cart_totals_coupon_html( $coupon ); ?></strong></p>
        </div>
    <?php endforeach; ?>
  <div class="tax-line line">
    <p class="label">Taxes</p>
    <p><strong class="cart_total_tax"><?= wc_price(WC()->cart->get_total_tax()) ?></strong></p>
  </div>
  <div class="order-total-line line">
    <p class="label"><strong>Order Total</strong></p>
    <p><strong class="cart_total"><?= wc_price($woocommerce->cart->total) ?></strong></p>
  </div>

    <a class="edit-cart-link" onclick="//event.preventDefault(); document.body.classList.add('no-scroll'); document.querySelector('.mini-cart').classList.add('cart-open');" href="/?cart=true">Edit Order</a>
</div>



