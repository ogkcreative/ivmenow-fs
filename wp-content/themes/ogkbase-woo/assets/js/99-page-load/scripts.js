/*
 * Copyright (c) 2019. OGK Creative
 */
document.addEventListener('wheel touchstart', (evt) => {
    // ... do stuff with evt
}, {
    capture: true,
    passive: true
});

var validatedStep = 0;
var cartCount = jQuery(".cart-count-number-span").html();
var miniCartState = 'closed';

let ogk_header_cart_form = jQuery('.woocommerce-cart-form');
let ogk_header_cart_form_initial_state = ogk_header_cart_form.serialize();

let variationSelectedProductPrice = 0;

// Check mobile height for each browser
let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);

jQuery(document).ready(function ($) {
   // window.setTimeout(routerOnRdy,300);
    routerOnRdy();
    // OWL CAROUSEL

    $(".blog-slider").owlCarousel({
        items: 3,
        loop: true,
        autoplay: false,
        stagePadding: 100,
        margin: 20,
        autoplayTimeout: 3000,
        pagination: false,
        nav: true,
        rewindSpeed: 2000,
        smartSpeed: 2000,
        responsive: {
            0: {
                items: 1,
                stagePadding: 50
            },
            991: {
                items: 2,
                stagePadding: 100
            },
            1310: {
                items: 3
            }
        }
    });

    $(".product-details-slider").owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        dots: true,
        dotsContainer: '.product-details-dots',
        nav: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoHeight: true
    });

    $('.product-details-dots').on('click', '.detail-dot', function (e) {
        $(".product-details-slider").trigger('to.owl.carousel', [$(this).index(), 300]);
    });

    $(".about-slider").owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        dots: true,
        dotsContainer: '.about-slider-dots',
        nav: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoHeight: true
    });

    $('.about-slider-dots').on('click', '.about-slider-dot', function (e) {
        $(".about-slider").trigger('to.owl.carousel', [$(this).index(), 300]);
    });

    $(".product-image-slider").owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        dots: true,
        dotsContainer: '.product-image-dots',
        nav: false,
        autoHeight: true
    });

    $('.product-image-dots').on('click', '.product-image-dot', function (e) {
        $(".product-image-slider").trigger('to.owl.carousel', [$(this).index(), 300]);
    });

    // Sticky header on scroll
    // var position = $(window).scrollTop();
    //
    // $(window).scroll(function() {
    //     var scroll = $(window).scrollTop();
    //     if(scroll > 0) {
    //         $('header').addClass('sticky');
    //         $(".site-content").addClass('sticky-header');
    //         $(".mini-cart").addClass('sticky-header');
    //     } else {
    //         $('header').removeClass('sticky');
    //         $(".site-content").removeClass('sticky-header');
    //         $(".mini-cart").removeClass('sticky-header');
    //     }
    //     position = scroll;
    // });


    // Mobile Header
    $(".hamburger").on("click", function () {
        $(this).toggleClass("open");
        $("body").toggleClass("no-scroll");
        $(".nav-menu").toggleClass("open");
    });

    $(".input-text.qty.text").bind('keyup mouseup', function () {
        var value = $(this).val();
        $(".product_quantity").val(value)
    });

    // Checkout Page

    var activePage = $(".page.active");
    var activeSide = $(".checkout-sidebar-btn.active");

    var cdPage = $("#customer-details");
    var sdPage = $("#shipping-details");
    var smPage = $("#shipping-method");
    var roPage = $("#review-order");
    var paymentMethod = $("#payment-method");

    var cdButton = $("#customer-details .next");
    var sdButton = $("#shipping-details .next");
    var smButton = $("#shipping-method .next");
    var roButton = $("#to-payment-method");

    var roSide = $("#sidebar-btn-ro");
    var cdSide = $("#sidebar-btn-cd");
    var sdSide = $("#sidebar-btn-sd");
    var smSide = $("#sidebar-btn-sm");
    var pmSide = $("#sidebar-btn-pm");

    var inputEmail = $("#cd_email_address_field");

    var timeoutHandler = null;

    $(".woocommerce .next").click(function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });


    cdPage.show();

    cdButton.on("click", function (event) {

        event.preventDefault();
        checkInputsCustomerDetails();

        $("body.woocommerce-checkout .woocommerce").removeClass("order-review");

    });


    sdButton.on("click", function (event) {
        event.preventDefault();
        checkInputsShipping();

        $("body.woocommerce-checkout .woocommerce").removeClass("order-review");

    });


    smButton.on("click", function (event) {
        event.preventDefault();

        validatedStep = 3;
        //console.log(validatedStep);

        roPage.addClass("loading");
        smPage.removeClass("active");
        smSide.removeClass("active");
        roSide.addClass("active");

        $("body.woocommerce-checkout .woocommerce").addClass("order-review");

        timeoutHandler = setTimeout(function () {
            roPage.removeClass("loading");
            roPage.addClass("active");
        }, 1000);
    });

    roButton.on("click", function (event) {
        event.preventDefault();

        validatedStep = 4;
        //console.log(validatedStep);

        $("#payment-method").addClass("loading");
        roPage.removeClass("active");
        roSide.removeClass("active");
        pmSide.addClass("active");
        //console.log("semi-working");

        $("body.woocommerce-checkout .woocommerce").removeClass("order-review");

        timeoutHandler = setTimeout(function () {
            $("#payment-method").removeClass("loading");
            $("#payment-method").addClass("active");
        }, 1000);
    });

    pmSide.on("click", function (event) {
        event.preventDefault();

        console.log(validatedStep);
        if (validatedStep >= 4) {

            $("#payment-method").addClass("loading");
            $(this).addClass("active");

            cdSide.removeClass("active");
            sdSide.removeClass("active");
            smSide.removeClass("active");
            roSide.removeClass("active");

            cdPage.removeClass("active");
            sdPage.removeClass("active");
            smPage.removeClass("active");
            roPage.removeClass("active");

            $("body.woocommerce-checkout .woocommerce").removeClass("order-review");

            timeoutHandler = setTimeout(function () {
                $("#payment-method").removeClass("loading");
                $("#payment-method").addClass("active");
            }, 1000);

        }
    });


    roSide.on("click", function (event) {
        event.preventDefault();

        console.log(validatedStep);
        if (validatedStep >= 3) {

            roPage.addClass("loading");
            $(this).addClass("active");

            cdSide.removeClass("active");
            sdSide.removeClass("active");
            smSide.removeClass("active");
            pmSide.removeClass("active");

            cdPage.removeClass("active");
            sdPage.removeClass("active");
            smPage.removeClass("active");
            $("#payment-method").removeClass("active");

            $("body.woocommerce-checkout .woocommerce").addClass("order-review");

            timeoutHandler = setTimeout(function () {
                roPage.removeClass("loading");
                roPage.addClass("active");
            }, 1000);

        }
    });

    cdSide.on("click", function (event) {
        event.preventDefault();
        cdPage.addClass("loading");
        $(this).addClass("active");

        roSide.removeClass("active");
        sdSide.removeClass("active");
        smSide.removeClass("active");
        pmSide.removeClass("active");

        roPage.removeClass("active");
        sdPage.removeClass("active");
        smPage.removeClass("active");
        $("#payment-method").removeClass("active");

        $("body.woocommerce-checkout .woocommerce").removeClass("order-review");

        timeoutHandler = setTimeout(function () {
            cdPage.removeClass("loading");
            cdPage.addClass("active");
        }, 1000);
    });

    sdSide.on("click", function (event) {

        event.preventDefault();

        console.log(validatedStep);
        if (validatedStep >= 1) {

            sdPage.addClass("loading");
            $(this).addClass("active");

            cdSide.removeClass("active");
            roSide.removeClass("active");
            smSide.removeClass("active");
            pmSide.removeClass("active");

            cdPage.removeClass("active");
            roPage.removeClass("active");
            smPage.removeClass("active");
            $("#payment-method").removeClass("active");

            $("body.woocommerce-checkout .woocommerce").removeClass("order-review");

            timeoutHandler = setTimeout(function () {
                sdPage.removeClass("loading");
                sdPage.addClass("active");
            }, 1000);

        }

    });

    smSide.on("click", function (event) {
        event.preventDefault();

        console.log(validatedStep);
        if (validatedStep >= 2) {

            smPage.addClass("loading");
            $(this).addClass("active");

            cdSide.removeClass("active");
            sdSide.removeClass("active");
            roSide.removeClass("active");
            pmSide.removeClass("active");

            cdPage.removeClass("active");
            sdPage.removeClass("active");
            roPage.removeClass("active");
            $("#payment-method").removeClass("active");

            $("body.woocommerce-checkout .woocommerce").removeClass("order-review");

            timeoutHandler = setTimeout(function () {
                smPage.removeClass("loading");
                smPage.addClass("active");
            }, 1000);

        }
    });

    if ($("#review-order").hasClass("active")) {

        //console.log("I'm active!");
    } else {
        $("body.woocommerce-checkout .woocommerce").removeClass("order-review");
    }

    // Checkout input verification

    var $input = $(".woocommerce input");
    var $gform_input = $(".ginput_container input[type=text], .ginput_container textarea, .ginput_container select");
    var $affiliate_input = $(".affwp-form input, .affwp-form textarea");

    function checkForInput(element) {

        const $label = $(element).parent().siblings('label');
        const $wrapper = $(element).parent();

        if ($(element).val().length > 0) {
            $label.addClass('focus');
            $wrapper.addClass('focus');
        } else {
            $label.removeClass('focus');
            $wrapper.removeClass('focus');
        }
    }

    function checkForInputLogin(element) {

        const $label = $(element).siblings('label');
        const $wrapper = $(element).parent();

        if ($(element).val().length > 0) {
            $label.addClass('focus');
            $wrapper.addClass('focus');
        } else {
            $label.removeClass('focus');
            $wrapper.removeClass('focus');
        }
    }

    // On Page Load
    $input.each(function () {
        checkForInput(this);
        checkForInputLogin(this);
    });
    $gform_input.each(function () {
        checkForInput(this);
    });
    $affiliate_input.each(function () {
        checkForInput(this);
    });

    // On Focus, Change, and Keyup
    $input.on('focus change keyup', function () {
        checkForInput(this);
        checkForInputLogin(this);
    });
    $gform_input.on('focus change keyup', function () {
        checkForInput(this);
    });
    $affiliate_input.on('focus change keyup', function () {
        checkForInput(this);
    });

    $("input#shipping_address_1, input#shipping_address_2").removeAttr("placeholder");


    // FAQ Page Toggle

    var allPanels = $(".faq-text").hide();

    $(".faq").on("click", function () {
        $(this).children(".faq-title .faq-toggle").toggleClass("open");
        $(this).children(".faq-text").slideToggle();
    });

    // Dashboard Mobile Menu
    $(".mobile-dash-menu").on("click", function() {
        $(".woocommerce-MyAccount-navigation").slideToggle();
        $(this).toggleClass("open");
    });


    $('div.woocommerce').on('change', '.qty', function () {
        $("[name='update_cart']").prop("disabled", false);
        $("[name='update_cart']").trigger("click");
    });


    $("#customer-capture button").on("click", function (e) {
        e.preventDefault();
        if ($("#customer-capture [name='cd_first_name']").val() === '') {
            $("#customer-capture [name='cd_first_name']").css("border", "1px solid red");
        } else if ($("#customer-capture [name='cd_email_address']").val() === '') {
            $("#customer-capture [name='cd_email_address']").css("border", "1px solid red");
        } else {
            $("#loading-img").css("display", "block");
            var sendData = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "get_response.php",
                data: sendData,
                success: function (data) {
                    $("#loading-img").css("display", "none");
                    $(".response_msg").text(data);
                    $(".response_msg").slideDown().fadeOut(3000);
                    $("#customer-capture").find("input[type=text], input[type=email], textarea").val("");
                }
            });
        }
    });

    $("#customer-capture input").blur(function () {
        var checkValue = $(this).val();
        if (checkValue != '') {
            $(this).css("border", "1px solid #eeeeee");
        }
    });

    // moved from mini-cart-scripts
    // toggle min cart events
    //jQuery(document).ready(function ($) {
    jQuery(document).on("click touch", ".header-cart-count, .cart-count-number-span, .close-cart svg", function () {
// cart-count-number-span
//         if(document.querySelector('header').classList.contains('checkout-header')){
//             return false;
//         }
        var cartCount = jQuery(".cart-count-number-span").html();
        //console.log(cartCount);

        if (cartCount != 0) {

            jQuery(".mini-cart").toggleClass("cart-open");
            jQuery("header.container-large").toggleClass("cart-open");
            jQuery("body").toggleClass("no-scroll");

            if (miniCartState == 'open') {
                miniCartState = 'closed';
            } else if (miniCartState == 'closed') {
                miniCartState = 'open';
            }

        } else {

            jQuery(".empty-cart-notice").addClass("visible");
            timeoutHandler = setTimeout(function () {
                jQuery(".empty-cart-notice").removeClass("visible");
            }, 2000);

        }

        //console.log(miniCartState);

    });
    //});

    // moved from woo-functions
    /*var timeoutHandler = null;
    jQuery(".header-cart-count").on("click", function () {
        jQuery(".empty-cart-notice").addClass("visible");
        timeoutHandler = setTimeout(function () {
            jQuery(".empty-cart-notice").removeClass("visible");
        }, 2000);
    });*/

    // after variation selection event
    // $(document).on("found_variation.first", function (e, v) {
    //     //console.log(v);
    //     variationSelectedProductPrice = v.display_price;
    //     var quantity = jQuery('.woocommerce-variation-add-to-cart .quantity input').val();
    //     var displayPrice = (variationSelectedProductPrice * quantity).toFixed(2);
    //     jQuery('.single_add_to_cart_button.button.alt').html('Add to Bag - $'+ displayPrice);
    // });
    // $(document).on('keyup', '.woocommerce-variation-add-to-cart .quantity input', function (e) {
    //     updateVariableButton();
    // });
    //
    // function updateVariableButton(){
    //
    //     var quantity = jQuery('.woocommerce-variation-add-to-cart .quantity input').val();
    //     var displayPrice = (variationSelectedProductPrice * quantity).toFixed(2);
    //     //var price2 = jQuery('.woocommerce-Price-amount.amount').html();
    //     //console.log(price2);
    //     jQuery('.single_add_to_cart_button.button.alt').html('Add to Bag - $' + displayPrice);
    //
    // }

    // moved from ajax-add-to-cart
    // add to cart event
    $(document).on('click', '.single_add_to_cart_button', function (e) {
        e.preventDefault();

        // Add the product to the cart
        $thisbutton = $(this),
            $form = $thisbutton.closest('form.cart'),
            id = $thisbutton.val(),
            product_qty = $(document).find('input[name=quantity]').val() || 1,
            product_id = $form.find('input[name=product_id]').val() || id,
            variation_id = $form.find('input[name=variation_id]').val() || 0;


        // Prepare Data
        var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: product_id,
            product_sku: '',
            quantity: product_qty,
            variation_id: variation_id,
        };

        // Ajax Add to cart event
        $(document.body).trigger('adding_to_cart', [$thisbutton, data]);

        // Ajax add to cart function
        $.ajax({
            type: 'post',
            url: wc_add_to_cart_params.ajax_url,
            data: data,
            beforeSend: function (response) {
                $thisbutton.removeClass('added').addClass('loading');
            },
            complete: function (response) {
                $thisbutton.addClass('added').removeClass('loading');
            },
            success: function (response) {

                //console.log(miniCartState);

                if (response.error & response.product_url) {
                    window.location = response.product_url;
                    return;
                } else {
                    $(".mini-cart-wrap").replaceWith(response.fragments.minicart);

                    //console.log(miniCartState);

                    if (miniCartState == 'open') {
                        $(".mini-cart").addClass('cart-open');
                    }

                    $(".cart-count-number").replaceWith(response.fragments.cartcountnumber);
                    $(".ogk-mini-cart-scripts").replaceWith(response.fragments.minicartscripts);

                    //console.log(response.fragments);
                    $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);

                    if (miniCartState == 'open') {
                        $(".item-added").addClass('cart-open');
                    }
                    $(".item-added").addClass("visible");

                    setTimeout(function () {
                        jQuery(".item-added").removeClass("visible");
                        if (miniCartState == 'open') {
                            $(".item-added").removeClass('cart-open');
                        }
                    }, 2000);

                    // update the initial state to prevent unneeded ajax calls
                    ogk_header_cart_form = jQuery('.woocommerce-cart-form');
                    ogk_header_cart_form_initial_state = ogk_header_cart_form.serialize();

                }
            },
        });

    });

    let shippingToOption = 'billing';
    $(document).on('click', 'input[name="checkout_ship_to_option"]', function () {

        if (this.value != shippingToOption) {
            $("#ship-to-different-address-checkbox").click();
            shippingToOption = this.value;
        }

        /*if (this.value == 'different') {
            $("#ship-to-different-address-checkbox").prop("checked", true);
        } else if (this.value == 'billing') {
            $("#ship-to-different-address-checkbox").prop("checked", false);
        }*/

    });

    //////// header cart updates - quantity, remove, coupon ////////

    // coupon button
    //$('.woocommerce-cart-form').submit(function (event) {
    $(document).on('submit', '.woocommerce-cart-form', {passive: true}, function (event) {
        event.preventDefault();
        updateHeaderCart();
        return false;
    });

    // submit form if user changes a quantity
    //$('.woocommerce-cart-form .product-quantity input').keyup(function (event) {
    $(document).on('change', '.woocommerce-cart-form .product-quantity input', {passive: true}, function (event) {

        qtyTimeoutHandler = null;
        qtyTimeoutHandler = setTimeout(function () {
            updateHeaderCart();
            return false;
        }, 1000);

    });

    // remove item links
    $(document).on('click', '.product-remove a', {passive: true}, function (event) {
        event.preventDefault();
        //var remove_product_id = jQuery(this).attr('data-product_id');
        //jQuery("#header_cart_remove_product").val(remove_product_id);
        var remove_cart_key = jQuery(this).attr('data-cart_key');
        jQuery("#header_cart_remove_product").val(remove_cart_key);

        updateHeaderCart();
        return false;
    });

    // remove coupon
    $(document).on('click', 'a.woocommerce-remove-coupon', {passive: true}, function (event) {
        event.preventDefault();
        var remove_coupon_id = jQuery(this).attr('data-coupon');
        jQuery("#header_cart_remove_coupon").val(remove_coupon_id);

        updateHeaderCart();
        return false;
    });


});

function updateHeaderCart() {

    let ogk_header_cart_form = jQuery('.woocommerce-cart-form');
    let ogk_header_cart_form_current_state = ogk_header_cart_form.serialize();
    // if the form values have changed only
    if (ogk_header_cart_form_initial_state != ogk_header_cart_form_current_state) {

        let data = ogk_header_cart_form.serializeArray(); // form data

        //console.log(data);

        jQuery.ajax({
            url: ogk_header_cart_form.attr('action'),
            data: data,
            dataType: "json",
            type: ogk_header_cart_form.attr('method'), // POST
            beforeSend: function (data) {
               // console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('AjaxErrorThrown:');
                console.log(errorThrown);
            },
            success: function (response) {
               // console.log(response);
                if (response.type == "success") {
                    //console.log(response);
                    jQuery(".cart-wrap").replaceWith(response.cart_html);

                    // update cart count
                    cartCount = response.cart_count;

                    jQuery('.cart-count-number span').html(cartCount);

                    //console.log(response.notice)

                    if (response.notice != '' && cartCount != 0) {
                        jQuery(".update-cart-notice .container-large p").html(response.notice);
                        jQuery(".update-cart-notice").addClass("visible");
                        setTimeout(function () {
                            jQuery(".update-cart-notice").removeClass("visible");
                        }, 2000);
                    }

                    if (cartCount == 0) {

                        //console.log(response.cart_count);

                        jQuery(".mini-cart").toggleClass("cart-open");
                        jQuery("header.container-large, header.checkout-header").toggleClass("cart-open");
                        jQuery("body").removeClass("no-scroll");

                        //jQuery('.cart-count-number-span').html(0);
                        jQuery(".empty-cart-notice").addClass("visible");
                        timeoutHandler = setTimeout(function () {
                            jQuery(".empty-cart-notice").removeClass("visible");
                        }, 2000);
                        miniCartState = 'closed';
                        //console.log(miniCartState);

                    }

                    // update order review section if on checkout page
                    //console.log(response.order_review);
                    jQuery(".order-review-wrap").replaceWith(response.order_review);

                    setTimeout(function (){
                        if(document.querySelector('header').classList.contains('checkout-header')){
                            document.querySelector(".cart-total .woocommerce-Price-amount").innerHTML = document.querySelector(".subtotal-line .woocommerce-Price-amount").innerHTML;
                        }

                    },300)
                  //  console.log(response);
                }

            }
        });

    }

}

//});


/*function openCart() { // used ?????
    console.log('cart_open');
    jQuery(document).on("click", ".header-cart-count, .close-cart span, .close-cart", function () {
        jQuery(".mini-cart").toggleClass("cart-open");
        jQuery("body").toggleClass("cart-open");
    });
}*/

/*function emptyCart() { // used ????
    var timeoutHandler = null;
    jQuery(".header-cart-count").on("click", function () {
        jQuery(".empty-cart-notice").addClass("visible");
        timeoutHandler = setTimeout(function () {
            jQuery(".empty-cart-notice").removeClass("visible");
        }, 2000);
    });
}*/

// Woo after order review ajax
jQuery(document).on('updated_checkout', function (event, data) {
    console.log(data);
    console.log(data.fragments.cart_totals.shipping_total);
    jQuery(".cart_shipping_label").html(data.fragments.cart_shipping_label);
    jQuery(".cart_shipping_total").html(data.fragments.cart_totals_formatted.shipping_total);
    jQuery(".cart_total_tax").html(data.fragments.cart_totals_formatted.total_tax);
    jQuery(".cart_total").html(data.fragments.cart_totals_formatted.total);
});

// JS Variables
//const emailInput = document.getElementById("cd_email_address");
//const firstNameInput = document.getElementById("cd_first_name");
//const lastNameInput = document.getElementById("cd_last_name");
const billingEmail = document.getElementById("billing_email");
const billingFirstName = document.getElementById("billing_first_name");
const billingLastName = document.getElementById("billing_last_name");
const billingAddress1 = document.getElementById("billing_address_1");
const billingCity = document.getElementById("billing_city");
const billingState = document.getElementById("billing_state");
const billingPostcode = document.getElementById("billing_postcode");
const billingCountry = document.getElementById("billing_country");

const shipToOption = jQuery("#ship-to-different-address-checkbox");
const addressInput = document.getElementById("shipping_address_1");
const cityInput = document.getElementById("shipping_city");
const stateInput = document.getElementById("shipping_state");
const zipInput = document.getElementById("shipping_postcode");

// jQuery Variables
var error = jQuery(".error");

var emailInputParent = jQuery(billingEmail).parent();
var emailError = "<p class='error'>Please enter your Email Address</p>";

var firstNameInputParent = jQuery(billingFirstName).parent();
var firstNameError = "<p class='error'>Please enter your First Name</p>";

var lastNameInputParent = jQuery(billingLastName).parent();
var lastNameError = "<p class='error'>Please enter your Last Name</p>";

var billingAddress1InputParent = jQuery(billingAddress1).parent();
var billingAddress1Error = "<p class='error'>Please enter your Address</p>";

var billingStateInputParent = jQuery(billingState).parent();
var billingStateError = "<p class='error'>Please enter your State</p>";

var billingCityInputParent = jQuery(billingCity).parent();
var billingCityError = "<p class='error'>Please enter your City</p>";

var billingPostcodeInputParent = jQuery(billingPostcode).parent();
var billingPostcodeError = "<p class='error'>Please enter your Postcode</p>";

var billingCountryInputParent = jQuery(billingCountry).parent();
var billingCountryError = "<p class='error'>Please enter your Country</p>";


var addressInputParent = jQuery("#shipping_address_1").parent();
var cityInputParent = jQuery("#shipping_city").parent();
var stateInputParent = jQuery("#shipping_state").parent();
var zipInputParent = jQuery("#shipping_postcode").parent();

var activePage = jQuery(".page.active");
var cdPage = jQuery("#customer-details");
var sdPage = jQuery("#shipping-details");
var smPage = jQuery("#shipping-method");
var roPage = jQuery("#review-order");

var cdButton = jQuery("#customer-details .next");
var sdButton = jQuery("#shipping-details .next");
var smButton = jQuery("#shipping-method .next");

var roSide = jQuery("#sidebar-btn-ro");
var cdSide = jQuery("#sidebar-btn-cd");
var sdSide = jQuery("#sidebar-btn-sd");
var smSide = jQuery("#sidebar-btn-sm");

//var inputEmail = jQuery("#cd_email_address_field");

//var timeoutHandler = null;

function checkInputsCustomerDetails() {

    beforeCheckoutValidation();

    // Customer Details Inputs Checker
    if (billingEmail.value && billingFirstName.value && billingLastName.value && billingAddress1.value && billingCity.value && billingState.value && billingPostcode.value && billingCountry.value) {

        validatedStep = 1;
        //console.log(validatedStep);

        //console.log("has value");
        sdPage.addClass("loading");
        cdPage.removeClass("active");
        cdSide.removeClass("active");
        sdSide.addClass("active");
        setTimeout(function () {
            sdPage.removeClass("loading");
            sdPage.addClass("active");
        }, 1000);
    }
    //if (!billingFirstName.value && emailInputParent.html().indexOf("<p class='error'>Please enter your Email Address</p>") == -1) {
    if (!billingEmail.value) {
        //console.log('email_failed');
        emailInputParent.append(emailError);
        //} else {
        //error.remove();
    }
    if (!billingFirstName.value) {
        firstNameInputParent.append(firstNameError);
        //} else {
        //error.remove();
    }
    if (!billingLastName.value) {
        lastNameInputParent.append(lastNameError);
        //else {
        //error.remove();
    }
    if (!billingAddress1.value) {
        billingAddress1InputParent.append(billingAddress1Error);
        //else {
        //error.remove();
    }
    if (!billingCity.value) {
        billingCityInputParent.append(billingCityError);
        //else {
        //error.remove();
    }
    if (!billingState.value) {
        billingStateInputParent.append(billingStateError);
        //else {
        //error.remove();
    }
    if (!billingPostcode.value) {
        billingPostcodeInputParent.append(billingPostcodeError);
        //else {
        //error.remove();
    }
    if (!billingCountry.value) {
        billingCountryInputParent.append(billingCountryError);
        //else {
        //error.remove();
    }

}

function checkInputsShipping() {

    beforeCheckoutValidation();

    // Shipping Details Inputs Checker
    if ((addressInput.value && cityInput.value && zipInput.value) || !shipToOption.is(":checked")) {

        validatedStep = 2;
        //console.log(validatedStep);

        smPage.addClass("loading");
        sdPage.removeClass("active");
        sdSide.removeClass("active");
        smSide.addClass("active");
        timeoutHandler = setTimeout(function () {
            smPage.removeClass("loading");
            smPage.addClass("active");
        }, 1000);
    }
    if (!addressInput.value && addressInputParent.html().indexOf("<p class='error'>Please enter your Address</p>") == -1) {
        addressInputParent.append("<p class='error'>Please enter your Address</p>");
    } //else {
    //error.remove();
    //}
    if (!cityInput.value && cityInputParent.html().indexOf("<p class='error'>Please enter your City</p>") == -1) {
        cityInputParent.append("<p class='error'>Please enter your City</p>");
    } //else {
    //error.remove();
    //}
    if (!zipInput.value && zipInputParent.html().indexOf("<p class='error'>Please enter your Zip Code</p>") == -1) {
        zipInputParent.append("<p class='error'>Please enter your Zip Code</p>");
    } //else {
    //error.remove();
    //}
}

function beforeCheckoutValidation() {

    // remove any previously generated errors
    jQuery(".error").remove();

}

//router for mini cart open
function routerOnRdy(){
    const urlParams = new URLSearchParams(window.location.search);
    const isCartOpen = urlParams.get("cart");

    if (isCartOpen == "true" || isCartOpen == "true" ) {
        document.querySelector(".mini-cart").classList.add('cart-open');
        document.body.classList.add('no-scroll');

    }
}
