<?php
/** =========================================================================================== **/
/** =============================   MAIN WOOCOMMERCE FUNCTIONS  =============================== **/
/** =========================================================================================== **/


// Remove Woo content wrappers
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);


/** =========================================================================================== **/


// Add OGK Wrappers
add_action('woocommerce_before_main_content', 'ogk_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'ogk_wrapper_end', 10);

function ogk_wrapper_start() {
    echo '<section id="ogk-woo">';
}

function ogk_wrapper_end() {
    echo '</section>';
}


/** =========================================================================================== **/


// Theme - Add WooCommerce Support
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');
function mytheme_add_woocommerce_support() {
    add_theme_support('woocommerce');
}

add_theme_support('wc-product-gallery-zoom');
add_theme_support('wc-product-gallery-lightbox');
add_theme_support('wc-product-gallery-slider');


/** =========================================================================================== **/


// WooCommerce - Remove Woo Styles
add_filter('woocommerce_enqueue_styles', '__return_false');