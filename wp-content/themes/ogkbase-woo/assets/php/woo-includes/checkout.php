<?php
/** =========================================================================================== **/
/** ============================   CHECKOUT PAGE FUNCTIONS  =================================== **/
/** =========================================================================================== **/

remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10);
remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);

// REORDERING CHECKOUT BILLING FIELDS (WOOCOMMERCE 3+)

add_filter("woocommerce_checkout_fields", "custom_wc_checkout_fields");
function custom_wc_checkout_fields($fields) {

$fields['shipping']['shipping_address_1']['placeholder'] = 'Test';
$fields['shipping']['shipping_address_2']['placeholder'] = 'Address Line 2';
return $fields;

}

add_filter("woocommerce_checkout_fields", "reordering_checkout_fields", 15, 1);
function reordering_checkout_fields($fields) {

## ---- 1. REORDERING BILLING FIELDS ---- ##

// Set the order of the fields
$billing_order = array(
'billing_email',
'billing_first_name',
'billing_last_name',
'billing_phone',
'billing_company',
'billing_address_1',
'billing_address_2',
'billing_postcode',
'billing_city',
'billing_state',
'billing_country'
);

$count = 0;
$priority = 10;

// Updating the 'priority' argument
foreach ($billing_order as $field_name) {
$count++;
$fields['billing'][$field_name]['priority'] = $count * $priority;
}

## ---- 2. CHANGING SOME CLASSES FOR BILLING FIELDS ---- ##

$fields['billing']['billing_email']['class'] = array('form-row-first');
$fields['billing']['billing_phone']['class'] = array('form-row-last');

$fields['billing']['billing_postcode']['class'] = array('form-row-first');
$fields['billing']['billing_city']['class'] = array('form-row-last');

$fields['billing']['billing_email']['required'] = true;
$fields['billing']['billing_first_name']['required'] = true;
$fields['billing']['billing_last_name']['required'] = true;
$fields['billing']['billing_phone']['required'] = false;
$fields['billing']['billing_address_1']['required'] = true;
$fields['billing']['billing_address_2']['required'] = false;
$fields['billing']['billing_postcode']['required'] = true;
$fields['billing']['billing_city']['required'] = true;
$fields['billing']['billing_state']['required'] = true;
$fields['billing']['billing_company']['required'] = false;
$fields['billing']['billing_country']['required'] = true;
$fields['shipping']['shipping_first_name']['required'] = false;
$fields['shipping']['shipping_last_name']['required'] = false;

// remove unwanted fields
unset($fields['billing']['billing_company']);

## ---- RETURN THE BILLING FIELDS CUSTOMIZED ---- ##

return $fields;
}

// remove Order Notes from checkout field in Woocommerce
add_filter('woocommerce_checkout_fields', 'alter_woocommerce_checkout_fields');
function alter_woocommerce_checkout_fields($fields) {
unset($fields['order']['order_comments']);
return $fields;
}

add_action('woocommerce_before_checkout_form', 'ogk_checkout_sidebar');
function ogk_checkout_sidebar() {
get_template_part("includes/sidebar", "checkout");
}


add_filter('woocommerce_shipping_package_name', 'custom_shipping_package_name');
function custom_shipping_package_name($name) {
return '';
}

add_action('woocommerce_after_shipping_rate', 'action_after_shipping_rate', 20, 2);
function action_after_shipping_rate($method, $index) {
// Targeting checkout page only:
if ( is_cart() ) return; // Exit on cart page

if ( 'flat_rate:1' === $method->id || 'flat_rate:2' === $method->id ) {
echo __("<p class='method-description'>Estimated Delivery<br>Delivered within 1-3 business days</p>");
echo __("<div class='radio-button'></div>");
}
if ( 'flat_rate:6' === $method->id || 'flat_rate:3' === $method->id ) {
echo __("<p class='method-description'>Estimated Delivery<br>Delivered within 3-5 business days</p>");
echo __("<div class='radio-button'></div>");
}
if ( 'flat_rate:7' === $method->id || 'flat_rate:4' === $method->id ) {
echo __("<p class='method-description'>Estimated Delivery<br>Delivered within 2-3 business days</p>");
echo __("<div class='radio-button'></div>");
}
if ( 'flat_rate:8' === $method->id || 'flat_rate:5' === $method->id ) {
echo __("<p class='method-description'>Estimated Delivery<br>Delivered in 1 business day</p>");
echo __("<div class='radio-button'></div>");
}
}

remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);


add_action('woocommerce_checkout_after_customer_details', 'woocommerce_checkout_payment', 20);

// add custom ajax fragments on checkout order review
add_filter('woocommerce_update_order_review_fragments', 'my_custom_shipping_table_update');
function my_custom_shipping_table_update($fragments) {

foreach (WC()->session->get('shipping_for_package_0')['rates'] as $method_id => $rate):

if ( WC()->session->get('chosen_shipping_methods')[0] == $method_id ):
$rate_label = $rate->label; // The shipping method label name
endif;

endforeach;

$fragments['cart_shipping_label'] = $rate_label;
$fragments['cart_totals'] = WC()->cart->get_totals();

$formatted_totals = array();
foreach (WC()->cart->get_totals() as $key => $value) :
$formatted_totals[$key] = wc_price($value);
endforeach;

$fragments['cart_totals_formatted'] = $formatted_totals;

return $fragments;
}