<?php
$current_user = wp_get_current_user();
$customer_id = get_current_user_id();

if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
    $get_addresses = apply_filters( 'woocommerce_my_account_get_addresses', array(
        'billing' => __( 'Billing address', 'woocommerce' ),
        'shipping' => __( 'Shipping address', 'woocommerce' ),
    ), $customer_id );
} else {
    $get_addresses = apply_filters( 'woocommerce_my_account_get_addresses', array(
        'billing' => __( 'Billing address', 'woocommerce' ),
    ), $customer_id );
}

$oldcol = 1;
$col    = 1;

$shipping_address_1 = get_user_meta($current_user->ID, 'shipping_address_1', true);
$shipping_address_2 = get_user_meta($current_user->ID, 'shipping_address_2', true);
$city = get_user_meta($current_user->ID, 'shipping_city', true);
$state = get_user_meta($current_user->ID, 'shipping_state', true);
$zip = get_user_meta($current_user->ID, 'shipping_postcode', true);
$phone = get_user_meta($current_user->ID, 'billing_phone', true);

?>

<div class="account-content">
    <div class="row">
        <div class="account-details">
            <div class="title">
                <h4>Account Details</h4>
                <h5 class="lightGray"><?= esc_html( $current_user->display_name ) ?></h5>
            </div>
            <div class="login-info">
                <p><span>Email Address</span></p>
                <p class="lightGray"><?= esc_html( $current_user->user_email ) ?></p>
                <p><span>Password</span></p>
                <p class="lightGray">●●●●●●●●●●●●●●</p>
            </div>
            <div class="btn-wrap">
                <a href="/my-account/edit-account/" class="btn dark-hover">Account Details</a>
            </div>
        </div>
        <div class="shipping-info">
            <div class="title">
                <h4>Shipping Information</h4>
                <h5 class="lightGray">Address Book</h5>
            </div>
            <div class="address">
                <p class="lightGray">
                    <?= esc_html( $current_user->display_name ) ?>
                    <br><?= $shipping_address_1 ?>
                    <?php if($shipping_address_2): ?><br><?= $shipping_address_2 ?><?php endif; ?>
                    <br><?= $city ?>, <?= $state ?> <?= $zip ?>
                    <br><?= $phone ?>
                </p>
                <div class="btn-wrap">
                    <a href="/my-account/edit-address/" class="btn dark-hover">Edit Shipping Info</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="order-history">
            <h4>Recent Orders</h4>
            <?php get_template_part('woocommerce/myaccount/my-orders') ?>
        </div>
    </div>
</div>