<?php
/** =========================================================================================== **/
/** ============================   LOGIN PAGE FUNCTIONS  ====================================== **/
/** =========================================================================================== **/


add_action('woocommerce_before_customer_login_form', 'ogk_add_login_page_wrapper_start');
function ogk_add_login_page_wrapper_start() {
    echo '<div class="ogk-login-wrap">';
}

add_action('woocommerce_after_customer_login_form', 'ogk_add_login_page_wrapper_end');
function ogk_add_login_page_wrapper_end() {
    echo '</div>';
}

add_action('woocommerce_before_lost_password_form', 'ogk_add_reset_password_wrapper_start');
function ogk_add_reset_password_wrapper_start() {
    echo '<div class="ogk-login-wrap"><h1>Lost Password</h1>';
}

add_action('woocommerce_after_lost_password_form', 'ogk_add_reset_password_wrapper_end');
function ogk_add_reset_password_wrapper_end() {
    echo '</div>';
}


/** =========================================================================================== **/