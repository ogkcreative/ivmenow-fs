<?php
/** =========================================================================================== **/
/** ============================   DASHBOARD FUNCTIONS  ======================================= **/
/** =========================================================================================== **/

add_action('woocommerce_before_account_navigation', 'ogk_before_navigation');
function ogk_before_navigation() {

    $current_user = wp_get_current_user();

    ob_start(); ?>

    <div class="ogk-welcome-message"><h4>
            <?php printf(
            /* translators: 1: user display name 2: logout url */
                __('Hello %1$s!', 'woocommerce'),
                esc_html($current_user->display_name),
                esc_url(wc_logout_url())
            ); ?>
        </h4></div>

    <div class="mobile-dash-menu" style="display: none;">
        <div class="menu-text">
            <h5>Menu</h5>
        </div>
        <div class="menu-plus">
            <span></span>
            <span></span>
        </div>
    </div>



    <?php $profile = ob_get_clean();

    echo $profile;


}

/** =========================================================================================== **/

add_action( 'woocommerce_account_dashboard', 'ogk_custom_dashboard' );
function ogk_custom_dashboard() {
    ob_start();
    get_template_part( 'assets/php/woo-includes/ogk-my-account');
    echo ob_get_clean();
}