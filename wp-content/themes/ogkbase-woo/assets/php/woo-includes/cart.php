<?php
/** =========================================================================================== **/
/** ============================   MINI CART FUNCTIONS  ======================================= **/
/** =========================================================================================== **/


function custom_mini_cart() {
    echo '<a href="#" class="dropdown-back" data-toggle="dropdown"> ';
    echo '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
    echo '<div class="basket-item-count" style="display: inline;">';
    echo '<span class="cart-items-count count">';
    echo WC()->cart->get_cart_contents_count();
    echo '</span>';
    echo '</div>';
    echo '</a>';
    echo '<ul class="dropdown-menu dropdown-menu-mini-cart">';
    echo '<li> <div class="widget_shopping_cart_content">';
    woocommerce_mini_cart();
    echo '</div></li></ul>';

}

/** =========================================================================================== **/

function ogk_mini_cart() {

    ob_start();

    get_template_part("includes/mini-cart");

    $mini_cart = ob_get_clean();

    return $mini_cart;

}

/** =========================================================================================== **/

// Add custom  add-to-cart fragments
add_filter('woocommerce_add_to_cart_fragments', 'add_to_cart_fragments');
function add_to_cart_fragments($fragments) {

    // required for single product add to cart only below
    ob_start();
    echo ogk_mini_cart();
    $fragments['minicart'] = ob_get_clean();
    ob_start();
    echo "<div class='cart-count-number'><span>" . WC()->cart->cart_contents_count . "</span></div>";
    $fragments['cartcountnumber'] = ob_get_clean();

    return $fragments;
}

/** =========================================================================================== **/

add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');

function woocommerce_ajax_add_to_cart() {

    $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
    $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
    $variation_id = absint($_POST['variation_id']);
    $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
    $product_status = get_post_status($product_id);

    if ( $passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status ) {

        do_action('woocommerce_ajax_added_to_cart', $product_id);

        if ( 'yes' === get_option('woocommerce_cart_redirect_after_add') ) {
            wc_add_to_cart_message(array(
                $product_id => $quantity,
                'minicart' => ogk_mini_cart()
            ), true);
        }

        WC_AJAX:: get_refreshed_fragments();
    } else {

        $data = array(
            'error' => true,
            'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id)
        );

        echo wp_send_json($data);
    }

    wp_die();
}

/** =========================================================================================== **/

function disable_shipping_calc_on_cart($show_shipping) {
    if ( is_cart() ) {
        return false;
    }
    return $show_shipping;
}

add_filter('woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99);

/** =========================================================================================== **/

/// header cart ajax handler
// catch all ajax request for archives
// add your query arguments in 'functions-archive-queries.php'.
add_action('wp_ajax_header-cart-ajax', 'ajax_header_cart_ajax');
add_action('wp_ajax_nopriv_header-cart-ajax', 'ajax_header_cart_ajax');

function ajax_header_cart_ajax() {

    $result = array();
    $result['notice'] = '';

    // remove product
    if ( $_POST['remove_product'] || $_POST['remove_coupon'] ) :

        if ( $_POST['remove_product'] ) :
            //$cart_item_key = WC()->cart->generate_cart_id($_POST['remove_product']);
            WC()->cart->remove_cart_item($_POST['remove_product']);
            $result['notice'] = 'Product Removed';

        endif;

        if ( $_POST['remove_coupon'] ) :

            WC()->cart->remove_coupon($_POST['remove_coupon']);
            $result['notice'] = 'Coupon Removed';

        endif;

    else:

        // cart quantities
        if ( $_POST['cart'] ) :
            foreach ($_POST['cart'] as $cart_item_key => $vars) :
                WC()->cart->set_quantity($cart_item_key, $vars['qty']);
                $result['notice'] = 'Quantity Updated';
            endforeach;
        endif;

        // coupons
        if ( $_POST['coupon_code'] ) :

            $wc_coupon = new WC_Coupon($_POST['coupon_code']);
            if ( $wc_coupon->amount != 0 ) :

                //$result['notices'][] = 'Coupon Code is valid';

                if ( !WC()->cart->has_discount($_POST['coupon_code']) ) :
                    WC()->cart->add_discount($_POST['coupon_code']);
                    $result['notice'] = 'Coupon Applied';
                endif;

            else:

                $result['notice'] = 'Coupon Code is not valid';

            endif;

        endif;

    endif;

    ob_start();
    get_template_part("includes/ogk-cart");

    $result['cart_html'] = ob_get_clean();

    ob_start();
    get_template_part("woocommerce/checkout/review-order");
    $result['order_review'] = ob_get_clean();

    ?>

    <?php
    //$result['ajax_content'] = ob_get_clean();
    global $woocommerce;
    $result['cart_count'] = $woocommerce->cart->cart_contents_count;

    //$result['woo_notices'] = wc_print_notices();
    $result['type'] = "success";
    ?>

    <?php
    if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }

    die();

}