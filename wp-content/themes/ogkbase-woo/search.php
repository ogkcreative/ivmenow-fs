<?php
get_header();

global $wp_query;
$search_count = $wp_query->found_posts;
?>



    <section class="search-results-section">
        <div class="title-wrap container flex flex-wrap flex-ac flex-sb">
            <div class="fifty flex flex-jc">
                <h1 class="h3"><?= $search_count ?> <?php _e( 'Search results found for', 'locale' ); ?>: <b>"<?php the_search_query(); ?>"</b></h1>
            </div>
            <div class="fifty flex flex-jc">
                <div class="search-container">
                    <?php get_search_form(); ?>
                    <svg onclick="document.getElementById('searchsubmit').click();" xmlns="http://www.w3.org/2000/svg" width="22.349" height="17.62" viewBox="0 0 22.349 17.62">
                        <path id="Path_30" data-name="Path 30" d="M-1894.613,3327.63l-1.244,1.244,6.686,6.686h-18.981v1.76h18.981l-6.686,6.686,1.244,1.244,8.81-8.81Z" transform="translate(1908.152 -3327.63)" fill="#0246A8"/>
                    </svg>
                </div>
            </div>

        </div>
        <?php if(have_posts()): ?>
            <div class="posts">
                <?php while(have_posts()): the_post(); ?>
                    <?php get_template_part("includes/content", "search"); ?>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </section>



<?php get_footer();