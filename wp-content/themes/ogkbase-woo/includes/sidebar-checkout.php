<section class="sidebar-progress">
    <div class="logo">
        <?= file_get_contents(get_field('logo', 'options')) ?>
    </div>
    <div class="checkout-steps">
        <a href="#customer-details" id="sidebar-btn-cd" class="checkout-sidebar-btn active">Customer Details</a>
        <a href="#shipping-details" id="sidebar-btn-sd" class="checkout-sidebar-btn">Shipping Details</a>
        <a href="#shipping-method" id="sidebar-btn-sm" class="checkout-sidebar-btn">Shipping Method</a>
        <a href="#review-order" id="sidebar-btn-ro" class="checkout-sidebar-btn">Order Review</a>
        <a href="#payment-method" id="sidebar-btn-pm" class="checkout-sidebar-btn">Payment Method</a>
    </div>
</section>