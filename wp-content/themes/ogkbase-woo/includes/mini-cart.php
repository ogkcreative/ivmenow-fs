<div class="mini-cart-wrap">
  <!---  Filtered Cart  ---->

  <?php //if ( WC()->cart->get_cart_contents_count() == 0 ): ?>

  <!--<div class="empty-cart-notice">
    <div class="container-large">
      <p>You have no items in your cart</p>
    </div>
  </div>-->

  <?php //else: ?>

    <div class="item-added">
        <div class="container-large">
            <p>Item(s) successfully added to cart!</p>
        </div>
    </div>
    <div class="empty-cart-notice">
        <div class="container-large">
            <p>You have no items in your cart</p>
        </div>
    </div>
    <?php // notice populated with ajax when required ?>
    <div class="update-cart-notice">
        <div class="container-large">
            <p></p>
        </div>
    </div>

    <div class="mini-cart">
      <div class="container-large">
      <div class="close-cart">
        <?= file_get_contents(get_field('close_icon', 'options')) ?>
      </div>
      <?php get_template_part("includes/ogk-cart"); ?>
    </div>

  </div>

  <?php //endif; ?>
</div>