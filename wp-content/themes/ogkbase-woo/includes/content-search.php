<?php

$post = get_post();
$type = $post->post_type;

$excerpt = get_field('excerpt');

$post_label = '';

if( get_post_type() == 'page' ) {
    //todo post tax
    $post_label = 'Service';
} else {
    $post_label = get_post_type();
}

?>

<div class="search-card">
    <div class="search-content-inner">
        <div class="h4"><?= $post_label ?></div>
        <h2 class="h3 search-title"><?= get_the_title() ?></h2>
        <?php if($excerpt): echo $excerpt; endif; ?>
        <div class="read-btn-wrap">
            <a href="<?= get_the_permalink() ?>" class="read-more-btn"><span>Read More</span></a>
        </div>
    </div>
</div>