<div class="ogk-mini-cart-scripts">
    <?php if ( WC()->cart->get_cart_contents_count() == 0 ): ?>
        <script>
            jQuery(document).ready(function ($) {
                var timeoutHandler = null;
                jQuery(".header-cart-count").on("click", function () {
                    jQuery(".empty-cart-notice").addClass("visible");
                    timeoutHandler = setTimeout(function () {
                        jQuery(".empty-cart-notice").removeClass("visible");
                    }, 2000);
                });
            });
        </script>
    <?php else: ?>
    <script>
        jQuery(document).ready(function ($) {
            jQuery(document).on("click", ".header-cart-count, .close-cart svg" , function () {
                jQuery(".mini-cart").toggleClass("cart-open");
                jQuery("header.container-large, header.checkout-header").toggleClass("cart-open");
            });
        });

    </script>
    <?php endif; ?>
</div>