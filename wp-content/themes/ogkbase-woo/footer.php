</main>

<footer class="container-large">
    <div class="footer-logo">
        <?= file_get_contents(get_field('logo', 'options')); ?>
        <p class="disclaimer">*These statements have not been evaluated by the Food and Drug Administration. This product is not intended to diagnose, treat, cure, or prevent any disease.  <a href="/fda-disclaimer/">Read Full Disclaimer</a></p>
        <p class="copyright">
            <span>&copy;<?= date("Y"); ?> <?= get_field('company_name','options') ?>. All rights reserved.</span><br>
        </p>
    </div>
    <div class="footer-right">
        <div class="footer-menu">
            <?php
            wp_nav_menu( array(
                'menu' => 'footer-menu'
            ) );
            ?>
        </div>
        <div class="footer-social">
            <?php get_template_part("includes/social-icons") ?>
        </div>
    </div>
</footer><!--.footer-->
<?php wp_footer(); ?>

</body>
</html>