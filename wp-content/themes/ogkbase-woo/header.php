<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<title><?= get_the_title() ?> | <?= get_bloginfo( 'title' ); ?></title>
	<meta name="viewport" content="width=device-width">

	<?php $ga_code = get_field( 'google_analytics_code', 'options' );
	if($ga_code): ?>
	    <?= $ga_code ?>
	<?php endif; ?>

	<?php wp_head(); ?>

    <link rel="stylesheet" href="https://use.typekit.net/xqo2ruy.css">


    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#3f3f3f">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">


    <style>
        :root {
            --woo-skin-color: <?= get_field('woo_skin_color', 'options') ?>;
            --cart-content-color: <?= get_field('cart_content_color', 'options') ?>;
            --cart-text-color: <?= get_field('cart_text_color', 'options') ?>;
            --cart-bg-color: <?= get_field('cart_bg_color', 'options') ?>;
            --cart-counter-color: <?= get_field('cart_counter_color', 'options') ?>;
            --cart-counter-bg-color: <?= get_field('cart_counter_bg_color', 'options') ?>;
            --inactive-color: <?= get_field('inactive_color', 'options') ?>;
            --checkout-bg-color: <?= get_field('checkout_bg_color', 'options') ?>;
            --checkout-shipping-inactive-color: <?= get_field('checkout_shipping_inactive_color', 'options') ?>;
        }
    </style>


</head>

<body <?php body_class(); ?>>


<?php get_template_part("includes/mini-cart") ?>


<?php if(!is_page('Checkout')): ?>
<header class="container-large">
    <div class="header-left desktop">
        <?php
        wp_nav_menu( array(
            'menu' => 'main-menu'
        ) );
        ?>
    </div>
    <div class="logo">
        <a href="<?php echo get_home_url(); ?>" title="<?= wp_title() ?>">
            <?php if ( get_field( 'logo', 'options' ) ): ?>
                <?= file_get_contents(get_field( 'logo', 'options' )); ?>
            <?php else: ?>
                <h2><?= wp_title(); ?></h2>
            <?php endif; ?>
        </a>
    </div>
	<div class="header-right desktop">
        <?php if(is_user_logged_in()): ?>
            <a href="<?= wc_get_page_permalink( 'myaccount' ); ?>" class="other-link">Account</a>
        <?php else: ?>
            <a href="<?= wc_get_page_permalink( 'myaccount' ); ?>" class="other-link">Login</a>
        <?php endif; ?>
        <?php
        $cart_count = WC()->cart->get_cart_contents_count();
        $cart_total = WC()->cart->get_cart_total(); ?>
        <div class="header-cart-count">
            <div class="icon">
                Bag
                <div class="cart-count-number">
                    <span class="cart-count-number-span"><?= WC()->cart->get_cart_contents_count() ?></span>
                </div>
            </div>
        </div>
        <div class="header-hamburger mobile" style="display: none;">
            <div class="hamburger">
                <span></span>
                <span></span>
            </div>
        </div>
	</div>
    <div class="nav-menu">
        <div class="container">
            <?php
            wp_nav_menu( array(
                'menu' => 'mobile-menu'
            ) );
            ?>
            <div class="chunky-links">
                <?php if(is_user_logged_in()): ?>
                    <a href="<?= wc_get_page_permalink( 'myaccount' ); ?>" class="chunky-link">Account</a>
                <?php else: ?>
                    <a href="<?= wc_get_page_permalink( 'myaccount' ); ?>" class="chunky-link">Login</a>
                <?php endif; ?>
                <a href="<?= wc_get_page_permalink( 'shop' ); ?>" class="chunky-link">Shop</a>
            </div>
        </div>
    </div>
</header>
<?php else: ?>
    <?php
    $cart_count = WC()->cart->get_cart_contents_count();
    $cart_total = WC()->cart->get_cart_total(); ?>


<header class="checkout-header">
    <div class="container">
        <div class="left">
            <a href="<?= wc_get_page_permalink( 'shop' ); ?>">Keep Shopping</a>
        </div>
        <div class="right">
            <div class="cart-total">
                <?= $cart_total ?>
            </div>
            <div class="cart-count header-cart-count cart-count-number">
              <span class="cart-count-number-span"><?= $cart_count ?></span>
            </div>
        </div>
    </div>
</header>

<?php endif; ?>
<main class="site-content">





