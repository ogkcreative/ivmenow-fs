
;

(function ($, window, document, undefined) {
  function Owl(element, options) {
    this.settings = null;

    this.options = $.extend({}, Owl.Defaults, options);

    this.$element = $(element);

    this._handlers = {};

    this._plugins = {};

    this._supress = {};

    this._current = null;

    this._speed = null;

    this._coordinates = [];

    this._breakpoint = null;

    this._width = null;

    this._items = [];

    this._clones = [];

    this._mergers = [];

    this._widths = [];

    this._invalidated = {};

    this._pipe = [];

    this._drag = {
      time: null,
      target: null,
      pointer: null,
      stage: {
        start: null,
        current: null
      },
      direction: null
    };

    this._states = {
      current: {},
      tags: {
        'initializing': ['busy'],
        'animating': ['busy'],
        'dragging': ['interacting']
      }
    };
    $.each(['onResize', 'onThrottledResize'], $.proxy(function (i, handler) {
      this._handlers[handler] = $.proxy(this[handler], this);
    }, this));
    $.each(Owl.Plugins, $.proxy(function (key, plugin) {
      this._plugins[key.charAt(0).toLowerCase() + key.slice(1)] = new plugin(this);
    }, this));
    $.each(Owl.Workers, $.proxy(function (priority, worker) {
      this._pipe.push({
        'filter': worker.filter,
        'run': $.proxy(worker.run, this)
      });
    }, this));
    this.setup();
    this.initialize();
  }


  Owl.Defaults = {
    items: 3,
    loop: false,
    center: false,
    rewind: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    freeDrag: false,
    margin: 0,
    stagePadding: 0,
    merge: false,
    mergeFit: true,
    autoWidth: false,
    startPosition: 0,
    rtl: false,
    smartSpeed: 250,
    fluidSpeed: false,
    dragEndSpeed: false,
    responsive: {},
    responsiveRefreshRate: 200,
    responsiveBaseElement: window,
    fallbackEasing: 'swing',
    info: false,
    nestedItemSelector: false,
    itemElement: 'div',
    stageElement: 'div',
    refreshClass: 'owl-refresh',
    loadedClass: 'owl-loaded',
    loadingClass: 'owl-loading',
    rtlClass: 'owl-rtl',
    responsiveClass: 'owl-responsive',
    dragClass: 'owl-drag',
    itemClass: 'owl-item',
    stageClass: 'owl-stage',
    stageOuterClass: 'owl-stage-outer',
    grabClass: 'owl-grab'
  };

  Owl.Width = {
    Default: 'default',
    Inner: 'inner',
    Outer: 'outer'
  };

  Owl.Type = {
    Event: 'event',
    State: 'state'
  };

  Owl.Plugins = {};

  Owl.Workers = [{
    filter: ['width', 'settings'],
    run: function () {
      this._width = this.$element.width();
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      cache.current = this._items && this._items[this.relative(this._current)];
    }
  }, {
    filter: ['items', 'settings'],
    run: function () {
      this.$stage.children('.cloned').remove();
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      var margin = this.settings.margin || '',
          grid = !this.settings.autoWidth,
          rtl = this.settings.rtl,
          css = {
        'width': 'auto',
        'margin-left': rtl ? margin : '',
        'margin-right': rtl ? '' : margin
      };
      !grid && this.$stage.children().css(css);
      cache.css = css;
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      var width = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
          merge = null,
          iterator = this._items.length,
          grid = !this.settings.autoWidth,
          widths = [];
      cache.items = {
        merge: false,
        width: width
      };

      while (iterator--) {
        merge = this._mergers[iterator];
        merge = this.settings.mergeFit && Math.min(merge, this.settings.items) || merge;
        cache.items.merge = merge > 1 || cache.items.merge;
        widths[iterator] = !grid ? this._items[iterator].width() : width * merge;
      }

      this._widths = widths;
    }
  }, {
    filter: ['items', 'settings'],
    run: function () {
      var clones = [],
          items = this._items,
          settings = this.settings,
      view = Math.max(settings.items * 2, 4),
          size = Math.ceil(items.length / 2) * 2,
          repeat = settings.loop && items.length ? settings.rewind ? view : Math.max(view, size) : 0,
          append = '',
          prepend = '';
      repeat /= 2;

      while (repeat > 0) {
        clones.push(this.normalize(clones.length / 2, true));
        append = append + items[clones[clones.length - 1]][0].outerHTML;
        clones.push(this.normalize(items.length - 1 - (clones.length - 1) / 2, true));
        prepend = items[clones[clones.length - 1]][0].outerHTML + prepend;
        repeat -= 1;
      }

      this._clones = clones;
      $(append).addClass('cloned').appendTo(this.$stage);
      $(prepend).addClass('cloned').prependTo(this.$stage);
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function () {
      var rtl = this.settings.rtl ? 1 : -1,
          size = this._clones.length + this._items.length,
          iterator = -1,
          previous = 0,
          current = 0,
          coordinates = [];

      while (++iterator < size) {
        previous = coordinates[iterator - 1] || 0;
        current = this._widths[this.relative(iterator)] + this.settings.margin;
        coordinates.push(previous + current * rtl);
      }

      this._coordinates = coordinates;
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function () {
      var padding = this.settings.stagePadding,
          coordinates = this._coordinates,
          css = {
        'width': Math.ceil(Math.abs(coordinates[coordinates.length - 1])) + padding * 2,
        'padding-left': padding || '',
        'padding-right': padding || ''
      };
      this.$stage.css(css);
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      var iterator = this._coordinates.length,
          grid = !this.settings.autoWidth,
          items = this.$stage.children();

      if (grid && cache.items.merge) {
        while (iterator--) {
          cache.css.width = this._widths[this.relative(iterator)];
          items.eq(iterator).css(cache.css);
        }
      } else if (grid) {
        cache.css.width = cache.items.width;
        items.css(cache.css);
      }
    }
  }, {
    filter: ['items'],
    run: function () {
      this._coordinates.length < 1 && this.$stage.removeAttr('style');
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      cache.current = cache.current ? this.$stage.children().index(cache.current) : 0;
      cache.current = Math.max(this.minimum(), Math.min(this.maximum(), cache.current));
      this.reset(cache.current);
    }
  }, {
    filter: ['position'],
    run: function () {
      this.animate(this.coordinates(this._current));
    }
  }, {
    filter: ['width', 'position', 'items', 'settings'],
    run: function () {
      var rtl = this.settings.rtl ? 1 : -1,
          padding = this.settings.stagePadding * 2,
          begin = this.coordinates(this.current()) + padding,
          end = begin + this.width() * rtl,
          inner,
          outer,
          matches = [],
          i,
          n;

      for (i = 0, n = this._coordinates.length; i < n; i++) {
        inner = this._coordinates[i - 1] || 0;
        outer = Math.abs(this._coordinates[i]) + padding * rtl;

        if (this.op(inner, '<=', begin) && this.op(inner, '>', end) || this.op(outer, '<', begin) && this.op(outer, '>', end)) {
          matches.push(i);
        }
      }

      this.$stage.children('.active').removeClass('active');
      this.$stage.children(':eq(' + matches.join('), :eq(') + ')').addClass('active');
      this.$stage.children('.center').removeClass('center');

      if (this.settings.center) {
        this.$stage.children().eq(this.current()).addClass('center');
      }
    }
  }];

  Owl.prototype.initializeStage = function () {
    this.$stage = this.$element.find('.' + this.settings.stageClass); 

    if (this.$stage.length) {
      return;
    }

    this.$element.addClass(this.options.loadingClass); 

    this.$stage = $('<' + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'); 

    this.$element.append(this.$stage.parent());
  };


  Owl.prototype.initializeItems = function () {
    var $items = this.$element.find('.owl-item'); 

    if ($items.length) {
      this._items = $items.get().map(function (item) {
        return $(item);
      });
      this._mergers = this._items.map(function () {
        return 1;
      });
      this.refresh();
      return;
    } 


    this.replace(this.$element.children().not(this.$stage.parent())); 

    if (this.isVisible()) {
      this.refresh();
    } else {
      this.invalidate('width');
    }

    this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass);
  };


  Owl.prototype.initialize = function () {
    this.enter('initializing');
    this.trigger('initialize');
    this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl);

    if (this.settings.autoWidth && !this.is('pre-loading')) {
      var imgs, nestedSelector, width;
      imgs = this.$element.find('img');
      nestedSelector = this.settings.nestedItemSelector ? '.' + this.settings.nestedItemSelector : undefined;
      width = this.$element.children(nestedSelector).width();

      if (imgs.length && width <= 0) {
        this.preloadAutoWidthImages(imgs);
      }
    }

    this.initializeStage();
    this.initializeItems(); 

    this.registerEventHandlers();
    this.leave('initializing');
    this.trigger('initialized');
  };


  Owl.prototype.isVisible = function () {
    return this.settings.checkVisibility ? this.$element.is(':visible') : true;
  };


  Owl.prototype.setup = function () {
    var viewport = this.viewport(),
        overwrites = this.options.responsive,
        match = -1,
        settings = null;

    if (!overwrites) {
      settings = $.extend({}, this.options);
    } else {
      $.each(overwrites, function (breakpoint) {
        if (breakpoint <= viewport && breakpoint > match) {
          match = Number(breakpoint);
        }
      });
      settings = $.extend({}, this.options, overwrites[match]);

      if (typeof settings.stagePadding === 'function') {
        settings.stagePadding = settings.stagePadding();
      }

      delete settings.responsive; 

      if (settings.responsiveClass) {
        this.$element.attr('class', this.$element.attr('class').replace(new RegExp('(' + this.options.responsiveClass + '-)\\S+\\s', 'g'), '$1' + match));
      }
    }

    this.trigger('change', {
      property: {
        name: 'settings',
        value: settings
      }
    });
    this._breakpoint = match;
    this.settings = settings;
    this.invalidate('settings');
    this.trigger('changed', {
      property: {
        name: 'settings',
        value: this.settings
      }
    });
  };


  Owl.prototype.optionsLogic = function () {
    if (this.settings.autoWidth) {
      this.settings.stagePadding = false;
      this.settings.merge = false;
    }
  };


  Owl.prototype.prepare = function (item) {
    var event = this.trigger('prepare', {
      content: item
    });

    if (!event.data) {
      event.data = $('<' + this.settings.itemElement + '/>').addClass(this.options.itemClass).append(item);
    }

    this.trigger('prepared', {
      content: event.data
    });
    return event.data;
  };


  Owl.prototype.update = function () {
    var i = 0,
        n = this._pipe.length,
        filter = $.proxy(function (p) {
      return this[p];
    }, this._invalidated),
        cache = {};

    while (i < n) {
      if (this._invalidated.all || $.grep(this._pipe[i].filter, filter).length > 0) {
        this._pipe[i].run(cache);
      }

      i++;
    }

    this._invalidated = {};
    !this.is('valid') && this.enter('valid');
  };


  Owl.prototype.width = function (dimension) {
    dimension = dimension || Owl.Width.Default;

    switch (dimension) {
      case Owl.Width.Inner:
      case Owl.Width.Outer:
        return this._width;

      default:
        return this._width - this.settings.stagePadding * 2 + this.settings.margin;
    }
  };


  Owl.prototype.refresh = function () {
    this.enter('refreshing');
    this.trigger('refresh');
    this.setup();
    this.optionsLogic();
    this.$element.addClass(this.options.refreshClass);
    this.update();
    this.$element.removeClass(this.options.refreshClass);
    this.leave('refreshing');
    this.trigger('refreshed');
  };


  Owl.prototype.onThrottledResize = function () {
    window.clearTimeout(this.resizeTimer);
    this.resizeTimer = window.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
  };


  Owl.prototype.onResize = function () {
    if (!this._items.length) {
      return false;
    }

    if (this._width === this.$element.width()) {
      return false;
    }

    if (!this.isVisible()) {
      return false;
    }

    this.enter('resizing');

    if (this.trigger('resize').isDefaultPrevented()) {
      this.leave('resizing');
      return false;
    }

    this.invalidate('width');
    this.refresh();
    this.leave('resizing');
    this.trigger('resized');
  };


  Owl.prototype.registerEventHandlers = function () {
    if ($.support.transition) {
      this.$stage.on($.support.transition.end + '.owl.core', $.proxy(this.onTransitionEnd, this));
    }

    if (this.settings.responsive !== false) {
      this.on(window, 'resize', this._handlers.onThrottledResize);
    }

    if (this.settings.mouseDrag) {
      this.$element.addClass(this.options.dragClass);
      this.$stage.on('mousedown.owl.core', $.proxy(this.onDragStart, this));
      this.$stage.on('dragstart.owl.core selectstart.owl.core', function () {
        return false;
      });
    }

    if (this.settings.touchDrag) {
      this.$stage.on('touchstart.owl.core', $.proxy(this.onDragStart, this));
      this.$stage.on('touchcancel.owl.core', $.proxy(this.onDragEnd, this));
    }
  };


  Owl.prototype.onDragStart = function (event) {
    var stage = null;

    if (event.which === 3) {
      return;
    }

    if ($.support.transform) {
      stage = this.$stage.css('transform').replace(/.*\(|\)| /g, '').split(',');
      stage = {
        x: stage[stage.length === 16 ? 12 : 4],
        y: stage[stage.length === 16 ? 13 : 5]
      };
    } else {
      stage = this.$stage.position();
      stage = {
        x: this.settings.rtl ? stage.left + this.$stage.width() - this.width() + this.settings.margin : stage.left,
        y: stage.top
      };
    }

    if (this.is('animating')) {
      $.support.transform ? this.animate(stage.x) : this.$stage.stop();
      this.invalidate('position');
    }

    this.$element.toggleClass(this.options.grabClass, event.type === 'mousedown');
    this.speed(0);
    this._drag.time = new Date().getTime();
    this._drag.target = $(event.target);
    this._drag.stage.start = stage;
    this._drag.stage.current = stage;
    this._drag.pointer = this.pointer(event);
    $(document).on('mouseup.owl.core touchend.owl.core', $.proxy(this.onDragEnd, this));
    $(document).one('mousemove.owl.core touchmove.owl.core', $.proxy(function (event) {
      var delta = this.difference(this._drag.pointer, this.pointer(event));
      $(document).on('mousemove.owl.core touchmove.owl.core', $.proxy(this.onDragMove, this));

      if (Math.abs(delta.x) < Math.abs(delta.y) && this.is('valid')) {
        return;
      }

      event.preventDefault();
      this.enter('dragging');
      this.trigger('drag');
    }, this));
  };


  Owl.prototype.onDragMove = function (event) {
    var minimum = null,
        maximum = null,
        pull = null,
        delta = this.difference(this._drag.pointer, this.pointer(event)),
        stage = this.difference(this._drag.stage.start, delta);

    if (!this.is('dragging')) {
      return;
    }

    event.preventDefault();

    if (this.settings.loop) {
      minimum = this.coordinates(this.minimum());
      maximum = this.coordinates(this.maximum() + 1) - minimum;
      stage.x = ((stage.x - minimum) % maximum + maximum) % maximum + minimum;
    } else {
      minimum = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum());
      maximum = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum());
      pull = this.settings.pullDrag ? -1 * delta.x / 5 : 0;
      stage.x = Math.max(Math.min(stage.x, minimum + pull), maximum + pull);
    }

    this._drag.stage.current = stage;
    this.animate(stage.x);
  };


  Owl.prototype.onDragEnd = function (event) {
    var delta = this.difference(this._drag.pointer, this.pointer(event)),
        stage = this._drag.stage.current,
        direction = delta.x > 0 ^ this.settings.rtl ? 'left' : 'right';
    $(document).off('.owl.core');
    this.$element.removeClass(this.options.grabClass);

    if (delta.x !== 0 && this.is('dragging') || !this.is('valid')) {
      this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed);
      this.current(this.closest(stage.x, delta.x !== 0 ? direction : this._drag.direction));
      this.invalidate('position');
      this.update();
      this._drag.direction = direction;

      if (Math.abs(delta.x) > 3 || new Date().getTime() - this._drag.time > 300) {
        this._drag.target.one('click.owl.core', function () {
          return false;
        });
      }
    }

    if (!this.is('dragging')) {
      return;
    }

    this.leave('dragging');
    this.trigger('dragged');
  };


  Owl.prototype.closest = function (coordinate, direction) {
    var position = -1,
        pull = 30,
        width = this.width(),
        coordinates = this.coordinates();

    if (!this.settings.freeDrag) {
      $.each(coordinates, $.proxy(function (index, value) {
        if (direction === 'left' && coordinate > value - pull && coordinate < value + pull) {
          position = index; 
        } else if (direction === 'right' && coordinate > value - width - pull && coordinate < value - width + pull) {
          position = index + 1;
        } else if (this.op(coordinate, '<', value) && this.op(coordinate, '>', coordinates[index + 1] !== undefined ? coordinates[index + 1] : value - width)) {
          position = direction === 'left' ? index + 1 : index;
        }

        return position === -1;
      }, this));
    }

    if (!this.settings.loop) {
      if (this.op(coordinate, '>', coordinates[this.minimum()])) {
        position = coordinate = this.minimum();
      } else if (this.op(coordinate, '<', coordinates[this.maximum()])) {
        position = coordinate = this.maximum();
      }
    }

    return position;
  };


  Owl.prototype.animate = function (coordinate) {
    var animate = this.speed() > 0;
    this.is('animating') && this.onTransitionEnd();

    if (animate) {
      this.enter('animating');
      this.trigger('translate');
    }

    if ($.support.transform3d && $.support.transition) {
      this.$stage.css({
        transform: 'translate3d(' + coordinate + 'px,0px,0px)',
        transition: this.speed() / 1000 + 's'
      });
    } else if (animate) {
      this.$stage.animate({
        left: coordinate + 'px'
      }, this.speed(), this.settings.fallbackEasing, $.proxy(this.onTransitionEnd, this));
    } else {
      this.$stage.css({
        left: coordinate + 'px'
      });
    }
  };


  Owl.prototype.is = function (state) {
    return this._states.current[state] && this._states.current[state] > 0;
  };


  Owl.prototype.current = function (position) {
    if (position === undefined) {
      return this._current;
    }

    if (this._items.length === 0) {
      return undefined;
    }

    position = this.normalize(position);

    if (this._current !== position) {
      var event = this.trigger('change', {
        property: {
          name: 'position',
          value: position
        }
      });

      if (event.data !== undefined) {
        position = this.normalize(event.data);
      }

      this._current = position;
      this.invalidate('position');
      this.trigger('changed', {
        property: {
          name: 'position',
          value: this._current
        }
      });
    }

    return this._current;
  };


  Owl.prototype.invalidate = function (part) {
    if ($.type(part) === 'string') {
      this._invalidated[part] = true;
      this.is('valid') && this.leave('valid');
    }

    return $.map(this._invalidated, function (v, i) {
      return i;
    });
  };


  Owl.prototype.reset = function (position) {
    position = this.normalize(position);

    if (position === undefined) {
      return;
    }

    this._speed = 0;
    this._current = position;
    this.suppress(['translate', 'translated']);
    this.animate(this.coordinates(position));
    this.release(['translate', 'translated']);
  };


  Owl.prototype.normalize = function (position, relative) {
    var n = this._items.length,
        m = relative ? 0 : this._clones.length;

    if (!this.isNumeric(position) || n < 1) {
      position = undefined;
    } else if (position < 0 || position >= n + m) {
      position = ((position - m / 2) % n + n) % n + m / 2;
    }

    return position;
  };


  Owl.prototype.relative = function (position) {
    position -= this._clones.length / 2;
    return this.normalize(position, true);
  };


  Owl.prototype.maximum = function (relative) {
    var settings = this.settings,
        maximum = this._coordinates.length,
        iterator,
        reciprocalItemsWidth,
        elementWidth;

    if (settings.loop) {
      maximum = this._clones.length / 2 + this._items.length - 1;
    } else if (settings.autoWidth || settings.merge) {
      iterator = this._items.length;

      if (iterator) {
        reciprocalItemsWidth = this._items[--iterator].width();
        elementWidth = this.$element.width();

        while (iterator--) {
          reciprocalItemsWidth += this._items[iterator].width() + this.settings.margin;

          if (reciprocalItemsWidth > elementWidth) {
            break;
          }
        }
      }

      maximum = iterator + 1;
    } else if (settings.center) {
      maximum = this._items.length - 1;
    } else {
      maximum = this._items.length - settings.items;
    }

    if (relative) {
      maximum -= this._clones.length / 2;
    }

    return Math.max(maximum, 0);
  };


  Owl.prototype.minimum = function (relative) {
    return relative ? 0 : this._clones.length / 2;
  };


  Owl.prototype.items = function (position) {
    if (position === undefined) {
      return this._items.slice();
    }

    position = this.normalize(position, true);
    return this._items[position];
  };


  Owl.prototype.mergers = function (position) {
    if (position === undefined) {
      return this._mergers.slice();
    }

    position = this.normalize(position, true);
    return this._mergers[position];
  };


  Owl.prototype.clones = function (position) {
    var odd = this._clones.length / 2,
        even = odd + this._items.length,
        map = function (index) {
      return index % 2 === 0 ? even + index / 2 : odd - (index + 1) / 2;
    };

    if (position === undefined) {
      return $.map(this._clones, function (v, i) {
        return map(i);
      });
    }

    return $.map(this._clones, function (v, i) {
      return v === position ? map(i) : null;
    });
  };


  Owl.prototype.speed = function (speed) {
    if (speed !== undefined) {
      this._speed = speed;
    }

    return this._speed;
  };


  Owl.prototype.coordinates = function (position) {
    var multiplier = 1,
        newPosition = position - 1,
        coordinate;

    if (position === undefined) {
      return $.map(this._coordinates, $.proxy(function (coordinate, index) {
        return this.coordinates(index);
      }, this));
    }

    if (this.settings.center) {
      if (this.settings.rtl) {
        multiplier = -1;
        newPosition = position + 1;
      }

      coordinate = this._coordinates[position];
      coordinate += (this.width() - coordinate + (this._coordinates[newPosition] || 0)) / 2 * multiplier;
    } else {
      coordinate = this._coordinates[newPosition] || 0;
    }

    coordinate = Math.ceil(coordinate);
    return coordinate;
  };


  Owl.prototype.duration = function (from, to, factor) {
    if (factor === 0) {
      return 0;
    }

    return Math.min(Math.max(Math.abs(to - from), 1), 6) * Math.abs(factor || this.settings.smartSpeed);
  };


  Owl.prototype.to = function (position, speed) {
    var current = this.current(),
        revert = null,
        distance = position - this.relative(current),
        direction = (distance > 0) - (distance < 0),
        items = this._items.length,
        minimum = this.minimum(),
        maximum = this.maximum();

    if (this.settings.loop) {
      if (!this.settings.rewind && Math.abs(distance) > items / 2) {
        distance += direction * -1 * items;
      }

      position = current + distance;
      revert = ((position - minimum) % items + items) % items + minimum;

      if (revert !== position && revert - distance <= maximum && revert - distance > 0) {
        current = revert - distance;
        position = revert;
        this.reset(current);
      }
    } else if (this.settings.rewind) {
      maximum += 1;
      position = (position % maximum + maximum) % maximum;
    } else {
      position = Math.max(minimum, Math.min(maximum, position));
    }

    this.speed(this.duration(current, position, speed));
    this.current(position);

    if (this.isVisible()) {
      this.update();
    }
  };


  Owl.prototype.next = function (speed) {
    speed = speed || false;
    this.to(this.relative(this.current()) + 1, speed);
  };


  Owl.prototype.prev = function (speed) {
    speed = speed || false;
    this.to(this.relative(this.current()) - 1, speed);
  };


  Owl.prototype.onTransitionEnd = function (event) {
    if (event !== undefined) {
      event.stopPropagation(); 

      if ((event.target || event.srcElement || event.originalTarget) !== this.$stage.get(0)) {
        return false;
      }
    }

    this.leave('animating');
    this.trigger('translated');
  };


  Owl.prototype.viewport = function () {
    var width;

    if (this.options.responsiveBaseElement !== window) {
      width = $(this.options.responsiveBaseElement).width();
    } else if (window.innerWidth) {
      width = window.innerWidth;
    } else if (document.documentElement && document.documentElement.clientWidth) {
      width = document.documentElement.clientWidth;
    } else {
      console.warn('Can not detect viewport width.');
    }

    return width;
  };


  Owl.prototype.replace = function (content) {
    this.$stage.empty();
    this._items = [];

    if (content) {
      content = content instanceof jQuery ? content : $(content);
    }

    if (this.settings.nestedItemSelector) {
      content = content.find('.' + this.settings.nestedItemSelector);
    }

    content.filter(function () {
      return this.nodeType === 1;
    }).each($.proxy(function (index, item) {
      item = this.prepare(item);
      this.$stage.append(item);

      this._items.push(item);

      this._mergers.push(item.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
    }, this));
    this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0);
    this.invalidate('items');
  };


  Owl.prototype.add = function (content, position) {
    var current = this.relative(this._current);
    position = position === undefined ? this._items.length : this.normalize(position, true);
    content = content instanceof jQuery ? content : $(content);
    this.trigger('add', {
      content: content,
      position: position
    });
    content = this.prepare(content);

    if (this._items.length === 0 || position === this._items.length) {
      this._items.length === 0 && this.$stage.append(content);
      this._items.length !== 0 && this._items[position - 1].after(content);

      this._items.push(content);

      this._mergers.push(content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
    } else {
      this._items[position].before(content);

      this._items.splice(position, 0, content);

      this._mergers.splice(position, 0, content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
    }

    this._items[current] && this.reset(this._items[current].index());
    this.invalidate('items');
    this.trigger('added', {
      content: content,
      position: position
    });
  };


  Owl.prototype.remove = function (position) {
    position = this.normalize(position, true);

    if (position === undefined) {
      return;
    }

    this.trigger('remove', {
      content: this._items[position],
      position: position
    });

    this._items[position].remove();

    this._items.splice(position, 1);

    this._mergers.splice(position, 1);

    this.invalidate('items');
    this.trigger('removed', {
      content: null,
      position: position
    });
  };


  Owl.prototype.preloadAutoWidthImages = function (images) {
    images.each($.proxy(function (i, element) {
      this.enter('pre-loading');
      element = $(element);
      $(new Image()).one('load', $.proxy(function (e) {
        element.attr('src', e.target.src);
        element.css('opacity', 1);
        this.leave('pre-loading');
        !this.is('pre-loading') && !this.is('initializing') && this.refresh();
      }, this)).attr('src', element.attr('src') || element.attr('data-src') || element.attr('data-src-retina'));
    }, this));
  };


  Owl.prototype.destroy = function () {
    this.$element.off('.owl.core');
    this.$stage.off('.owl.core');
    $(document).off('.owl.core');

    if (this.settings.responsive !== false) {
      window.clearTimeout(this.resizeTimer);
      this.off(window, 'resize', this._handlers.onThrottledResize);
    }

    for (var i in this._plugins) {
      this._plugins[i].destroy();
    }

    this.$stage.children('.cloned').remove();
    this.$stage.unwrap();
    this.$stage.children().contents().unwrap();
    this.$stage.children().unwrap();
    this.$stage.remove();
    this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr('class', this.$element.attr('class').replace(new RegExp(this.options.responsiveClass + '-\\S+\\s', 'g'), '')).removeData('owl.carousel');
  };


  Owl.prototype.op = function (a, o, b) {
    var rtl = this.settings.rtl;

    switch (o) {
      case '<':
        return rtl ? a > b : a < b;

      case '>':
        return rtl ? a < b : a > b;

      case '>=':
        return rtl ? a <= b : a >= b;

      case '<=':
        return rtl ? a >= b : a <= b;

      default:
        break;
    }
  };


  Owl.prototype.on = function (element, event, listener, capture) {
    if (element.addEventListener) {
      element.addEventListener(event, listener, capture);
    } else if (element.attachEvent) {
      element.attachEvent('on' + event, listener);
    }
  };


  Owl.prototype.off = function (element, event, listener, capture) {
    if (element.removeEventListener) {
      element.removeEventListener(event, listener, capture);
    } else if (element.detachEvent) {
      element.detachEvent('on' + event, listener);
    }
  };


  Owl.prototype.trigger = function (name, data, namespace, state, enter) {
    var status = {
      item: {
        count: this._items.length,
        index: this.current()
      }
    },
        handler = $.camelCase($.grep(['on', name, namespace], function (v) {
      return v;
    }).join('-').toLowerCase()),
        event = $.Event([name, 'owl', namespace || 'carousel'].join('.').toLowerCase(), $.extend({
      relatedTarget: this
    }, status, data));

    if (!this._supress[name]) {
      $.each(this._plugins, function (name, plugin) {
        if (plugin.onTrigger) {
          plugin.onTrigger(event);
        }
      });
      this.register({
        type: Owl.Type.Event,
        name: name
      });
      this.$element.trigger(event);

      if (this.settings && typeof this.settings[handler] === 'function') {
        this.settings[handler].call(this, event);
      }
    }

    return event;
  };


  Owl.prototype.enter = function (name) {
    $.each([name].concat(this._states.tags[name] || []), $.proxy(function (i, name) {
      if (this._states.current[name] === undefined) {
        this._states.current[name] = 0;
      }

      this._states.current[name]++;
    }, this));
  };


  Owl.prototype.leave = function (name) {
    $.each([name].concat(this._states.tags[name] || []), $.proxy(function (i, name) {
      this._states.current[name]--;
    }, this));
  };


  Owl.prototype.register = function (object) {
    if (object.type === Owl.Type.Event) {
      if (!$.event.special[object.name]) {
        $.event.special[object.name] = {};
      }

      if (!$.event.special[object.name].owl) {
        var _default = $.event.special[object.name]._default;

        $.event.special[object.name]._default = function (e) {
          if (_default && _default.apply && (!e.namespace || e.namespace.indexOf('owl') === -1)) {
            return _default.apply(this, arguments);
          }

          return e.namespace && e.namespace.indexOf('owl') > -1;
        };

        $.event.special[object.name].owl = true;
      }
    } else if (object.type === Owl.Type.State) {
      if (!this._states.tags[object.name]) {
        this._states.tags[object.name] = object.tags;
      } else {
        this._states.tags[object.name] = this._states.tags[object.name].concat(object.tags);
      }

      this._states.tags[object.name] = $.grep(this._states.tags[object.name], $.proxy(function (tag, i) {
        return $.inArray(tag, this._states.tags[object.name]) === i;
      }, this));
    }
  };


  Owl.prototype.suppress = function (events) {
    $.each(events, $.proxy(function (index, event) {
      this._supress[event] = true;
    }, this));
  };


  Owl.prototype.release = function (events) {
    $.each(events, $.proxy(function (index, event) {
      delete this._supress[event];
    }, this));
  };


  Owl.prototype.pointer = function (event) {
    var result = {
      x: null,
      y: null
    };
    event = event.originalEvent || event || window.event;
    event = event.touches && event.touches.length ? event.touches[0] : event.changedTouches && event.changedTouches.length ? event.changedTouches[0] : event;

    if (event.pageX) {
      result.x = event.pageX;
      result.y = event.pageY;
    } else {
      result.x = event.clientX;
      result.y = event.clientY;
    }

    return result;
  };


  Owl.prototype.isNumeric = function (number) {
    return !isNaN(parseFloat(number));
  };


  Owl.prototype.difference = function (first, second) {
    return {
      x: first.x - second.x,
      y: first.y - second.y
    };
  };


  $.fn.owlCarousel = function (option) {
    var args = Array.prototype.slice.call(arguments, 1);
    return this.each(function () {
      var $this = $(this),
          data = $this.data('owl.carousel');

      if (!data) {
        data = new Owl(this, typeof option == 'object' && option);
        $this.data('owl.carousel', data);
        $.each(['next', 'prev', 'to', 'destroy', 'refresh', 'replace', 'add', 'remove'], function (i, event) {
          data.register({
            type: Owl.Type.Event,
            name: event
          });
          data.$element.on(event + '.owl.carousel.core', $.proxy(function (e) {
            if (e.namespace && e.relatedTarget !== this) {
              this.suppress([event]);
              data[event].apply(this, [].slice.call(arguments, 1));
              this.release([event]);
            }
          }, data));
        });
      }

      if (typeof option == 'string' && option.charAt(0) !== '_') {
        data[option].apply(data, args);
      }
    });
  };


  $.fn.owlCarousel.Constructor = Owl;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var AutoRefresh = function (carousel) {
    this._core = carousel;

    this._interval = null;

    this._visible = null;

    this._handlers = {
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoRefresh) {
          this.watch();
        }
      }, this)
    }; 

    this._core.options = $.extend({}, AutoRefresh.Defaults, this._core.options); 

    this._core.$element.on(this._handlers);
  };


  AutoRefresh.Defaults = {
    autoRefresh: true,
    autoRefreshInterval: 500
  };

  AutoRefresh.prototype.watch = function () {
    if (this._interval) {
      return;
    }

    this._visible = this._core.isVisible();
    this._interval = window.setInterval($.proxy(this.refresh, this), this._core.settings.autoRefreshInterval);
  };


  AutoRefresh.prototype.refresh = function () {
    if (this._core.isVisible() === this._visible) {
      return;
    }

    this._visible = !this._visible;

    this._core.$element.toggleClass('owl-hidden', !this._visible);

    this._visible && this._core.invalidate('width') && this._core.refresh();
  };


  AutoRefresh.prototype.destroy = function () {
    var handler, property;
    window.clearInterval(this._interval);

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.AutoRefresh = AutoRefresh;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var Lazy = function (carousel) {
    this._core = carousel;

    this._loaded = [];

    this._handlers = {
      'initialized.owl.carousel change.owl.carousel resized.owl.carousel': $.proxy(function (e) {
        if (!e.namespace) {
          return;
        }

        if (!this._core.settings || !this._core.settings.lazyLoad) {
          return;
        }

        if (e.property && e.property.name == 'position' || e.type == 'initialized') {
          var settings = this._core.settings,
              n = settings.center && Math.ceil(settings.items / 2) || settings.items,
              i = settings.center && n * -1 || 0,
              position = (e.property && e.property.value !== undefined ? e.property.value : this._core.current()) + i,
              clones = this._core.clones().length,
              load = $.proxy(function (i, v) {
            this.load(v);
          }, this);

          while (i++ < n) {
            this.load(clones / 2 + this._core.relative(position));
            clones && $.each(this._core.clones(this._core.relative(position)), load);
            position++;
          }
        }
      }, this)
    }; 

    this._core.options = $.extend({}, Lazy.Defaults, this._core.options); 

    this._core.$element.on(this._handlers);
  };


  Lazy.Defaults = {
    lazyLoad: false
  };

  Lazy.prototype.load = function (position) {
    var $item = this._core.$stage.children().eq(position),
        $elements = $item && $item.find('.owl-lazy');

    if (!$elements || $.inArray($item.get(0), this._loaded) > -1) {
      return;
    }

    $elements.each($.proxy(function (index, element) {
      var $element = $(element),
          image,
          url = window.devicePixelRatio > 1 && $element.attr('data-src-retina') || $element.attr('data-src') || $element.attr('data-srcset');

      this._core.trigger('load', {
        element: $element,
        url: url
      }, 'lazy');

      if ($element.is('img')) {
        $element.one('load.owl.lazy', $.proxy(function () {
          $element.css('opacity', 1);

          this._core.trigger('loaded', {
            element: $element,
            url: url
          }, 'lazy');
        }, this)).attr('src', url);
      } else if ($element.is('source')) {
        $element.one('load.owl.lazy', $.proxy(function () {
          this._core.trigger('loaded', {
            element: $element,
            url: url
          }, 'lazy');
        }, this)).attr('srcset', url);
      } else {
        image = new Image();
        image.onload = $.proxy(function () {
          $element.css({
            'background-image': 'url("' + url + '")',
            'opacity': '1'
          });

          this._core.trigger('loaded', {
            element: $element,
            url: url
          }, 'lazy');
        }, this);
        image.src = url;
      }
    }, this));

    this._loaded.push($item.get(0));
  };


  Lazy.prototype.destroy = function () {
    var handler, property;

    for (handler in this.handlers) {
      this._core.$element.off(handler, this.handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Lazy = Lazy;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var AutoHeight = function (carousel) {
    this._core = carousel;

    this._handlers = {
      'initialized.owl.carousel refreshed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoHeight) {
          this.update();
        }
      }, this),
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoHeight && e.property.name === 'position') {
          console.log('update called');
          this.update();
        }
      }, this),
      'loaded.owl.lazy': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoHeight && e.element.closest('.' + this._core.settings.itemClass).index() === this._core.current()) {
          this.update();
        }
      }, this)
    }; 

    this._core.options = $.extend({}, AutoHeight.Defaults, this._core.options); 

    this._core.$element.on(this._handlers);

    this._intervalId = null;
    var refThis = this; 

    $(window).on('load', function () {
      if (refThis._core.settings.autoHeight) {
        refThis.update();
      }
    }); 

    $(window).resize(function () {
      if (refThis._core.settings.autoHeight) {
        if (refThis._intervalId != null) {
          clearTimeout(refThis._intervalId);
        }

        refThis._intervalId = setTimeout(function () {
          refThis.update();
        }, 250);
      }
    });
  };


  AutoHeight.Defaults = {
    autoHeight: false,
    autoHeightClass: 'owl-height'
  };

  AutoHeight.prototype.update = function () {
    var start = this._core._current,
        end = start + this._core.settings.items,
        visible = this._core.$stage.children().toArray().slice(start, end),
        heights = [],
        maxheight = 0;

    $.each(visible, function (index, item) {
      heights.push($(item).height());
    });
    maxheight = Math.max.apply(null, heights);

    this._core.$stage.parent().height(maxheight).addClass(this._core.settings.autoHeightClass);
  };

  AutoHeight.prototype.destroy = function () {
    var handler, property;

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] !== 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.AutoHeight = AutoHeight;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var Video = function (carousel) {
    this._core = carousel;

    this._videos = {};

    this._playing = null;

    this._handlers = {
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace) {
          this._core.register({
            type: 'state',
            name: 'playing',
            tags: ['interacting']
          });
        }
      }, this),
      'resize.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.video && this.isInFullScreen()) {
          e.preventDefault();
        }
      }, this),
      'refreshed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.is('resizing')) {
          this._core.$stage.find('.cloned .owl-video-frame').remove();
        }
      }, this),
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name === 'position' && this._playing) {
          this.stop();
        }
      }, this),
      'prepared.owl.carousel': $.proxy(function (e) {
        if (!e.namespace) {
          return;
        }

        var $element = $(e.content).find('.owl-video');

        if ($element.length) {
          $element.css('display', 'none');
          this.fetch($element, $(e.content));
        }
      }, this)
    }; 

    this._core.options = $.extend({}, Video.Defaults, this._core.options); 

    this._core.$element.on(this._handlers);

    this._core.$element.on('click.owl.video', '.owl-video-play-icon', $.proxy(function (e) {
      this.play(e);
    }, this));
  };


  Video.Defaults = {
    video: false,
    videoHeight: false,
    videoWidth: false
  };

  Video.prototype.fetch = function (target, item) {
    var type = function () {
      if (target.attr('data-vimeo-id')) {
        return 'vimeo';
      } else if (target.attr('data-vzaar-id')) {
        return 'vzaar';
      } else {
        return 'youtube';
      }
    }(),
        id = target.attr('data-vimeo-id') || target.attr('data-youtube-id') || target.attr('data-vzaar-id'),
        width = target.attr('data-width') || this._core.settings.videoWidth,
        height = target.attr('data-height') || this._core.settings.videoHeight,
        url = target.attr('href');

    if (url) {
      id = url.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

      if (id[3].indexOf('youtu') > -1) {
        type = 'youtube';
      } else if (id[3].indexOf('vimeo') > -1) {
        type = 'vimeo';
      } else if (id[3].indexOf('vzaar') > -1) {
        type = 'vzaar';
      } else {
        throw new Error('Video URL not supported.');
      }

      id = id[6];
    } else {
      throw new Error('Missing video URL.');
    }

    this._videos[url] = {
      type: type,
      id: id,
      width: width,
      height: height
    };
    item.attr('data-video', url);
    this.thumbnail(target, this._videos[url]);
  };


  Video.prototype.thumbnail = function (target, video) {
    var tnLink,
        icon,
        path,
        dimensions = video.width && video.height ? 'style="width:' + video.width + 'px;height:' + video.height + 'px;"' : '',
        customTn = target.find('img'),
        srcType = 'src',
        lazyClass = '',
        settings = this._core.settings,
        create = function (path) {
      icon = '<div class="owl-video-play-icon"></div>';

      if (settings.lazyLoad) {
        tnLink = '<div class="owl-video-tn ' + lazyClass + '" ' + srcType + '="' + path + '"></div>';
      } else {
        tnLink = '<div class="owl-video-tn" style="opacity:1;background-image:url(' + path + ')"></div>';
      }

      target.after(tnLink);
      target.after(icon);
    }; 


    target.wrap('<div class="owl-video-wrapper"' + dimensions + '></div>');

    if (this._core.settings.lazyLoad) {
      srcType = 'data-src';
      lazyClass = 'owl-lazy';
    } 


    if (customTn.length) {
      create(customTn.attr(srcType));
      customTn.remove();
      return false;
    }

    if (video.type === 'youtube') {
      path = "//img.youtube.com/vi/" + video.id + "/hqdefault.jpg";
      create(path);
    } else if (video.type === 'vimeo') {
      $.ajax({
        type: 'GET',
        url: '//vimeo.com/api/v2/video/' + video.id + '.json',
        jsonp: 'callback',
        dataType: 'jsonp',
        success: function (data) {
          path = data[0].thumbnail_large;
          create(path);
        }
      });
    } else if (video.type === 'vzaar') {
      $.ajax({
        type: 'GET',
        url: '//vzaar.com/api/videos/' + video.id + '.json',
        jsonp: 'callback',
        dataType: 'jsonp',
        success: function (data) {
          path = data.framegrab_url;
          create(path);
        }
      });
    }
  };


  Video.prototype.stop = function () {
    this._core.trigger('stop', null, 'video');

    this._playing.find('.owl-video-frame').remove();

    this._playing.removeClass('owl-video-playing');

    this._playing = null;

    this._core.leave('playing');

    this._core.trigger('stopped', null, 'video');
  };


  Video.prototype.play = function (event) {
    var target = $(event.target),
        item = target.closest('.' + this._core.settings.itemClass),
        video = this._videos[item.attr('data-video')],
        width = video.width || '100%',
        height = video.height || this._core.$stage.height(),
        html;

    if (this._playing) {
      return;
    }

    this._core.enter('playing');

    this._core.trigger('play', null, 'video');

    item = this._core.items(this._core.relative(item.index()));

    this._core.reset(item.index());

    if (video.type === 'youtube') {
      html = '<iframe width="' + width + '" height="' + height + '" src="//www.youtube.com/embed/' + video.id + '?autoplay=1&rel=0&v=' + video.id + '" frameborder="0" allowfullscreen></iframe>';
    } else if (video.type === 'vimeo') {
      html = '<iframe src="//player.vimeo.com/video/' + video.id + '?autoplay=1" width="' + width + '" height="' + height + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    } else if (video.type === 'vzaar') {
      html = '<iframe frameborder="0"' + 'height="' + height + '"' + 'width="' + width + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen ' + 'src="//view.vzaar.com/' + video.id + '/player?autoplay=true"></iframe>';
    }

    $('<div class="owl-video-frame">' + html + '</div>').insertAfter(item.find('.owl-video'));
    this._playing = item.addClass('owl-video-playing');
  };


  Video.prototype.isInFullScreen = function () {
    var element = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
    return element && $(element).parent().hasClass('owl-video-frame');
  };


  Video.prototype.destroy = function () {
    var handler, property;

    this._core.$element.off('click.owl.video');

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Video = Video;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var Animate = function (scope) {
    this.core = scope;
    this.core.options = $.extend({}, Animate.Defaults, this.core.options);
    this.swapping = true;
    this.previous = undefined;
    this.next = undefined;
    this.handlers = {
      'change.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name == 'position') {
          this.previous = this.core.current();
          this.next = e.property.value;
        }
      }, this),
      'drag.owl.carousel dragged.owl.carousel translated.owl.carousel': $.proxy(function (e) {
        if (e.namespace) {
          this.swapping = e.type == 'translated';
        }
      }, this),
      'translate.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn)) {
          this.swap();
        }
      }, this)
    };
    this.core.$element.on(this.handlers);
  };


  Animate.Defaults = {
    animateOut: false,
    animateIn: false
  };

  Animate.prototype.swap = function () {
    if (this.core.settings.items !== 1) {
      return;
    }

    if (!$.support.animation || !$.support.transition) {
      return;
    }

    this.core.speed(0);
    var left,
        clear = $.proxy(this.clear, this),
        previous = this.core.$stage.children().eq(this.previous),
        next = this.core.$stage.children().eq(this.next),
        incoming = this.core.settings.animateIn,
        outgoing = this.core.settings.animateOut;

    if (this.core.current() === this.previous) {
      return;
    }

    if (outgoing) {
      left = this.core.coordinates(this.previous) - this.core.coordinates(this.next);
      previous.one($.support.animation.end, clear).css({
        'left': left + 'px'
      }).addClass('animated owl-animated-out').addClass(outgoing);
    }

    if (incoming) {
      next.one($.support.animation.end, clear).addClass('animated owl-animated-in').addClass(incoming);
    }
  };

  Animate.prototype.clear = function (e) {
    $(e.target).css({
      'left': ''
    }).removeClass('animated owl-animated-out owl-animated-in').removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut);
    this.core.onTransitionEnd();
  };


  Animate.prototype.destroy = function () {
    var handler, property;

    for (handler in this.handlers) {
      this.core.$element.off(handler, this.handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Animate = Animate;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var Autoplay = function (carousel) {
    this._core = carousel;

    this._call = null;

    this._time = 0;

    this._timeout = 0;

    this._paused = true;

    this._handlers = {
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name === 'settings') {
          if (this._core.settings.autoplay) {
            this.play();
          } else {
            this.stop();
          }
        } else if (e.namespace && e.property.name === 'position' && this._paused) {
          this._time = 0;
        }
      }, this),
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoplay) {
          this.play();
        }
      }, this),
      'play.owl.autoplay': $.proxy(function (e, t, s) {
        if (e.namespace) {
          this.play(t, s);
        }
      }, this),
      'stop.owl.autoplay': $.proxy(function (e) {
        if (e.namespace) {
          this.stop();
        }
      }, this),
      'mouseover.owl.autoplay': $.proxy(function () {
        if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
          this.pause();
        }
      }, this),
      'mouseleave.owl.autoplay': $.proxy(function () {
        if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
          this.play();
        }
      }, this),
      'touchstart.owl.core': $.proxy(function () {
        if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
          this.pause();
        }
      }, this),
      'touchend.owl.core': $.proxy(function () {
        if (this._core.settings.autoplayHoverPause) {
          this.play();
        }
      }, this)
    }; 

    this._core.$element.on(this._handlers); 


    this._core.options = $.extend({}, Autoplay.Defaults, this._core.options);
  };


  Autoplay.Defaults = {
    autoplay: false,
    autoplayTimeout: 5000,
    autoplayHoverPause: false,
    autoplaySpeed: false
  };

  Autoplay.prototype._next = function (speed) {
    this._call = window.setTimeout($.proxy(this._next, this, speed), this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read());

    if (this._core.is('busy') || this._core.is('interacting') || document.hidden) {
      return;
    }

    this._core.next(speed || this._core.settings.autoplaySpeed);
  };


  Autoplay.prototype.read = function () {
    return new Date().getTime() - this._time;
  };


  Autoplay.prototype.play = function (timeout, speed) {
    var elapsed;

    if (!this._core.is('rotating')) {
      this._core.enter('rotating');
    }

    timeout = timeout || this._core.settings.autoplayTimeout; 

    elapsed = Math.min(this._time % (this._timeout || timeout), timeout);

    if (this._paused) {
      this._time = this.read();
      this._paused = false;
    } else {
      window.clearTimeout(this._call);
    } 


    this._time += this.read() % timeout - elapsed;
    this._timeout = timeout;
    this._call = window.setTimeout($.proxy(this._next, this, speed), timeout - elapsed);
  };


  Autoplay.prototype.stop = function () {
    if (this._core.is('rotating')) {
      this._time = 0;
      this._paused = true;
      window.clearTimeout(this._call);

      this._core.leave('rotating');
    }
  };


  Autoplay.prototype.pause = function () {
    if (this._core.is('rotating') && !this._paused) {
      this._time = this.read();
      this._paused = true;
      window.clearTimeout(this._call);
    }
  };


  Autoplay.prototype.destroy = function () {
    var handler, property;
    this.stop();

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.autoplay = Autoplay;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  'use strict';

  var Navigation = function (carousel) {
    this._core = carousel;

    this._initialized = false;

    this._pages = [];

    this._controls = {};

    this._templates = [];

    this.$element = this._core.$element;

    this._overrides = {
      next: this._core.next,
      prev: this._core.prev,
      to: this._core.to
    };

    this._handlers = {
      'prepared.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.dotsData) {
          this._templates.push('<div class="' + this._core.settings.dotClass + '">' + $(e.content).find('[data-dot]').addBack('[data-dot]').attr('data-dot') + '</div>');
        }
      }, this),
      'added.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.dotsData) {
          this._templates.splice(e.position, 0, this._templates.pop());
        }
      }, this),
      'remove.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.dotsData) {
          this._templates.splice(e.position, 1);
        }
      }, this),
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name == 'position') {
          this.draw();
        }
      }, this),
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace && !this._initialized) {
          this._core.trigger('initialize', null, 'navigation');

          this.initialize();
          this.update();
          this.draw();
          this._initialized = true;

          this._core.trigger('initialized', null, 'navigation');
        }
      }, this),
      'refreshed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._initialized) {
          this._core.trigger('refresh', null, 'navigation');

          this.update();
          this.draw();

          this._core.trigger('refreshed', null, 'navigation');
        }
      }, this)
    }; 

    this._core.options = $.extend({}, Navigation.Defaults, this._core.options); 

    this.$element.on(this._handlers);
  };


  Navigation.Defaults = {
    nav: false,
    navText: ['<span aria-label="' + 'Previous' + '">&#x2039;</span>', '<span aria-label="' + 'Next' + '">&#x203a;</span>'],
    navSpeed: false,
    navElement: 'button type="button" role="presentation"',
    navContainer: false,
    navContainerClass: 'owl-nav',
    navClass: ['owl-prev', 'owl-next'],
    slideBy: 1,
    dotClass: 'owl-dot',
    dotsClass: 'owl-dots',
    dots: true,
    dotsEach: false,
    dotsData: false,
    dotsSpeed: false,
    dotsContainer: false
  };

  Navigation.prototype.initialize = function () {
    var override,
        settings = this._core.settings; 

    this._controls.$relative = (settings.navContainer ? $(settings.navContainer) : $('<div>').addClass(settings.navContainerClass).appendTo(this.$element)).addClass('disabled');
    this._controls.$previous = $('<' + settings.navElement + '>').addClass(settings.navClass[0]).html(settings.navText[0]).prependTo(this._controls.$relative).on('click', $.proxy(function (e) {
      this.prev(settings.navSpeed);
    }, this));
    this._controls.$next = $('<' + settings.navElement + '>').addClass(settings.navClass[1]).html(settings.navText[1]).appendTo(this._controls.$relative).on('click', $.proxy(function (e) {
      this.next(settings.navSpeed);
    }, this)); 

    if (!settings.dotsData) {
      this._templates = [$('<button role="button">').addClass(settings.dotClass).append($('<span>')).prop('outerHTML')];
    }

    this._controls.$absolute = (settings.dotsContainer ? $(settings.dotsContainer) : $('<div>').addClass(settings.dotsClass).appendTo(this.$element)).addClass('disabled');

    this._controls.$absolute.on('click', 'button', $.proxy(function (e) {
      var index = $(e.target).parent().is(this._controls.$absolute) ? $(e.target).index() : $(e.target).parent().index();
      e.preventDefault();
      this.to(index, settings.dotsSpeed);
    }, this));


    for (override in this._overrides) {
      this._core[override] = $.proxy(this[override], this);
    }
  };


  Navigation.prototype.destroy = function () {
    var handler, control, property, override, settings;
    settings = this._core.settings;

    for (handler in this._handlers) {
      this.$element.off(handler, this._handlers[handler]);
    }

    for (control in this._controls) {
      if (control === '$relative' && settings.navContainer) {
        this._controls[control].html('');
      } else {
        this._controls[control].remove();
      }
    }

    for (override in this.overides) {
      this._core[override] = this._overrides[override];
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };


  Navigation.prototype.update = function () {
    var i,
        j,
        k,
        lower = this._core.clones().length / 2,
        upper = lower + this._core.items().length,
        maximum = this._core.maximum(true),
        settings = this._core.settings,
        size = settings.center || settings.autoWidth || settings.dotsData ? 1 : settings.dotsEach || settings.items;

    if (settings.slideBy !== 'page') {
      settings.slideBy = Math.min(settings.slideBy, settings.items);
    }

    if (settings.dots || settings.slideBy == 'page') {
      this._pages = [];

      for (i = lower, j = 0, k = 0; i < upper; i++) {
        if (j >= size || j === 0) {
          this._pages.push({
            start: Math.min(maximum, i - lower),
            end: i - lower + size - 1
          });

          if (Math.min(maximum, i - lower) === maximum) {
            break;
          }

          j = 0, ++k;
        }

        j += this._core.mergers(this._core.relative(i));
      }
    }
  };


  Navigation.prototype.draw = function () {
    var difference,
        settings = this._core.settings,
        disabled = this._core.items().length <= settings.items,
        index = this._core.relative(this._core.current()),
        loop = settings.loop || settings.rewind;

    this._controls.$relative.toggleClass('disabled', !settings.nav || disabled);

    if (settings.nav) {
      this._controls.$previous.toggleClass('disabled', !loop && index <= this._core.minimum(true));

      this._controls.$next.toggleClass('disabled', !loop && index >= this._core.maximum(true));
    }

    this._controls.$absolute.toggleClass('disabled', !settings.dots || disabled);

    if (settings.dots) {
      difference = this._pages.length - this._controls.$absolute.children().length;

      if (settings.dotsData && difference !== 0) {
        this._controls.$absolute.html(this._templates.join(''));
      } else if (difference > 0) {
        this._controls.$absolute.append(new Array(difference + 1).join(this._templates[0]));
      } else if (difference < 0) {
        this._controls.$absolute.children().slice(difference).remove();
      }

      this._controls.$absolute.find('.active').removeClass('active');

      this._controls.$absolute.children().eq($.inArray(this.current(), this._pages)).addClass('active');
    }
  };


  Navigation.prototype.onTrigger = function (event) {
    var settings = this._core.settings;
    event.page = {
      index: $.inArray(this.current(), this._pages),
      count: this._pages.length,
      size: settings && (settings.center || settings.autoWidth || settings.dotsData ? 1 : settings.dotsEach || settings.items)
    };
  };


  Navigation.prototype.current = function () {
    var current = this._core.relative(this._core.current());

    return $.grep(this._pages, $.proxy(function (page, index) {
      return page.start <= current && page.end >= current;
    }, this)).pop();
  };


  Navigation.prototype.getPosition = function (successor) {
    var position,
        length,
        settings = this._core.settings;

    if (settings.slideBy == 'page') {
      position = $.inArray(this.current(), this._pages);
      length = this._pages.length;
      successor ? ++position : --position;
      position = this._pages[(position % length + length) % length].start;
    } else {
      position = this._core.relative(this._core.current());
      length = this._core.items().length;
      successor ? position += settings.slideBy : position -= settings.slideBy;
    }

    return position;
  };


  Navigation.prototype.next = function (speed) {
    $.proxy(this._overrides.to, this._core)(this.getPosition(true), speed);
  };


  Navigation.prototype.prev = function (speed) {
    $.proxy(this._overrides.to, this._core)(this.getPosition(false), speed);
  };


  Navigation.prototype.to = function (position, speed, standard) {
    var length;

    if (!standard && this._pages.length) {
      length = this._pages.length;
      $.proxy(this._overrides.to, this._core)(this._pages[(position % length + length) % length].start, speed);
    } else {
      $.proxy(this._overrides.to, this._core)(position, speed);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Navigation = Navigation;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  'use strict';

  var Hash = function (carousel) {
    this._core = carousel;

    this._hashes = {};

    this.$element = this._core.$element;

    this._handlers = {
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.startPosition === 'URLHash') {
          $(window).trigger('hashchange.owl.navigation');
        }
      }, this),
      'prepared.owl.carousel': $.proxy(function (e) {
        if (e.namespace) {
          var hash = $(e.content).find('[data-hash]').addBack('[data-hash]').attr('data-hash');

          if (!hash) {
            return;
          }

          this._hashes[hash] = e.content;
        }
      }, this),
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name === 'position') {
          var current = this._core.items(this._core.relative(this._core.current())),
              hash = $.map(this._hashes, function (item, hash) {
            return item === current ? hash : null;
          }).join();

          if (!hash || window.location.hash.slice(1) === hash) {
            return;
          }

          window.location.hash = hash;
        }
      }, this)
    }; 

    this._core.options = $.extend({}, Hash.Defaults, this._core.options); 

    this.$element.on(this._handlers); 

    $(window).on('hashchange.owl.navigation', $.proxy(function (e) {
      var hash = window.location.hash.substring(1),
          items = this._core.$stage.children(),
          position = this._hashes[hash] && items.index(this._hashes[hash]);

      if (position === undefined || position === this._core.current()) {
        return;
      }

      this._core.to(this._core.relative(position), false, true);
    }, this));
  };


  Hash.Defaults = {
    URLhashListener: false
  };

  Hash.prototype.destroy = function () {
    var handler, property;
    $(window).off('hashchange.owl.navigation');

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Hash = Hash;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var style = $('<support>').get(0).style,
      prefixes = 'Webkit Moz O ms'.split(' '),
      events = {
    transition: {
      end: {
        WebkitTransition: 'webkitTransitionEnd',
        MozTransition: 'transitionend',
        OTransition: 'oTransitionEnd',
        transition: 'transitionend'
      }
    },
    animation: {
      end: {
        WebkitAnimation: 'webkitAnimationEnd',
        MozAnimation: 'animationend',
        OAnimation: 'oAnimationEnd',
        animation: 'animationend'
      }
    }
  },
      tests = {
    csstransforms: function () {
      return !!test('transform');
    },
    csstransforms3d: function () {
      return !!test('perspective');
    },
    csstransitions: function () {
      return !!test('transition');
    },
    cssanimations: function () {
      return !!test('animation');
    }
  };

  function test(property, prefixed) {
    var result = false,
        upper = property.charAt(0).toUpperCase() + property.slice(1);
    $.each((property + ' ' + prefixes.join(upper + ' ') + upper).split(' '), function (i, property) {
      if (style[property] !== undefined) {
        result = prefixed ? property : true;
        return false;
      }
    });
    return result;
  }

  function prefixed(property) {
    return test(property, true);
  }

  if (tests.csstransitions()) {
    $.support.transition = new String(prefixed('transition'));
    $.support.transition.end = events.transition.end[$.support.transition];
  }

  if (tests.cssanimations()) {
    $.support.animation = new String(prefixed('animation'));
    $.support.animation.end = events.animation.end[$.support.animation];
  }

  if (tests.csstransforms()) {
    $.support.transform = new String(prefixed('transform'));
    $.support.transform3d = tests.csstransforms3d();
  }
})(window.Zepto || window.jQuery, window, document);
document.addEventListener('wheel touchstart', evt => {
}, {
  capture: true,
  passive: true
});
var validatedStep = 0;
var cartCount = jQuery(".cart-count-number-span").html();
var miniCartState = 'closed';
let ogk_header_cart_form = jQuery('.woocommerce-cart-form');
let ogk_header_cart_form_initial_state = ogk_header_cart_form.serialize();
let variationSelectedProductPrice = 0; 

let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);
jQuery(document).ready(function ($) {
  routerOnRdy(); 

  $(".blog-slider").owlCarousel({
    items: 3,
    loop: true,
    autoplay: false,
    stagePadding: 100,
    margin: 20,
    autoplayTimeout: 3000,
    pagination: false,
    nav: true,
    rewindSpeed: 2000,
    smartSpeed: 2000,
    responsive: {
      0: {
        items: 1,
        stagePadding: 50
      },
      991: {
        items: 2,
        stagePadding: 100
      },
      1310: {
        items: 3
      }
    }
  });
  $(".product-details-slider").owlCarousel({
    items: 1,
    loop: true,
    autoplay: false,
    dots: true,
    dotsContainer: '.product-details-dots',
    nav: false,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoHeight: true
  });
  $('.product-details-dots').on('click', '.detail-dot', function (e) {
    $(".product-details-slider").trigger('to.owl.carousel', [$(this).index(), 300]);
  });
  $(".about-slider").owlCarousel({
    items: 1,
    loop: true,
    autoplay: false,
    dots: true,
    dotsContainer: '.about-slider-dots',
    nav: false,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoHeight: true
  });
  $('.about-slider-dots').on('click', '.about-slider-dot', function (e) {
    $(".about-slider").trigger('to.owl.carousel', [$(this).index(), 300]);
  });
  $(".product-image-slider").owlCarousel({
    items: 1,
    loop: true,
    autoplay: false,
    dots: true,
    dotsContainer: '.product-image-dots',
    nav: false,
    autoHeight: true
  });
  $('.product-image-dots').on('click', '.product-image-dot', function (e) {
    $(".product-image-slider").trigger('to.owl.carousel', [$(this).index(), 300]);
  }); 

  $(".hamburger").on("click", function () {
    $(this).toggleClass("open");
    $("body").toggleClass("no-scroll");
    $(".nav-menu").toggleClass("open");
  });
  $(".input-text.qty.text").bind('keyup mouseup', function () {
    var value = $(this).val();
    $(".product_quantity").val(value);
  }); 

  var activePage = $(".page.active");
  var activeSide = $(".checkout-sidebar-btn.active");
  var cdPage = $("#customer-details");
  var sdPage = $("#shipping-details");
  var smPage = $("#shipping-method");
  var roPage = $("#review-order");
  var paymentMethod = $("#payment-method");
  var cdButton = $("#customer-details .next");
  var sdButton = $("#shipping-details .next");
  var smButton = $("#shipping-method .next");
  var roButton = $("#to-payment-method");
  var roSide = $("#sidebar-btn-ro");
  var cdSide = $("#sidebar-btn-cd");
  var sdSide = $("#sidebar-btn-sd");
  var smSide = $("#sidebar-btn-sm");
  var pmSide = $("#sidebar-btn-pm");
  var inputEmail = $("#cd_email_address_field");
  var timeoutHandler = null;
  $(".woocommerce .next").click(function (event) {
    event.preventDefault();
    $("html, body").animate({
      scrollTop: 0
    }, "slow");
    return false;
  });
  cdPage.show();
  cdButton.on("click", function (event) {
    event.preventDefault();
    checkInputsCustomerDetails();
    $("body.woocommerce-checkout .woocommerce").removeClass("order-review");
  });
  sdButton.on("click", function (event) {
    event.preventDefault();
    checkInputsShipping();
    $("body.woocommerce-checkout .woocommerce").removeClass("order-review");
  });
  smButton.on("click", function (event) {
    event.preventDefault();
    validatedStep = 3; 

    roPage.addClass("loading");
    smPage.removeClass("active");
    smSide.removeClass("active");
    roSide.addClass("active");
    $("body.woocommerce-checkout .woocommerce").addClass("order-review");
    timeoutHandler = setTimeout(function () {
      roPage.removeClass("loading");
      roPage.addClass("active");
    }, 1000);
  });
  roButton.on("click", function (event) {
    event.preventDefault();
    validatedStep = 4; 

    $("#payment-method").addClass("loading");
    roPage.removeClass("active");
    roSide.removeClass("active");
    pmSide.addClass("active"); 

    $("body.woocommerce-checkout .woocommerce").removeClass("order-review");
    timeoutHandler = setTimeout(function () {
      $("#payment-method").removeClass("loading");
      $("#payment-method").addClass("active");
    }, 1000);
  });
  pmSide.on("click", function (event) {
    event.preventDefault();
    console.log(validatedStep);

    if (validatedStep >= 4) {
      $("#payment-method").addClass("loading");
      $(this).addClass("active");
      cdSide.removeClass("active");
      sdSide.removeClass("active");
      smSide.removeClass("active");
      roSide.removeClass("active");
      cdPage.removeClass("active");
      sdPage.removeClass("active");
      smPage.removeClass("active");
      roPage.removeClass("active");
      $("body.woocommerce-checkout .woocommerce").removeClass("order-review");
      timeoutHandler = setTimeout(function () {
        $("#payment-method").removeClass("loading");
        $("#payment-method").addClass("active");
      }, 1000);
    }
  });
  roSide.on("click", function (event) {
    event.preventDefault();
    console.log(validatedStep);

    if (validatedStep >= 3) {
      roPage.addClass("loading");
      $(this).addClass("active");
      cdSide.removeClass("active");
      sdSide.removeClass("active");
      smSide.removeClass("active");
      pmSide.removeClass("active");
      cdPage.removeClass("active");
      sdPage.removeClass("active");
      smPage.removeClass("active");
      $("#payment-method").removeClass("active");
      $("body.woocommerce-checkout .woocommerce").addClass("order-review");
      timeoutHandler = setTimeout(function () {
        roPage.removeClass("loading");
        roPage.addClass("active");
      }, 1000);
    }
  });
  cdSide.on("click", function (event) {
    event.preventDefault();
    cdPage.addClass("loading");
    $(this).addClass("active");
    roSide.removeClass("active");
    sdSide.removeClass("active");
    smSide.removeClass("active");
    pmSide.removeClass("active");
    roPage.removeClass("active");
    sdPage.removeClass("active");
    smPage.removeClass("active");
    $("#payment-method").removeClass("active");
    $("body.woocommerce-checkout .woocommerce").removeClass("order-review");
    timeoutHandler = setTimeout(function () {
      cdPage.removeClass("loading");
      cdPage.addClass("active");
    }, 1000);
  });
  sdSide.on("click", function (event) {
    event.preventDefault();
    console.log(validatedStep);

    if (validatedStep >= 1) {
      sdPage.addClass("loading");
      $(this).addClass("active");
      cdSide.removeClass("active");
      roSide.removeClass("active");
      smSide.removeClass("active");
      pmSide.removeClass("active");
      cdPage.removeClass("active");
      roPage.removeClass("active");
      smPage.removeClass("active");
      $("#payment-method").removeClass("active");
      $("body.woocommerce-checkout .woocommerce").removeClass("order-review");
      timeoutHandler = setTimeout(function () {
        sdPage.removeClass("loading");
        sdPage.addClass("active");
      }, 1000);
    }
  });
  smSide.on("click", function (event) {
    event.preventDefault();
    console.log(validatedStep);

    if (validatedStep >= 2) {
      smPage.addClass("loading");
      $(this).addClass("active");
      cdSide.removeClass("active");
      sdSide.removeClass("active");
      roSide.removeClass("active");
      pmSide.removeClass("active");
      cdPage.removeClass("active");
      sdPage.removeClass("active");
      roPage.removeClass("active");
      $("#payment-method").removeClass("active");
      $("body.woocommerce-checkout .woocommerce").removeClass("order-review");
      timeoutHandler = setTimeout(function () {
        smPage.removeClass("loading");
        smPage.addClass("active");
      }, 1000);
    }
  });

  if ($("#review-order").hasClass("active")) {
  } else {
    $("body.woocommerce-checkout .woocommerce").removeClass("order-review");
  } 


  var $input = $(".woocommerce input");
  var $gform_input = $(".ginput_container input[type=text], .ginput_container textarea, .ginput_container select");
  var $affiliate_input = $(".affwp-form input, .affwp-form textarea");

  function checkForInput(element) {
    const $label = $(element).parent().siblings('label');
    const $wrapper = $(element).parent();

    if ($(element).val().length > 0) {
      $label.addClass('focus');
      $wrapper.addClass('focus');
    } else {
      $label.removeClass('focus');
      $wrapper.removeClass('focus');
    }
  }

  function checkForInputLogin(element) {
    const $label = $(element).siblings('label');
    const $wrapper = $(element).parent();

    if ($(element).val().length > 0) {
      $label.addClass('focus');
      $wrapper.addClass('focus');
    } else {
      $label.removeClass('focus');
      $wrapper.removeClass('focus');
    }
  } 


  $input.each(function () {
    checkForInput(this);
    checkForInputLogin(this);
  });
  $gform_input.each(function () {
    checkForInput(this);
  });
  $affiliate_input.each(function () {
    checkForInput(this);
  }); 

  $input.on('focus change keyup', function () {
    checkForInput(this);
    checkForInputLogin(this);
  });
  $gform_input.on('focus change keyup', function () {
    checkForInput(this);
  });
  $affiliate_input.on('focus change keyup', function () {
    checkForInput(this);
  });
  $("input#shipping_address_1, input#shipping_address_2").removeAttr("placeholder"); 

  var allPanels = $(".faq-text").hide();
  $(".faq").on("click", function () {
    $(this).children(".faq-title .faq-toggle").toggleClass("open");
    $(this).children(".faq-text").slideToggle();
  }); 

  $(".mobile-dash-menu").on("click", function () {
    $(".woocommerce-MyAccount-navigation").slideToggle();
    $(this).toggleClass("open");
  });
  $('div.woocommerce').on('change', '.qty', function () {
    $("[name='update_cart']").prop("disabled", false);
    $("[name='update_cart']").trigger("click");
  });
  $("#customer-capture button").on("click", function (e) {
    e.preventDefault();

    if ($("#customer-capture [name='cd_first_name']").val() === '') {
      $("#customer-capture [name='cd_first_name']").css("border", "1px solid red");
    } else if ($("#customer-capture [name='cd_email_address']").val() === '') {
      $("#customer-capture [name='cd_email_address']").css("border", "1px solid red");
    } else {
      $("#loading-img").css("display", "block");
      var sendData = $(this).serialize();
      $.ajax({
        type: "POST",
        url: "get_response.php",
        data: sendData,
        success: function (data) {
          $("#loading-img").css("display", "none");
          $(".response_msg").text(data);
          $(".response_msg").slideDown().fadeOut(3000);
          $("#customer-capture").find("input[type=text], input[type=email], textarea").val("");
        }
      });
    }
  });
  $("#customer-capture input").blur(function () {
    var checkValue = $(this).val();

    if (checkValue != '') {
      $(this).css("border", "1px solid #eeeeee");
    }
  }); 

  jQuery(document).on("click touch", ".header-cart-count, .cart-count-number-span, .close-cart svg", function () {
    var cartCount = jQuery(".cart-count-number-span").html(); 

    if (cartCount != 0) {
      jQuery(".mini-cart").toggleClass("cart-open");
      jQuery("header.container-large").toggleClass("cart-open");
      jQuery("body").toggleClass("no-scroll");

      if (miniCartState == 'open') {
        miniCartState = 'closed';
      } else if (miniCartState == 'closed') {
        miniCartState = 'open';
      }
    } else {
      jQuery(".empty-cart-notice").addClass("visible");
      timeoutHandler = setTimeout(function () {
        jQuery(".empty-cart-notice").removeClass("visible");
      }, 2000);
    } 

  }); 


  $(document).on('click', '.single_add_to_cart_button', function (e) {
    e.preventDefault(); 

    $thisbutton = $(this), $form = $thisbutton.closest('form.cart'), id = $thisbutton.val(), product_qty = $(document).find('input[name=quantity]').val() || 1, product_id = $form.find('input[name=product_id]').val() || id, variation_id = $form.find('input[name=variation_id]').val() || 0; 

    var data = {
      action: 'woocommerce_ajax_add_to_cart',
      product_id: product_id,
      product_sku: '',
      quantity: product_qty,
      variation_id: variation_id
    }; 

    $(document.body).trigger('adding_to_cart', [$thisbutton, data]); 

    $.ajax({
      type: 'post',
      url: wc_add_to_cart_params.ajax_url,
      data: data,
      beforeSend: function (response) {
        $thisbutton.removeClass('added').addClass('loading');
      },
      complete: function (response) {
        $thisbutton.addClass('added').removeClass('loading');
      },
      success: function (response) {
        if (response.error & response.product_url) {
          window.location = response.product_url;
          return;
        } else {
          $(".mini-cart-wrap").replaceWith(response.fragments.minicart); 

          if (miniCartState == 'open') {
            $(".mini-cart").addClass('cart-open');
          }

          $(".cart-count-number").replaceWith(response.fragments.cartcountnumber);
          $(".ogk-mini-cart-scripts").replaceWith(response.fragments.minicartscripts); 

          $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);

          if (miniCartState == 'open') {
            $(".item-added").addClass('cart-open');
          }

          $(".item-added").addClass("visible");
          setTimeout(function () {
            jQuery(".item-added").removeClass("visible");

            if (miniCartState == 'open') {
              $(".item-added").removeClass('cart-open');
            }
          }, 2000); 

          ogk_header_cart_form = jQuery('.woocommerce-cart-form');
          ogk_header_cart_form_initial_state = ogk_header_cart_form.serialize();
        }
      }
    });
  });
  let shippingToOption = 'billing';
  $(document).on('click', 'input[name="checkout_ship_to_option"]', function () {
    if (this.value != shippingToOption) {
      $("#ship-to-different-address-checkbox").click();
      shippingToOption = this.value;
    }

  }); 

  $(document).on('submit', '.woocommerce-cart-form', {
    passive: true
  }, function (event) {
    event.preventDefault();
    updateHeaderCart();
    return false;
  }); 

  $(document).on('change', '.woocommerce-cart-form .product-quantity input', {
    passive: true
  }, function (event) {
    qtyTimeoutHandler = null;
    qtyTimeoutHandler = setTimeout(function () {
      updateHeaderCart();
      return false;
    }, 1000);
  }); 

  $(document).on('click', '.product-remove a', {
    passive: true
  }, function (event) {
    event.preventDefault(); 

    var remove_cart_key = jQuery(this).attr('data-cart_key');
    jQuery("#header_cart_remove_product").val(remove_cart_key);
    updateHeaderCart();
    return false;
  }); 

  $(document).on('click', 'a.woocommerce-remove-coupon', {
    passive: true
  }, function (event) {
    event.preventDefault();
    var remove_coupon_id = jQuery(this).attr('data-coupon');
    jQuery("#header_cart_remove_coupon").val(remove_coupon_id);
    updateHeaderCart();
    return false;
  });
});

function updateHeaderCart() {
  let ogk_header_cart_form = jQuery('.woocommerce-cart-form');
  let ogk_header_cart_form_current_state = ogk_header_cart_form.serialize(); 

  if (ogk_header_cart_form_initial_state != ogk_header_cart_form_current_state) {
    let data = ogk_header_cart_form.serializeArray(); 

    jQuery.ajax({
      url: ogk_header_cart_form.attr('action'),
      data: data,
      dataType: "json",
      type: ogk_header_cart_form.attr('method'),
      beforeSend: function (data) {
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('jqXHR:');
        console.log(jqXHR);
        console.log('textStatus:');
        console.log(textStatus);
        console.log('AjaxErrorThrown:');
        console.log(errorThrown);
      },
      success: function (response) {
        if (response.type == "success") {
          jQuery(".cart-wrap").replaceWith(response.cart_html); 

          cartCount = response.cart_count;
          jQuery('.cart-count-number span').html(cartCount); 

          if (response.notice != '' && cartCount != 0) {
            jQuery(".update-cart-notice .container-large p").html(response.notice);
            jQuery(".update-cart-notice").addClass("visible");
            setTimeout(function () {
              jQuery(".update-cart-notice").removeClass("visible");
            }, 2000);
          }

          if (cartCount == 0) {
            jQuery(".mini-cart").toggleClass("cart-open");
            jQuery("header.container-large, header.checkout-header").toggleClass("cart-open");
            jQuery("body").removeClass("no-scroll"); 

            jQuery(".empty-cart-notice").addClass("visible");
            timeoutHandler = setTimeout(function () {
              jQuery(".empty-cart-notice").removeClass("visible");
            }, 2000);
            miniCartState = 'closed'; 
          } 


          jQuery(".order-review-wrap").replaceWith(response.order_review);
          setTimeout(function () {
            if (document.querySelector('header').classList.contains('checkout-header')) {
              document.querySelector(".cart-total .woocommerce-Price-amount").innerHTML = document.querySelector(".subtotal-line .woocommerce-Price-amount").innerHTML;
            }
          }, 300); 
        }
      }
    });
  }
} 




jQuery(document).on('updated_checkout', function (event, data) {
  console.log(data);
  console.log(data.fragments.cart_totals.shipping_total);
  jQuery(".cart_shipping_label").html(data.fragments.cart_shipping_label);
  jQuery(".cart_shipping_total").html(data.fragments.cart_totals_formatted.shipping_total);
  jQuery(".cart_total_tax").html(data.fragments.cart_totals_formatted.total_tax);
  jQuery(".cart_total").html(data.fragments.cart_totals_formatted.total);
}); 

const billingEmail = document.getElementById("billing_email");
const billingFirstName = document.getElementById("billing_first_name");
const billingLastName = document.getElementById("billing_last_name");
const billingAddress1 = document.getElementById("billing_address_1");
const billingCity = document.getElementById("billing_city");
const billingState = document.getElementById("billing_state");
const billingPostcode = document.getElementById("billing_postcode");
const billingCountry = document.getElementById("billing_country");
const shipToOption = jQuery("#ship-to-different-address-checkbox");
const addressInput = document.getElementById("shipping_address_1");
const cityInput = document.getElementById("shipping_city");
const stateInput = document.getElementById("shipping_state");
const zipInput = document.getElementById("shipping_postcode"); 

var error = jQuery(".error");
var emailInputParent = jQuery(billingEmail).parent();
var emailError = "<p class='error'>Please enter your Email Address</p>";
var firstNameInputParent = jQuery(billingFirstName).parent();
var firstNameError = "<p class='error'>Please enter your First Name</p>";
var lastNameInputParent = jQuery(billingLastName).parent();
var lastNameError = "<p class='error'>Please enter your Last Name</p>";
var billingAddress1InputParent = jQuery(billingAddress1).parent();
var billingAddress1Error = "<p class='error'>Please enter your Address</p>";
var billingStateInputParent = jQuery(billingState).parent();
var billingStateError = "<p class='error'>Please enter your State</p>";
var billingCityInputParent = jQuery(billingCity).parent();
var billingCityError = "<p class='error'>Please enter your City</p>";
var billingPostcodeInputParent = jQuery(billingPostcode).parent();
var billingPostcodeError = "<p class='error'>Please enter your Postcode</p>";
var billingCountryInputParent = jQuery(billingCountry).parent();
var billingCountryError = "<p class='error'>Please enter your Country</p>";
var addressInputParent = jQuery("#shipping_address_1").parent();
var cityInputParent = jQuery("#shipping_city").parent();
var stateInputParent = jQuery("#shipping_state").parent();
var zipInputParent = jQuery("#shipping_postcode").parent();
var activePage = jQuery(".page.active");
var cdPage = jQuery("#customer-details");
var sdPage = jQuery("#shipping-details");
var smPage = jQuery("#shipping-method");
var roPage = jQuery("#review-order");
var cdButton = jQuery("#customer-details .next");
var sdButton = jQuery("#shipping-details .next");
var smButton = jQuery("#shipping-method .next");
var roSide = jQuery("#sidebar-btn-ro");
var cdSide = jQuery("#sidebar-btn-cd");
var sdSide = jQuery("#sidebar-btn-sd");
var smSide = jQuery("#sidebar-btn-sm"); 

function checkInputsCustomerDetails() {
  beforeCheckoutValidation(); 

  if (billingEmail.value && billingFirstName.value && billingLastName.value && billingAddress1.value && billingCity.value && billingState.value && billingPostcode.value && billingCountry.value) {
    validatedStep = 1; 

    sdPage.addClass("loading");
    cdPage.removeClass("active");
    cdSide.removeClass("active");
    sdSide.addClass("active");
    setTimeout(function () {
      sdPage.removeClass("loading");
      sdPage.addClass("active");
    }, 1000);
  } 


  if (!billingEmail.value) {
    emailInputParent.append(emailError); 
  }

  if (!billingFirstName.value) {
    firstNameInputParent.append(firstNameError); 
  }

  if (!billingLastName.value) {
    lastNameInputParent.append(lastNameError); 
  }

  if (!billingAddress1.value) {
    billingAddress1InputParent.append(billingAddress1Error); 
  }

  if (!billingCity.value) {
    billingCityInputParent.append(billingCityError); 
  }

  if (!billingState.value) {
    billingStateInputParent.append(billingStateError); 
  }

  if (!billingPostcode.value) {
    billingPostcodeInputParent.append(billingPostcodeError); 
  }

  if (!billingCountry.value) {
    billingCountryInputParent.append(billingCountryError); 
  }
}

function checkInputsShipping() {
  beforeCheckoutValidation(); 

  if (addressInput.value && cityInput.value && zipInput.value || !shipToOption.is(":checked")) {
    validatedStep = 2; 

    smPage.addClass("loading");
    sdPage.removeClass("active");
    sdSide.removeClass("active");
    smSide.addClass("active");
    timeoutHandler = setTimeout(function () {
      smPage.removeClass("loading");
      smPage.addClass("active");
    }, 1000);
  }

  if (!addressInput.value && addressInputParent.html().indexOf("<p class='error'>Please enter your Address</p>") == -1) {
    addressInputParent.append("<p class='error'>Please enter your Address</p>");
  } 


  if (!cityInput.value && cityInputParent.html().indexOf("<p class='error'>Please enter your City</p>") == -1) {
    cityInputParent.append("<p class='error'>Please enter your City</p>");
  } 


  if (!zipInput.value && zipInputParent.html().indexOf("<p class='error'>Please enter your Zip Code</p>") == -1) {
    zipInputParent.append("<p class='error'>Please enter your Zip Code</p>");
  } 

}

function beforeCheckoutValidation() {
  jQuery(".error").remove();
} 


function routerOnRdy() {
  const urlParams = new URLSearchParams(window.location.search);
  const isCartOpen = urlParams.get("cart");

  if (isCartOpen == "true" || isCartOpen == "true") {
    document.querySelector(".mini-cart").classList.add('cart-open');
    document.body.classList.add('no-scroll');
  }
}
!function (e) {
  "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery);
}(function ($) {
  function e() {
    var e,
        t,
        n = {
      height: f.innerHeight,
      width: f.innerWidth
    };
    return n.height || !(e = l.compatMode) && $.support.boxModel || (t = "CSS1Compat" === e ? a : l.body, n = {
      height: t.clientHeight,
      width: t.clientWidth
    }), n;
  }

  function t() {
    return {
      top: f.pageYOffset || a.scrollTop || l.body.scrollTop,
      left: f.pageXOffset || a.scrollLeft || l.body.scrollLeft
    };
  }

  function n() {
    if (i.length) {
      var n = 0,
          l = $.map(i, function (e) {
        var t = e.data.selector,
            n = e.$element;
        return t ? n.find(t) : n;
      });

      for (o = o || e(), r = r || t(); n < i.length; n++) if ($.contains(a, l[n][0])) {
        var f = $(l[n]),
            d = {
          height: f[0].offsetHeight,
          width: f[0].offsetWidth
        },
            h = f.offset(),
            c = f.data("inview");
        if (!r || !o) return;
        h.top + d.height > r.top && h.top < r.top + o.height && h.left + d.width > r.left && h.left < r.left + o.width ? c || f.data("inview", !0).trigger("inview", [!0]) : c && f.data("inview", !1).trigger("inview", [!1]);
      }
    }
  }

  var i = [],
      o,
      r,
      l = document,
      f = window,
      a = l.documentElement,
      d;
  $.event.special.inview = {
    add: function (e) {
      i.push({
        data: e,
        $element: $(this),
        element: this
      }), !d && i.length && (d = setInterval(n, 250));
    },
    remove: function (e) {
      for (var t = 0; t < i.length; t++) {
        var n = i[t];

        if (n.element === this && n.data.guid === e.guid) {
          i.splice(t, 1);
          break;
        }
      }

      i.length || (clearInterval(d), d = null);
    }
  }, $(f).on("scroll resize scrollstop", function () {
    o = r = null;
  }), !a.addEventListener && a.attachEvent && a.attachEvent("onfocusin", function () {
    r = null;
  });
});