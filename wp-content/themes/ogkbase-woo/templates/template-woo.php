<?php
/* Template Name: Woocommerce Page */
get_header(); ?>

<section id="<?= sanitize_title(get_the_title()) ?>" class="woo-section <?php if( is_page(array('my-account')) && !is_user_logged_in() ): ?>login-form<?php endif; ?>">
    <div class="container">
        <?php the_content(); ?>
    </div>
</section>

<?php get_footer();
