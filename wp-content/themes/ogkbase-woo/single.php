<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<section class="page-header">
    <div class="container">
        <h1><span><?= get_the_title(); ?></span></h1>
    </div>
</section>
<section class="blog-content bg-ofw">
    <div class="container">
        <div class="blog-top">
            <div class="blog-image" style="background: url(<?= get_the_post_thumbnail_url() ?>) no-repeat center / cover;">
            </div>
            <div class="blog-sidebar">
                <div class="block">
                    <h5>Post Date</h5>
                    <p><?= get_the_date('M d, Y') ?></p>
                </div>
                <div class="block">
                    <h5>Share this Post</h5>
                    <?php get_template_part("includes/social-share"); ?>
                </div>
                <div class="block">
                    <h5>Quick Links</h5>
                    <div class="btn-wrap">
                        <a href="/faqs/">FAQS</a>
                        <a href="/about/">About Us</a>
                        <a href="/contact/">Contact Us</a>
                    </div>
                </div>
                <div class="product-sugg">
                    <h5>Suggested For You</h5>
                    <?php
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => 1,
                        'orderby' => 'rand'
                    );
                    ?>
                    <?php $randprod = new WP_Query( $args ); ?>
                    <?php if($randprod->have_posts()): ?>
                        <?php while($randprod->have_posts()): $randprod->the_post(); ?>
                            <div class="product">
                                <?= get_the_post_thumbnail() ?>
                                <h6><?= get_the_title() ?></h6>
                                <p><?= get_field('product_flavor') ?></p>
                                <div class="btn-wrap">
                                    <a href="<?= get_the_permalink() ?>" class="btn dark-hover">Shop</a>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="blog-wrap">
            <?= apply_filters('the_content', $post->post_content); ?>
        </div>
        <div class="btn-wrap">
            <a href="/culture/" class="btn dark-hover">Back to Blog</a>
        </div>
    </div>
</section>

<?php endwhile; ?>
<?php get_footer(); ?>
